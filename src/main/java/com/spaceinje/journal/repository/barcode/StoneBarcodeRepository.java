package com.spaceinje.journal.repository.barcode;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.spaceinje.journal.model.barcode.StoneBarcode;

public interface StoneBarcodeRepository
		extends JpaRepository<StoneBarcode, String>, JpaSpecificationExecutor<StoneBarcode> {

}
