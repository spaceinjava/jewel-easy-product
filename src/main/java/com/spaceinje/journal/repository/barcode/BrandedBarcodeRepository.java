package com.spaceinje.journal.repository.barcode;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.spaceinje.journal.model.barcode.BrandedBarcode;

public interface BrandedBarcodeRepository
		extends JpaRepository<BrandedBarcode, String>, JpaSpecificationExecutor<BrandedBarcode> {

}
