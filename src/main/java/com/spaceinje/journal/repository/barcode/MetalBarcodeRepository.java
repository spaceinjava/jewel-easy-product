package com.spaceinje.journal.repository.barcode;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.spaceinje.journal.model.barcode.MetalBarcode;

public interface MetalBarcodeRepository
		extends JpaRepository<MetalBarcode, String>, JpaSpecificationExecutor<MetalBarcode> {

	final String lastGeneratedCodeQuery = "select code from ( "
			+ "	(select m.code as code, m.createddate from je_product.metal_barcode m where m.tenant_code = ?1 and m.createddate = ?2 order by m.createddate desc limit 1) union "
			+ "	(select b.code as code, b.createddate from je_product.branded_barcode b where b.tenant_code = ?1 and b.createddate = ?2 order by b.createddate desc limit 1) union "
			+ "	(select s.code as code, s.createddate from je_product.stone_barcode s where s.tenant_code = ?1 and s.createddate = ?2 order by s.createddate desc limit 1) "
			+ ") res order by res.createddate desc limit 1";

	@Query(value = lastGeneratedCodeQuery, nativeQuery = true)
	String fetchLastCodeGeneratedByTenantCode(String tenantCode, Date date);
	
	//today date created date in where condition 
	

}
