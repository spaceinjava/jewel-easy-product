package com.spaceinje.journal.repository.lot;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spaceinje.journal.model.lot.MetalLot;

@Repository
public interface MetalLotRepository extends JpaRepository<MetalLot, String>, JpaSpecificationExecutor<MetalLot> {

	Optional<MetalLot> findByLotNoAndTenantCode(String lotNo, String tenantCode);
	
	@Query(value="select lot_No from ((select m.lot_No as lot_No, m.createddate from je_product.Metal_Lot m where m.tenant_code =?1 order by m.createddate desc limit 1) union\n" + 
			"		(select b.lot_No as lot_No, b.createddate from je_product.Branded_Lot b where b.tenant_code =?1 order by b.createddate desc limit 1) union\n" + 
			"		(select s.lot_No as lot_No, s.createddate from je_product.Stone_Lot s where s.tenant_code =?1 and s.lot_No is not null order by s.createddate desc limit 1)\n" + 
			"	) res order by res.createddate desc\n" + 
			"	limit 1" , nativeQuery = true)
	String getLotId(String tenantCode);

}
