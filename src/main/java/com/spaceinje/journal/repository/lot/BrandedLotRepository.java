package com.spaceinje.journal.repository.lot;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.journal.model.lot.BrandedLot;

@Repository
public interface BrandedLotRepository extends JpaRepository<BrandedLot, String>, JpaSpecificationExecutor<BrandedLot> {

	Optional<BrandedLot> findByLotNoAndTenantCode(String lotNo, String tenantCode);

}
