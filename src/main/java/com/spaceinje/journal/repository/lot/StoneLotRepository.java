package com.spaceinje.journal.repository.lot;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.journal.model.lot.StoneLot;

@Repository
public interface StoneLotRepository
		extends JpaRepository<StoneLot, String>, JpaSpecificationExecutor<StoneLot> {

	Optional<StoneLot> findByLotNoAndTenantCode(String lotNo, String tenantCode);

}
