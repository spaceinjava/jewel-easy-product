package com.spaceinje.journal.repository.inventory;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.journal.model.inventory.GemStonePurchase;

@Repository
public interface GemStonePurchaseRepository
		extends JpaRepository<GemStonePurchase, String>, JpaSpecificationExecutor<GemStonePurchase> {

	@Modifying
	@Transactional
	@Query(value = "update GemStonePurchase set isDeleted = true where id in (?1)")
	public void deleteGemStonePurchases(Set<String> gemStonePurchaseIds);

	public Optional<GemStonePurchase> findTopByTenantCodeOrderByCreatedDateDesc(String tenantCode);

}
