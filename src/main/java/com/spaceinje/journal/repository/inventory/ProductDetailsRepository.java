package com.spaceinje.journal.repository.inventory;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.journal.model.inventory.ProductDetails;

@Repository
public interface ProductDetailsRepository
		extends JpaRepository<ProductDetails, String>, JpaSpecificationExecutor<ProductDetails> {

}
