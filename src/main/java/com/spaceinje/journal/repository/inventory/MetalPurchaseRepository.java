package com.spaceinje.journal.repository.inventory;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.journal.model.inventory.MetalPurchase;

@Repository
public interface MetalPurchaseRepository
		extends JpaRepository<MetalPurchase, String>, JpaSpecificationExecutor<MetalPurchase> {

//	@Query(value = "select case when count(mg) > 0 then true else false end from MainGroup mg left join mg.brands b on b.mainGroup = mg "
//			+ " left join mg.products p on p.mainGroup = mg left join mg.purities pu on pu.mainGroup = mg "
//			+ " where (b.isDeleted = false or p.isDeleted = false or pu.isDeleted = false) and mg.id in (?1)")
//	public boolean metalPurchaseHasOrphans(Set<String> metalPurchaseIds);

	@Modifying
	@Transactional
	@Query(value = "update MetalPurchase set isDeleted = true where id in (?1)")
	public void deleteMetalPurchases(Set<String> metalPurchaseIds);

	public Optional<MetalPurchase> findTopByTenantCodeOrderByCreatedDateDesc(String tenantCode);

}
