package com.spaceinje.journal.repository.inventory;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.journal.model.inventory.BrandedPurchase;

@Repository
public interface BrandedPurchaseRepository
		extends JpaRepository<BrandedPurchase, String>, JpaSpecificationExecutor<BrandedPurchase> {

	@Modifying
	@Transactional
	@Query(value = "update BrandedPurchase set isDeleted = true where id in (?1)")
	public void deleteBrandedPurchases(Set<String> brandedPurchaseIds);

	public Optional<BrandedPurchase> findTopByTenantCodeOrderByCreatedDateDesc(String tenantCode);

}
