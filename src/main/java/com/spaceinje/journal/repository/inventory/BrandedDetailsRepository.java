package com.spaceinje.journal.repository.inventory;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.journal.model.inventory.BrandedDetails;

@Repository
public interface BrandedDetailsRepository
		extends JpaRepository<BrandedDetails, String>, JpaSpecificationExecutor<BrandedDetails> {

}
