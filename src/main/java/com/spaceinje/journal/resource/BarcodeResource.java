package com.spaceinje.journal.resource;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spaceinje.journal.bean.barcode.BrandedBarcodeBean;
import com.spaceinje.journal.bean.barcode.MetalBarcodeBean;
import com.spaceinje.journal.bean.barcode.StoneBarcodeBean;
import com.spaceinje.journal.service.BarcodeService;
import com.spaceinje.journal.service.LotService;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/journal/barcode")
@SuppressWarnings("rawtypes")
public class BarcodeResource {

	private static final Logger log = LoggerFactory.getLogger(BarcodeResource.class);

	@Autowired
	private BarcodeService barcodeService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private Tracer tracer;

	@Autowired
	private LotService lotService;

	@PostMapping(value = "/metal", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveMetalBarcode(
			@RequestPart(value = "image", required = false) MultipartFile file,
			@RequestPart(value = "bean") String bean)
			throws MethodArgumentNotValidException, JsonMappingException, JsonProcessingException {
		log.debug("calling saveMetalBarcode service");
		MetalBarcodeBean metalBarcodeBean = new ObjectMapper().readValue(bean, MetalBarcodeBean.class);
		ResponseCachet cachet = barcodeService.saveMetalBarcode(metalBarcodeBean, file);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveMetalBarcode service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/branded", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveBrandedBarcode(
			@RequestPart(value = "image", required = false) MultipartFile file,
			@RequestPart(value = "bean") String bean)
			throws MethodArgumentNotValidException, JsonMappingException, JsonProcessingException {
		log.debug("calling saveBrandedBarcode service");
		BrandedBarcodeBean brandedBarcodeBean = new ObjectMapper().readValue(bean, BrandedBarcodeBean.class);
		ResponseCachet cachet = barcodeService.saveBrandedBarcode(brandedBarcodeBean, file);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveBrandedBarcode service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/stone", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveStoneBarcode(
			@RequestPart(value = "image", required = false) MultipartFile file,
			@RequestPart(value = "bean") String bean)
			throws MethodArgumentNotValidException, JsonMappingException, JsonProcessingException {
		log.debug("calling saveStoneBarcode service");
		StoneBarcodeBean stoneBarcodeBean = new ObjectMapper().readValue(bean, StoneBarcodeBean.class);
		ResponseCachet cachet = barcodeService.saveStoneBarcode(stoneBarcodeBean, file);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveStoneBarcode service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/{lot_type}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchLotDetailsByType(@PathVariable(value = "lot_type") String lotType,
			@RequestParam(value = "tenant_code") String tenantCode,
			@RequestParam(value = "party_id", required = false) String partyId,
			@RequestParam(value = "main_group_id", required = false) String mainGroupId,
			@RequestParam(value = "from_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate fromDate,
			@RequestParam(value = "to_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate toDate) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchLotDetailsByType service");
		cachet = lotService.fetchLotDetailsByType(tenantCode, lotType, partyId, mainGroupId, toDate, fromDate);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchLotDetailsByType service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
