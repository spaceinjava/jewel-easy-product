package com.spaceinje.journal.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.LotServiceClient;
import com.spaceinje.journal.bean.lot.BrandedLotBean;
import com.spaceinje.journal.bean.lot.MetalLotBean;
import com.spaceinje.journal.bean.lot.StoneLotBean;
import com.spaceinje.journal.service.LotService;
import com.spaceinje.journal.validator.LotValidator;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/journal/lot-creation")
@SuppressWarnings("rawtypes")
public class LotResource {

	private static final Logger log = LoggerFactory.getLogger(LotResource.class);

	@Autowired
	private LotService lotService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private LotValidator lotValidator;

	@Autowired
	private Tracer tracer;

	@Autowired
	private LotServiceClient lotServiceClient;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@GetMapping(value = "/rc/{lotType}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllRcNos(@PathVariable(value = "lotType") String lotType,
			@RequestParam(value = "tenant_code") String tenantCode,
			@RequestParam(value = "supplier_Id") String supplierId,
			@RequestParam(value = "purchaseType") String purchaseType,
			@RequestParam(value = "from_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate fromDate,
			@RequestParam(value = "to_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate toDate) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(supplierId)) {
			log.debug("supplierName can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("supplierName can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(purchaseType)) {
			log.debug("purchaseType can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("purchaseType can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllRcNos service");
		cachet = lotService.fetchAllRcNos(tenantCode, supplierId, lotType, purchaseType, fromDate, toDate);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllMetalPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/{lotType}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllMetalDetails(@PathVariable(value = "lotType") String lotType,
			@RequestParam(value = "tenant_code") String tenantCode, @RequestParam(value = "rc_no") String rcNo) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(rcNo)) {
			log.debug("supplierName can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("supplierName can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllRcNos service");
		cachet = lotService.fetchAllMetalDetails(tenantCode, rcNo, lotType);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllMetalPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/metal", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateMetalLot(@Valid @RequestBody List<MetalLotBean> beans)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = lotValidator.metalLotValidator(beans);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating metal Lot purchase bean");
			return entity;
		}
		log.debug("calling saveOrUpdateMetalLotPurchase service");
		ResponseCachet cachet = lotService.saveOrUpdateMetalLot(beans);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateMetalLot service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/branded", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateBrandedLot(@Valid @RequestBody List<BrandedLotBean> beans)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = lotValidator.brandedLotValidator(beans);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating metal Lot purchase bean");
			return entity;
		}
		log.debug("calling saveOrUpdateBrandedLot service");
		ResponseCachet cachet = lotService.saveOrUpdateBrandedLot(beans);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateBrandedLot service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/stone", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateGemStoneLot(@Valid @RequestBody List<StoneLotBean> beans)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = lotValidator.gemStoneLotValidator(beans);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating gemStoneLot bean");
			return entity;
		}
		log.debug("calling saveOrUpdateGemStoneLot service");
		ResponseCachet cachet = lotService.saveOrUpdateGemStoneLot(beans);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateGemStoneLot service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/getEmployes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> getEmployes(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}

		log.debug("calling getEmployeeDetails service");
		cachet = lotServiceClient.getEmployeeDetails(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting getEmployeeDetails service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
