package com.spaceinje.journal.resource;

import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.journal.bean.inventory.BrandedPurchaseBean;
import com.spaceinje.journal.bean.inventory.GemStonePurchaseBean;
import com.spaceinje.journal.bean.inventory.MetalPurchaseBean;
import com.spaceinje.journal.service.InventoryService;
import com.spaceinje.journal.validator.InventoryValidator;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/journal/inventory")
@SuppressWarnings("rawtypes")
public class InventoryResource {

	private static final Logger log = LoggerFactory.getLogger(InventoryResource.class);

	@Autowired
	private InventoryService inventoryService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private InventoryValidator inventoryValidator;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@PostMapping(value = "/metal-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateMetalPurchase(@Valid @RequestBody MetalPurchaseBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = inventoryValidator.metalPurchaseValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating metal purchase bean");
			return entity;
		}
		log.debug("calling saveOrUpdateMetalPurchase service");
		ResponseCachet cachet = inventoryService.saveOrUpdateMetalPurchase(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateMetalPurchase service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/metal-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllMetalPurchases(
			@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllMetalPurchases service");
		cachet = inventoryService.fetchAllMetalPurchases(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllMetalPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/metal-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteMetalPurchases(
			@RequestParam(value = "metal_purchase_ids") Set<String> metalPurchaseIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(metalPurchaseIds)) {
			log.debug("metalPurchaseIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("metalPurchaseIds can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteMetalPurchases service");
		cachet = inventoryService.deleteMetalPurchases(metalPurchaseIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteMetalPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/branded-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateBrandedPurchase(@Valid @RequestBody BrandedPurchaseBean bean)
			throws MethodArgumentNotValidException {

		ResponseEntity<ResponseCachet> entity = inventoryValidator.brandedPurchaseValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating branded purchase bean");
			return entity;
		}
		log.debug("calling saveOrUpdateBrandedPurchase service");
		ResponseCachet cachet = inventoryService.saveOrUpdateBrandedPurchase(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateBrandedPurchase service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/branded-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllBrandedPurchases(
			@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllBrandedPurchases service");
		cachet = inventoryService.fetchAllBrandedPurchases(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllBrandedPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/branded-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteBrandedPurchases(
			@RequestParam(value = "branded_purchase_ids") Set<String> brandedPurchaseIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(brandedPurchaseIds)) {
			log.debug("brandedurchaseIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("brandedPurchaseIds can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteBrandedPurchases service");
		cachet = inventoryService.deleteBrandedPurchases(brandedPurchaseIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteBrandedPurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/gemstone-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateGemStonePurchase(@Valid @RequestBody GemStonePurchaseBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = inventoryValidator.gemStonePurchaseValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating gemStone purchase bean");
			return entity;
		}
		log.debug("calling saveOrUpdateGemStonePurchase service");
		ResponseCachet cachet = inventoryService.saveOrUpdateGemStonePurchase(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateGemStonePurchase service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/gemstone-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllGemStonePurchases(
			@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllGemStonePurchases service");
		cachet = inventoryService.fetchAllGemStonePurchases(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllGemStonePurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/gemstone-purchase", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteGemStonePurchases(
			@RequestParam(value = "purchase_ids") Set<String> gemStonePurchaseIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(gemStonePurchaseIds)) {
			log.debug("gemStonePurchaseIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("gemStonePurchaseIds can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteMetalGemStonePurchases service");
		cachet = inventoryService.deleteGemStonePurchases(gemStonePurchaseIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteGemStonePurchases service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
