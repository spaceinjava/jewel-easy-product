package com.spaceinje.journal.specification;

import org.springframework.data.jpa.domain.Specification;

import com.spaceinje.products.specification.CommonSpecs;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class InventorySpecs extends CommonSpecs {

	/**
	 * return criteria to retrieve based on given invoiceNo
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification invoiceNoSpec(String invoiceNo) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("invoiceNo"), invoiceNo);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByInvoiceNoSpec(String invoiceNo) {
		return Specification.where(invoiceNoSpec(invoiceNo)).and(isDeletedSpec());
	}
	
	/**
	 * return criteria to retrieve based on given invoiceNo
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification recieptNoSpec(String recieptNo) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("receiptNo"), recieptNo);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByrecieptNoSpec(String recieptNo) {
		return Specification.where(recieptNoSpec(recieptNo)).and(isDeletedSpec());
	}

}
