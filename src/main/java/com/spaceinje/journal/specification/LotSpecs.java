package com.spaceinje.journal.specification;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import com.spaceinje.journal.model.inventory.BrandedPurchase;
import com.spaceinje.journal.model.inventory.GemStonePurchase;
import com.spaceinje.journal.model.inventory.MetalPurchase;
import com.spaceinje.products.specification.CommonSpecs;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class LotSpecs extends CommonSpecs {

	/**
	 * return criteria to retrieve based on given invoiceNo
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification invoiceNoSpec(String invoiceNo) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("invoiceNo"), invoiceNo);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByInvoiceNoSpec(String invoiceNo) {
		return Specification.where(invoiceNoSpec(invoiceNo)).and(isDeletedSpec());
	}

	/**
	 * return criteria to retrieve based on given purchaseType
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification puchaseTypeSpec(String purchaseType) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("purchaseType"), purchaseType);
	}

	/**
	 * return criteria to retrieve based on given supplierName
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification supplierNameSpec(String supplierName) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("partyId"), supplierName);
	}

	/**
	 * return criteria to retrieve based on given rcDate
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification rcDateSpec(LocalDate rcDate) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("invoiceDate"), rcDate);
	}

	/**
	 * return criteria to retrieve based on given rcNo
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification rcNoSpec(String rcNo) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("receiptNo"), rcNo);
	}

	/**
	 * return criteria to retrieve based on given brandedPurchase
	 * 
	 * @param shortCode
	 * @return
	 */

	public static Specification brandedPurchaseSpec(BrandedPurchase brandedPurchase) {
		// TODO Auto-generated method stub
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("brandedPurchase"), brandedPurchase);
	}

	/**
	 * return criteria to retrieve based on given metalPurchase
	 * 
	 * @param shortCode
	 * @return
	 */

	public static Specification metalPurchaseSpec(MetalPurchase metalPurchase) {
		// TODO Auto-generated method stub
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("metalPurchase"), metalPurchase);
	}

	/**
	 * return criteria to retrieve based on given gemStonePurchase
	 * 
	 * @param shortCode
	 * @return
	 */

	public static Specification gemStonePurchaseSpec(GemStonePurchase gemStonePurchase) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("gemStonePurchase"), gemStonePurchase);
	}

	/**
	 * return criteria to retrieve based on mainGroupId with reference of
	 * subProduct, product and mainGroup
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification mainGroupBySubProductSpec(String mainGroupId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
				.equal(root.get("subProduct").get("product").get("mainGroup").get("id"), mainGroupId);
	}

	/**
	 * return criteria to retrieve based on mainGroupId with reference of
	 * brandSubProduct, brandProduct, brand and mainGroup
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification mainGroupByBrandSubProductSpec(String mainGroupId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(
				root.get("brandSubProduct").get("brandProduct").get("brand").get("mainGroup").get("id"), mainGroupId);
	}

}
