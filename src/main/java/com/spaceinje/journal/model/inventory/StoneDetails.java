package com.spaceinje.journal.model.inventory;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneClarity;
import com.spaceinje.products.model.stone.master.StoneColor;
import com.spaceinje.products.model.stone.master.StonePolish;
import com.spaceinje.products.model.stone.master.StoneSize;

@Entity
public class StoneDetails extends AuditEntity {

	private static final long serialVersionUID = -3953523797622590592L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "productDetailsId", nullable = true)
	private ProductDetails productDetails;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "gemStonePurchaseId", nullable = true)
	private GemStonePurchase gemStonePurchase;

	private Double quantity;

	private String uom;

	private Double weightPerGram;

	private Double weightPerKarat;

	private Double rate;

	private Double amount;

	private Boolean isNoWeight;

	private Double gstAmount;

	private Double totalAmount;

	private String remarks;

	private Double createdLotQuantity;

	private Double createdLotWeight;

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public ProductDetails getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(ProductDetails productDetails) {
		this.productDetails = productDetails;
	}

	public GemStonePurchase getGemStonePurchase() {
		return gemStonePurchase;
	}

	public void setGemStonePurchase(GemStonePurchase gemStonePurchase) {
		this.gemStonePurchase = gemStonePurchase;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getIsNoWeight() {
		return isNoWeight;
	}

	public void setIsNoWeight(Boolean isNoWeight) {
		this.isNoWeight = isNoWeight;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	@Override
	public String toString() {
		return "StoneDetails [stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor
				+ ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", productDetails="
				+ productDetails + ", gemStonePurchase=" + gemStonePurchase + ", quantity=" + quantity + ", uom=" + uom
				+ ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", rate=" + rate
				+ ", amount=" + amount + ", isNoWeight=" + isNoWeight + ", gstAmount=" + gstAmount + ", totalAmount="
				+ totalAmount + ", remarks=" + remarks + ", createdLotQuantity=" + createdLotQuantity
				+ ", createdLotWeight=" + createdLotWeight + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
