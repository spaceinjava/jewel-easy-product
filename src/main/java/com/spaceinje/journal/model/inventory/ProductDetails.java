package com.spaceinje.journal.model.inventory;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.product.master.ProductSize;
import com.spaceinje.products.model.product.master.SubProduct;

@Entity
public class ProductDetails extends AuditEntity {

	private static final long serialVersionUID = -8528295785174055148L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private SubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productSizeId", nullable = false)
	private ProductSize productSize;

	private Double quantity;

	private Double grossWeight;

	private Double stoneWeight;

	private Double netWeight;

	private Boolean touchCalc;

	private Double touchPercent;

	private Double touchValue;

	private String wastageOn;

	private Double wastageValue;

	private Double pureWeight;

	private String mcOn;

	private Double mcValue;

	private Double rate;

	private Double amount;

	private Double stoneAmount;

	private Double mcAmount;

	private Double gstAmount;

	private Double totalAmount;

	private String remarks;

	private Double createdLotQuantity;

	private Double createdLotWeight;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "metalPurchaseId", nullable = false)
	private MetalPurchase metalPurchase;

	@OneToMany(mappedBy = "productDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneDetails> stoneDetails;

	public SubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public Boolean getTouchCalc() {
		return touchCalc;
	}

	public void setTouchCalc(Boolean touchCalc) {
		this.touchCalc = touchCalc;
	}

	public Double getTouchPercent() {
		return touchPercent;
	}

	public void setTouchPercent(Double touchPercent) {
		this.touchPercent = touchPercent;
	}

	public Double getTouchValue() {
		return touchValue;
	}

	public void setTouchValue(Double touchValue) {
		this.touchValue = touchValue;
	}

	public String getWastageOn() {
		return wastageOn;
	}

	public void setWastageOn(String wastageOn) {
		this.wastageOn = wastageOn;
	}

	public Double getWastageValue() {
		return wastageValue;
	}

	public void setWastageValue(Double wastageValue) {
		this.wastageValue = wastageValue;
	}

	public Double getPureWeight() {
		return pureWeight;
	}

	public void setPureWeight(Double pureWeight) {
		this.pureWeight = pureWeight;
	}

	public String getMcOn() {
		return mcOn;
	}

	public void setMcOn(String mcOn) {
		this.mcOn = mcOn;
	}

	public Double getMcValue() {
		return mcValue;
	}

	public void setMcValue(Double mcValue) {
		this.mcValue = mcValue;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getStoneAmount() {
		return stoneAmount;
	}

	public void setStoneAmount(Double stoneAmount) {
		this.stoneAmount = stoneAmount;
	}

	public Double getMcAmount() {
		return mcAmount;
	}

	public void setMcAmount(Double mcAmount) {
		this.mcAmount = mcAmount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	public MetalPurchase getMetalPurchase() {
		return metalPurchase;
	}

	public void setMetalPurchase(MetalPurchase metalPurchase) {
		this.metalPurchase = metalPurchase;
	}

	public Set<StoneDetails> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<StoneDetails> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	@Override
	public String toString() {
		return "ProductDetails [subProduct=" + subProduct + ", purity=" + purity + ", productSize=" + productSize
				+ ", quantity=" + quantity + ", grossWeight=" + grossWeight + ", stoneWeight=" + stoneWeight
				+ ", netWeight=" + netWeight + ", touchCalc=" + touchCalc + ", touchPercent=" + touchPercent
				+ ", touchValue=" + touchValue + ", wastageOn=" + wastageOn + ", wastageValue=" + wastageValue
				+ ", pureWeight=" + pureWeight + ", mcOn=" + mcOn + ", mcValue=" + mcValue + ", rate=" + rate
				+ ", amount=" + amount + ", stoneAmount=" + stoneAmount + ", mcAmount=" + mcAmount + ", gstAmount="
				+ gstAmount + ", totalAmount=" + totalAmount + ", remarks=" + remarks + ", createdLotQuantity="
				+ createdLotQuantity + ", createdLotWeight=" + createdLotWeight + ", metalPurchase=" + metalPurchase
				+ ", stoneDetails=" + stoneDetails + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
