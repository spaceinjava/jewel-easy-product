package com.spaceinje.journal.model.inventory;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.brand.master.BrandSubProduct;

@Entity
public class BrandedDetails extends AuditEntity {

	private static final long serialVersionUID = -852829578517405518L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private BrandSubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "brandedPurchaseId", nullable = false)
	private BrandedPurchase brandedPurchase;

	private Double quantity;

	private Double weight;

	private Double rate;

	private Double amount;

	private Double gstAmount;

	private Double totalAmount;

	private String remarks;
	
	private Double createdLotQuantity;

	private Double createdLotWeight;

	public BrandSubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(BrandSubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BrandedPurchase getBrandedPurchase() {
		return brandedPurchase;
	}

	public void setBrandedPurchase(BrandedPurchase brandedPurchase) {
		this.brandedPurchase = brandedPurchase;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	@Override
	public String toString() {
		return "BrandedDetails [subProduct=" + subProduct + ", brandedPurchase=" + brandedPurchase + ", quantity="
				+ quantity + ", weight=" + weight + ", rate=" + rate + ", amount=" + amount + ", gstAmount=" + gstAmount
				+ ", totalAmount=" + totalAmount + ", remarks=" + remarks + ", createdLotQuantity=" + createdLotQuantity
				+ ", createdLotWeight=" + createdLotWeight + "]";
	}



}
