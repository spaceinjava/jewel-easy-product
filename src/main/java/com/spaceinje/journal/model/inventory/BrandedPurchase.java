package com.spaceinje.journal.model.inventory;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class BrandedPurchase extends AuditEntity {

	private static final long serialVersionUID = -3474061683707193791L;

	private String purchaseType;

	private String invoiceNo;

	private LocalDate invoiceDate;

	private String partyId;
	
	private String receiptNo;

	@OneToMany(mappedBy = "brandedPurchase", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BrandedDetails> brandedDetails;

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Set<BrandedDetails> getBrandedDetails() {
		return brandedDetails;
	}

	public void setBrandedDetails(Set<BrandedDetails> brandedDetails) {
		this.brandedDetails = brandedDetails;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	@Override
	public String toString() {
		return "BrandedPurchase [purchaseType=" + purchaseType + ", invoiceNo=" + invoiceNo + ", invoiceDate="
				+ invoiceDate + ", partyId=" + partyId + ", receiptNo=" + receiptNo + ", brandedDetails="
				+ brandedDetails + "]";
	}

	

}
