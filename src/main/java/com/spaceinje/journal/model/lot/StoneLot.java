package com.spaceinje.journal.model.lot;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneClarity;
import com.spaceinje.products.model.stone.master.StoneColor;
import com.spaceinje.products.model.stone.master.StonePolish;
import com.spaceinje.products.model.stone.master.StoneSize;

@Entity
public class StoneLot extends AuditEntity {

	private static final long serialVersionUID = -3953523797622590592L;

	private String purchaseType;

	private String receiptNo;

	private LocalDate purchaseDate;

	private String partyId;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	// this field is used to identify this stone details is for which metal lot
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "metalLotId", nullable = true)
	private MetalLot metalLot;

	private String uom;

	private Double quantity;

	private Double weightPerGram;

	private Double weightPerKarat;

	private String employeeId;

	private String remarks;

	// this field is used for both gemstone purchase and metal purchases stone
	// details as well
	private String purchaseId;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public MetalLot getMetalLot() {
		return metalLot;
	}

	public void setMetalLot(MetalLot metalLot) {
		this.metalLot = metalLot;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	@Override
	public String toString() {
		return "StoneLotDetails [purchaseType=" + purchaseType + ", receiptNo=" + receiptNo + ", purchaseDate="
				+ purchaseDate + ", partyId=" + partyId + ", lotNo=" + lotNo + ", stone=" + stone + ", stoneSize="
				+ stoneSize + ", stoneColor=" + stoneColor + ", stoneClarity=" + stoneClarity + ", stonePolish="
				+ stonePolish + ", metalLot=" + metalLot + ", uom=" + uom + ", quantity=" + quantity
				+ ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", employeeId="
				+ employeeId + ", remarks=" + remarks + ", purchaseId=" + purchaseId + ", createdBarCodeQuantity="
				+ createdBarCodeQuantity + ", createdBarCodeWeight=" + createdBarCodeWeight + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
