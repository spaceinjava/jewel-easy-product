package com.spaceinje.journal.model.lot;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.brand.master.BrandSubProduct;
import com.spaceinje.products.model.product.master.SubProduct;

@Entity
public class BrandedLot extends AuditEntity {

	private static final long serialVersionUID = -852829578517405518L;

	private String purchaseType;

	private String receiptNo;

	private LocalDate purchaseDate;

	private String partyId;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "brandSubProductId", nullable = false)
	private BrandSubProduct brandSubProduct;

	private Double quantity;

	private Double weight;

	private String remarks;

	private String employeeId;

	private String purchaseId;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	public BrandSubProduct getBrandSubProduct() {
		return brandSubProduct;
	}

	public void setBrandSubProduct(BrandSubProduct brandSubProduct) {
		this.brandSubProduct = brandSubProduct;
	}

	@Override
	public String toString() {
		return "BrandedLot [purchaseType=" + purchaseType + ", receiptNo=" + receiptNo + ", purchaseDate="
				+ purchaseDate + ", partyId=" + partyId + ", lotNo=" + lotNo + ", brandSubProduct=" + brandSubProduct
				+ ", quantity=" + quantity + ", weight=" + weight + ", remarks=" + remarks + ", employeeId="
				+ employeeId + ", purchaseId=" + purchaseId + ", createdBarCodeQuantity=" + createdBarCodeQuantity
				+ ", createdBarCodeWeight=" + createdBarCodeWeight + "]";
	}

	
}
