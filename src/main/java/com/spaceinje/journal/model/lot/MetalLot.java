package com.spaceinje.journal.model.lot;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.product.master.ProductSize;
import com.spaceinje.products.model.product.master.SubProduct;

@Entity
public class MetalLot extends AuditEntity {

	private static final long serialVersionUID = -8528295785174055148L;

	private String purchaseType;

	private String receiptNo;

	private LocalDate purchaseDate;

	private String partyId;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private SubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "productSizeId", nullable = true)
	private ProductSize productSize;

	private Double quantity;

	private Double grossWeight;

	private Double stoneWeight;

	private Double netWeight;

	private String employeeId;

	private String remarks;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	// this field refers to productDetailsId of metal purchase
	private String purchaseId;

	@OneToMany(mappedBy = "metalLot", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneLot> stoneLot;

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public SubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Set<StoneLot> getStoneLot() {
		return stoneLot;
	}

	public void setStoneLot(Set<StoneLot> stoneLot) {
		this.stoneLot = stoneLot;
	}

	@Override
	public String toString() {
		return "MetalLot [purchaseType=" + purchaseType + ", receiptNo=" + receiptNo + ", purchaseDate=" + purchaseDate
				+ ", partyId=" + partyId + ", lotNo=" + lotNo + ", subProduct=" + subProduct + ", purity=" + purity
				+ ", productSize=" + productSize + ", quantity=" + quantity + ", grossWeight=" + grossWeight
				+ ", stoneWeight=" + stoneWeight + ", netWeight=" + netWeight + ", employeeId=" + employeeId
				+ ", remarks=" + remarks + ", createdBarCodeQuantity=" + createdBarCodeQuantity
				+ ", createdBarCodeWeight=" + createdBarCodeWeight + ", purchaseId=" + purchaseId + ", stoneLot="
				+ stoneLot + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()="
				+ getTenantCode() + "]";
	}

}
