package com.spaceinje.journal.model.barcode;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicUpdate;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.product.master.ProductSize;
import com.spaceinje.products.model.product.master.SubProduct;

@Entity
@DynamicUpdate
public class MetalBarcode extends AuditEntity {

	private static final long serialVersionUID = -4249578383444428072L;

	// anticipating to use this field itself as input for both BarCode and QRCode
	private String code;

	private String partyId;

	private LocalDate lotDate;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private SubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productSizeId", nullable = false)
	private ProductSize productSize;

	private Double quantity;

	private Double grossWeight;

	private Double stoneWeight;

	private Double netWeight;

	private Double rate;

	private Double wastagePercent;

	private Double dirWastage;

	private Double mcPerGram;

	private Double mcDir;

	private String design;

	private String remarks;

	private String photoUrl;

	@OneToMany(mappedBy = "metalBarcode", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<MetalBarcodeStoneDetails> stoneDetails;

	private Boolean isItemSoldOut;

	private Boolean isAddedToStock;

	// used to differentiate between BarCode and QRCode
	private String labelType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public SubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getWastagePercent() {
		return wastagePercent;
	}

	public void setWastagePercent(Double wastagePercent) {
		this.wastagePercent = wastagePercent;
	}

	public Double getDirWastage() {
		return dirWastage;
	}

	public void setDirWastage(Double dirWastage) {
		this.dirWastage = dirWastage;
	}

	public Double getMcPerGram() {
		return mcPerGram;
	}

	public void setMcPerGram(Double mcPerGram) {
		this.mcPerGram = mcPerGram;
	}

	public Double getMcDir() {
		return mcDir;
	}

	public void setMcDir(Double mcDir) {
		this.mcDir = mcDir;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Set<MetalBarcodeStoneDetails> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<MetalBarcodeStoneDetails> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "MetalBarcode [code=" + code + ", partyId=" + partyId + ", lotDate=" + lotDate + ", lotNo=" + lotNo
				+ ", subProduct=" + subProduct + ", purity=" + purity + ", productSize=" + productSize + ", quantity="
				+ quantity + ", grossWeight=" + grossWeight + ", stoneWeight=" + stoneWeight + ", netWeight="
				+ netWeight + ", rate=" + rate + ", wastagePercent=" + wastagePercent + ", dirWastage=" + dirWastage
				+ ", mcPerGram=" + mcPerGram + ", mcDir=" + mcDir + ", design=" + design + ", remarks=" + remarks
				+ ", photoUrl=" + photoUrl + ", stoneDetails=" + stoneDetails + ", isItemSoldOut=" + isItemSoldOut
				+ ", isAddedToStock=" + isAddedToStock + ", labelType=" + labelType + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
