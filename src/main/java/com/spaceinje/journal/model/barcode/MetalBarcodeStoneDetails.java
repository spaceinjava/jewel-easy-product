package com.spaceinje.journal.model.barcode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicUpdate;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneClarity;
import com.spaceinje.products.model.stone.master.StoneColor;
import com.spaceinje.products.model.stone.master.StonePolish;
import com.spaceinje.products.model.stone.master.StoneSize;

@Entity
@DynamicUpdate
public class MetalBarcodeStoneDetails extends AuditEntity {

	private static final long serialVersionUID = -3953523797622590592L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	private String uom;

	private Double quantity;

	private Double weightPerGram;

	private Double weightPerKarat;

	private Double rate;

	private Double amount;

	private Boolean noWeight;

	// this field helps in identifying, for which stone details in metal lot,
	// barcode is created
	private String stoneLotId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "metalBarcodeId", nullable = false)
	private MetalBarcode metalBarcode;

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getNoWeight() {
		return noWeight;
	}

	public void setNoWeight(Boolean noWeight) {
		this.noWeight = noWeight;
	}

	public String getStoneLotId() {
		return stoneLotId;
	}

	public void setStoneLotId(String stoneLotId) {
		this.stoneLotId = stoneLotId;
	}

	public MetalBarcode getMetalBarcode() {
		return metalBarcode;
	}

	public void setMetalBarcode(MetalBarcode metalBarcode) {
		this.metalBarcode = metalBarcode;
	}

	@Override
	public String toString() {
		return "MetalBarcodeStoneDetails [stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor
				+ ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", uom=" + uom + ", quantity="
				+ quantity + ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", rate="
				+ rate + ", amount=" + amount + ", noWeight=" + noWeight + ", stoneLotId=" + stoneLotId
				+ ", metalBarcode=" + metalBarcode + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
