package com.spaceinje.journal.model.barcode;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicUpdate;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneClarity;
import com.spaceinje.products.model.stone.master.StoneColor;
import com.spaceinje.products.model.stone.master.StonePolish;
import com.spaceinje.products.model.stone.master.StoneSize;

@Entity
@DynamicUpdate
public class StoneBarcode extends AuditEntity {

	private static final long serialVersionUID = 3847092065736917660L;

	// anticipating to use this field itself as input for both BarCode and QRCode
	private String code;

	private String partyId;

	private LocalDate lotDate;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	private String uom;

	private Double quantity;

	private Double weightPerGram;

	private Double weightPerKarat;

	private Double rate;

	private Double amount;

	private String remarks;

	private String photoUrl;

	private Boolean isAddedToStock;

	private Boolean isItemSoldOut;

	// used to differentiate between BarCode and QRCode
	private String labelType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "StoneBarcode [code=" + code + ", partyId=" + partyId + ", lotDate=" + lotDate + ", lotNo=" + lotNo
				+ ", stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor + ", stoneClarity="
				+ stoneClarity + ", stonePolish=" + stonePolish + ", uom=" + uom + ", quantity=" + quantity
				+ ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", rate=" + rate
				+ ", amount=" + amount + ", remarks=" + remarks + ", photoUrl=" + photoUrl + ", isAddedToStock="
				+ isAddedToStock + ", isItemSoldOut=" + isItemSoldOut + ", labelType=" + labelType + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
