package com.spaceinje.journal.model.barcode;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicUpdate;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.brand.master.BrandSubProduct;
import com.spaceinje.products.model.other.master.Purity;

@Entity
@DynamicUpdate
public class BrandedBarcode extends AuditEntity {

	private static final long serialVersionUID = 10915944057293968L;

	// anticipating to use this field itself as input for both BarCode and QRCode
	private String code;

	private String partyId;

	private LocalDate lotDate;

	private String lotNo;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private BrandSubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	private Double quantity;

	private Double weight;

	private Double rate;

	private Double amount;

	private String remarks;

	private String photoUrl;

	private Boolean isItemSoldOut;

	private Boolean isAddedToStock;
	
	// used to differentiate between BarCode and QRCode
	private String labelType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public BrandSubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(BrandSubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "BrandedBarcode [code=" + code + ", partyId=" + partyId + ", lotDate=" + lotDate + ", lotNo=" + lotNo
				+ ", subProduct=" + subProduct + ", purity=" + purity + ", quantity=" + quantity + ", weight=" + weight
				+ ", rate=" + rate + ", amount=" + amount + ", remarks=" + remarks + ", photoUrl=" + photoUrl
				+ ", isItemSoldOut=" + isItemSoldOut + ", isAddedToStock=" + isAddedToStock + ", labelType=" + labelType
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode()
				+ "]";
	}

}
