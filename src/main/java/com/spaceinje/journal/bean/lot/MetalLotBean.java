package com.spaceinje.journal.bean.lot;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SubProductBean;

public class MetalLotBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	@NotEmpty(message = "purchase type can't be null")
	private String purchaseType;

	@NotEmpty(message = "receipt number can't be null")
	private String receiptNo;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate purchaseDate;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	private String lotNo;

	@NotNull(message = "subProduct can't be null")
	private SubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	@NotNull(message = "productSize can't be null")
	private ProductSizeBean productSize;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "grossWeight can't be null")
	private Double grossWeight;

	@NotNull(message = "stoneWeight can't be null")
	private Double stoneWeight;

	@NotNull(message = "netWeight can't be null")
	private Double netWeight;

	@NotEmpty(message = "employeeId can't be null")
	private String employeeId;

	private String remarks;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	// this field refers to productDetailsId of metal purchase
	@NotEmpty(message = "purchaseId can't be null")
	private String purchaseId;

	private Set<StoneLotBean> stoneLot;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public SubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public ProductSizeBean getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSizeBean productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Set<StoneLotBean> getStoneLot() {
		return stoneLot;
	}

	public void setStoneLot(Set<StoneLotBean> stoneLot) {
		this.stoneLot = stoneLot;
	}

	@Override
	public String toString() {
		return "MetalLotBean [tenantCode=" + tenantCode + ", purchaseType=" + purchaseType + ", receiptNo=" + receiptNo
				+ ", purchaseDate=" + purchaseDate + ", partyId=" + partyId + ", lotNo=" + lotNo + ", subProduct="
				+ subProduct + ", purity=" + purity + ", productSize=" + productSize + ", quantity=" + quantity
				+ ", grossWeight=" + grossWeight + ", stoneWeight=" + stoneWeight + ", netWeight=" + netWeight
				+ ", employeeId=" + employeeId + ", remarks=" + remarks + ", createdBarCodeQuantity="
				+ createdBarCodeQuantity + ", createdBarCodeWeight=" + createdBarCodeWeight + ", purchaseId="
				+ purchaseId + ", stoneLot=" + stoneLot + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ "]";
	}

}
