package com.spaceinje.journal.bean.lot;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.bean.product.master.SubProductBean;

public class BrandedLotBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	@NotEmpty(message = "purchase type can't be null")
	private String purchaseType;

	@NotEmpty(message = "receipt number can't be null")
	private String receiptNo;

	@NotNull(message = "invoice date can't be null")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate purchaseDate;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	private String lotNo;

	@NotNull(message = "BrandProduct can't be null")
	private BrandSubProductBean brandSubProduct;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "weight can't be null")
	private Double weight;

	@NotNull(message = "employeeId can't be null")
	private String employeeId;

	private String remarks;

	@NotNull(message = "purchaseId can't be null")
	private String purchaseId;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	public BrandSubProductBean getBrandSubProduct() {
		return brandSubProduct;
	}

	public void setBrandSubProduct(BrandSubProductBean brandSubProduct) {
		this.brandSubProduct = brandSubProduct;
	}

	@Override
	public String toString() {
		return "BrandedLotBean [tenantCode=" + tenantCode + ", purchaseType=" + purchaseType + ", receiptNo="
				+ receiptNo + ", purchaseDate=" + purchaseDate + ", partyId=" + partyId + ", lotNo=" + lotNo
				+ ", brandSubProduct=" + brandSubProduct + ", quantity=" + quantity + ", weight=" + weight
				+ ", employeeId=" + employeeId + ", remarks=" + remarks + ", purchaseId=" + purchaseId
				+ ", createdBarCodeQuantity=" + createdBarCodeQuantity + ", createdBarCodeWeight="
				+ createdBarCodeWeight + "]";
	}

	

	
}
