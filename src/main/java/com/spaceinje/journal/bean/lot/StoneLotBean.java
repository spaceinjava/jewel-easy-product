package com.spaceinje.journal.bean.lot;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;

public class StoneLotBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	@NotEmpty(message = "purchase type can't be null")
	private String purchaseType;

	@NotEmpty(message = "receipt number can't be null")
	private String receiptNo;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate purchaseDate;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	private String lotNo;

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotNull(message = "uom can't be null")
	private String uom;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "weightPerGram can't be null")
	private Double weightPerGram;

	private Double weightPerKarat;

	@NotEmpty(message = "employeeId can't be null")
	private String employeeId;

	private String remarks;

	@NotNull(message = "purchaseId can't be null")
	private String purchaseId;

	private Double createdBarCodeQuantity;

	private Double createdBarCodeWeight;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Double getCreatedBarCodeQuantity() {
		return createdBarCodeQuantity;
	}

	public void setCreatedBarCodeQuantity(Double createdBarCodeQuantity) {
		this.createdBarCodeQuantity = createdBarCodeQuantity;
	}

	public Double getCreatedBarCodeWeight() {
		return createdBarCodeWeight;
	}

	public void setCreatedBarCodeWeight(Double createdBarCodeWeight) {
		this.createdBarCodeWeight = createdBarCodeWeight;
	}

	@Override
	public String toString() {
		return "StoneLotDetailsBean [tenantCode=" + tenantCode + ", purchaseType=" + purchaseType + ", receiptNo="
				+ receiptNo + ", purchaseDate=" + purchaseDate + ", partyId=" + partyId + ", lotNo=" + lotNo
				+ ", stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor + ", stoneClarity="
				+ stoneClarity + ", stonePolish=" + stonePolish + ", uom=" + uom + ", quantity=" + quantity
				+ ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", employeeId="
				+ employeeId + ", remarks=" + remarks + ", purchaseId=" + purchaseId + ", createdBarCodeQuantity="
				+ createdBarCodeQuantity + ", createdBarCodeWeight=" + createdBarCodeWeight + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
