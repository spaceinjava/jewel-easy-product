package com.spaceinje.journal.bean.barcode;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.bean.other.master.PurityBean;

public class BrandedBarcodeBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	private String code;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	@NotNull(message = "lotDate can't be null")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate lotDate;

	@NotEmpty(message = "lotNo can't be null")
	private String lotNo;

	@NotNull(message = "subProduct can't be null")
	private BrandSubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	private Double quantity;

	private Double weight;

	@NotNull(message = "rate can't be null")
	private Double rate;

	private Double amount;

	private String remarks;

	private String photoUrl;

	private Boolean isAddedToStock;

	private Boolean isItemSoldOut;

	private String labelType;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public BrandSubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(BrandSubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "BrandedBarcodeBean [tenantCode=" + tenantCode + ", code=" + code + ", partyId=" + partyId + ", lotDate="
				+ lotDate + ", lotNo=" + lotNo + ", subProduct=" + subProduct + ", purity=" + purity + ", quantity="
				+ quantity + ", weight=" + weight + ", rate=" + rate + ", amount=" + amount + ", remarks=" + remarks
				+ ", photoUrl=" + photoUrl + ", isAddedToStock=" + isAddedToStock + ", isItemSoldOut=" + isItemSoldOut
				+ ", labelType=" + labelType + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
