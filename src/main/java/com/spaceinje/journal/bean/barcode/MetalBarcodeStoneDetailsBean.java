package com.spaceinje.journal.bean.barcode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;

public class MetalBarcodeStoneDetailsBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotEmpty(message = "uom can't be null")
	private String uom;

	private Double quantity;

	private Double weightPerGram;

	private Double weightPerKarat;

	@NotNull(message = "rate can't be null")
	private Double rate;

	private Double amount;

	private Boolean noWeight;

	// this field helps in identifying, for which stone details in metal lot,
	// barcode is created
	@NotNull(message = "stoneLotId can't be null")
	private String stoneLotId;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getNoWeight() {
		return noWeight;
	}

	public void setNoWeight(Boolean noWeight) {
		this.noWeight = noWeight;
	}

	public String getStoneLotId() {
		return stoneLotId;
	}

	public void setStoneLotId(String stoneLotId) {
		this.stoneLotId = stoneLotId;
	}

	@Override
	public String toString() {
		return "MetalBarcodeStoneDetailsBean [tenantCode=" + tenantCode + ", stone=" + stone + ", stoneSize="
				+ stoneSize + ", stoneColor=" + stoneColor + ", stoneClarity=" + stoneClarity + ", stonePolish="
				+ stonePolish + ", uom=" + uom + ", quantity=" + quantity + ", weightPerGram=" + weightPerGram
				+ ", weightPerKarat=" + weightPerKarat + ", rate=" + rate + ", amount=" + amount + ", noWeight="
				+ noWeight + ", stoneLotId=" + stoneLotId + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}

}
