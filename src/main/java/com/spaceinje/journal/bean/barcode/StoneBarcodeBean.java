package com.spaceinje.journal.bean.barcode;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;

public class StoneBarcodeBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	private String code;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	@NotNull(message = "lotDate can't be null")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate lotDate;

	@NotEmpty(message = "lotNo can't be null")
	private String lotNo;

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotEmpty(message = "uom can't be null")
	private String uom;

	private Double quantity;

	private Double weightPerGram;

	private Double weightPerKarat;

	@NotNull(message = "rate can't be null")
	private Double rate;

	private Double amount;

	private String remarks;

	private String photoUrl;

	private Boolean isAddedToStock;

	private Boolean isItemSoldOut;

	private String labelType;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "StoneBarcodeBean [tenantCode=" + tenantCode + ", code=" + code + ", partyId=" + partyId + ", lotDate="
				+ lotDate + ", lotNo=" + lotNo + ", stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor="
				+ stoneColor + ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", uom=" + uom
				+ ", quantity=" + quantity + ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat
				+ ", rate=" + rate + ", amount=" + amount + ", remarks=" + remarks + ", photoUrl=" + photoUrl
				+ ", isAddedToStock=" + isAddedToStock + ", isItemSoldOut=" + isItemSoldOut + ", labelType=" + labelType
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
