package com.spaceinje.journal.bean.barcode;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SubProductBean;

public class MetalBarcodeBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	private String code;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	@NotNull(message = "lotDate can't be null")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate lotDate;

	@NotEmpty(message = "lotNo can't be null")
	private String lotNo;

	@NotNull(message = "subProduct can't be null")
	private SubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	@NotNull(message = "productSize can't be null")
	private ProductSizeBean productSize;

	private Double quantity;

	private Double grossWeight;

	private Double stoneWeight;

	private Double netWeight;

	@NotNull(message = "rate can't be null")
	private Double rate;

	@NotNull(message = "wastagePercent can't be null")
	private Double wastagePercent;

	@NotNull(message = "dirWastage can't be null")
	private Double dirWastage;

	@NotNull(message = "mcPerGram can't be null")
	private Double mcPerGram;

	@NotNull(message = "mcDir can't be null")
	private Double mcDir;

	private String design;

	private String remarks;

	private String photoUrl;

	private Set<MetalBarcodeStoneDetailsBean> stoneDetails;

	private Boolean isAddedToStock;

	private Boolean isItemSoldOut;

	private String labelType;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public LocalDate getLotDate() {
		return lotDate;
	}

	public void setLotDate(LocalDate lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public SubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public ProductSizeBean getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSizeBean productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getWastagePercent() {
		return wastagePercent;
	}

	public void setWastagePercent(Double wastagePercent) {
		this.wastagePercent = wastagePercent;
	}

	public Double getDirWastage() {
		return dirWastage;
	}

	public void setDirWastage(Double dirWastage) {
		this.dirWastage = dirWastage;
	}

	public Double getMcPerGram() {
		return mcPerGram;
	}

	public void setMcPerGram(Double mcPerGram) {
		this.mcPerGram = mcPerGram;
	}

	public Double getMcDir() {
		return mcDir;
	}

	public void setMcDir(Double mcDir) {
		this.mcDir = mcDir;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Set<MetalBarcodeStoneDetailsBean> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<MetalBarcodeStoneDetailsBean> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	public Boolean getIsAddedToStock() {
		return isAddedToStock;
	}

	public void setIsAddedToStock(Boolean isAddedToStock) {
		this.isAddedToStock = isAddedToStock;
	}

	public Boolean getIsItemSoldOut() {
		return isItemSoldOut;
	}

	public void setIsItemSoldOut(Boolean isItemSoldOut) {
		this.isItemSoldOut = isItemSoldOut;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}

	@Override
	public String toString() {
		return "MetalBarcodeBean [tenantCode=" + tenantCode + ", code=" + code + ", partyId=" + partyId + ", lotDate="
				+ lotDate + ", lotNo=" + lotNo + ", subProduct=" + subProduct + ", purity=" + purity + ", productSize="
				+ productSize + ", quantity=" + quantity + ", grossWeight=" + grossWeight + ", stoneWeight="
				+ stoneWeight + ", netWeight=" + netWeight + ", rate=" + rate + ", wastagePercent=" + wastagePercent
				+ ", dirWastage=" + dirWastage + ", mcPerGram=" + mcPerGram + ", mcDir=" + mcDir + ", design=" + design
				+ ", remarks=" + remarks + ", photoUrl=" + photoUrl + ", stoneDetails=" + stoneDetails
				+ ", isAddedToStock=" + isAddedToStock + ", isItemSoldOut=" + isItemSoldOut + ", labelType=" + labelType
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
