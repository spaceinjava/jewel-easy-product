package com.spaceinje.journal.bean.inventory;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.spaceinje.products.bean.base.BaseBean;

public class GemStonePurchaseBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;

	@NotEmpty(message = "purchase type can't be null")
	private String purchaseType;

	private String invoiceNo;

	@NotNull(message = "invoice date can't be null")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate invoiceDate;
	
	private String receiptNo;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	@NotNull(message = "Stone Details can't be null")
	private Set<StoneDetailsBean> stoneDetails;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Set<StoneDetailsBean> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<StoneDetailsBean> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	@Override
	public String toString() {
		return "GemStonePurchaseBean [tenantCode=" + tenantCode + ", purchaseType=" + purchaseType + ", invoiceNo="
				+ invoiceNo + ", invoiceDate=" + invoiceDate + ", receiptNo=" + receiptNo + ", partyId=" + partyId
				+ ", stoneDetails=" + stoneDetails + "]";
	}

	

	


}
