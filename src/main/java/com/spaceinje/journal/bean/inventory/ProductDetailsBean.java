package com.spaceinje.journal.bean.inventory;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SubProductBean;

public class ProductDetailsBean extends BaseBean {

	@NotNull(message = "subProduct can't be null")
	private SubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	@NotNull(message = "productSize can't be null")
	private ProductSizeBean productSize;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "grossWeight can't be null")
	private Double grossWeight;

	@NotNull(message = "stoneWeight can't be null")
	private Double stoneWeight;

	@NotNull(message = "netWeight can't be null")
	private Double netWeight;

	@NotNull(message = "touchCalc can't be null")
	private Boolean touchCalc;

	private Double touchPercent;

	private Double touchValue;

	private String wastageOn;

	private Double wastageValue;

	@NotNull(message = "pureWeight can't be null")
	private Double pureWeight;

	private String mcOn;

	private Double mcValue;

	@NotNull(message = "rate can't be null")
	private Double rate;

	@NotNull(message = "amount can't be null")
	private Double amount;

	@NotNull(message = "stoneAmount can't be null")
	private Double stoneAmount;

	@NotNull(message = "mcAmount can't be null")
	private Double mcAmount;

	@NotNull(message = "gstAmount can't be null")
	private Double gstAmount;

	@NotNull(message = "totalAmount can't be null")
	private Double totalAmount;

	private String remarks;

	private Double createdLotQuantity;

	private Double createdLotWeight;

	@NotNull(message = "Stone Details can't be null")
	private Set<StoneDetailsBean> stoneDetails;

	public SubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public ProductSizeBean getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSizeBean productSize) {
		this.productSize = productSize;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(Double stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public Boolean getTouchCalc() {
		return touchCalc;
	}

	public void setTouchCalc(Boolean touchCalc) {
		this.touchCalc = touchCalc;
	}

	public Double getTouchPercent() {
		return touchPercent;
	}

	public void setTouchPercent(Double touchPercent) {
		this.touchPercent = touchPercent;
	}

	public Double getTouchValue() {
		return touchValue;
	}

	public void setTouchValue(Double touchValue) {
		this.touchValue = touchValue;
	}

	public String getWastageOn() {
		return wastageOn;
	}

	public void setWastageOn(String wastageOn) {
		this.wastageOn = wastageOn;
	}

	public Double getWastageValue() {
		return wastageValue;
	}

	public void setWastageValue(Double wastageValue) {
		this.wastageValue = wastageValue;
	}

	public Double getPureWeight() {
		return pureWeight;
	}

	public void setPureWeight(Double pureWeight) {
		this.pureWeight = pureWeight;
	}

	public String getMcOn() {
		return mcOn;
	}

	public void setMcOn(String mcOn) {
		this.mcOn = mcOn;
	}

	public Double getMcValue() {
		return mcValue;
	}

	public void setMcValue(Double mcValue) {
		this.mcValue = mcValue;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getStoneAmount() {
		return stoneAmount;
	}

	public void setStoneAmount(Double stoneAmount) {
		this.stoneAmount = stoneAmount;
	}

	public Double getMcAmount() {
		return mcAmount;
	}

	public void setMcAmount(Double mcAmount) {
		this.mcAmount = mcAmount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	public Set<StoneDetailsBean> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<StoneDetailsBean> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	@Override
	public String toString() {
		return "ProductDetailsBean [subProduct=" + subProduct + ", purity=" + purity + ", productSize=" + productSize
				+ ", quantity=" + quantity + ", grossWeight=" + grossWeight + ", stoneWeight=" + stoneWeight
				+ ", netWeight=" + netWeight + ", touchCalc=" + touchCalc + ", touchPercent=" + touchPercent
				+ ", touchValue=" + touchValue + ", wastageOn=" + wastageOn + ", wastageValue=" + wastageValue
				+ ", pureWeight=" + pureWeight + ", mcOn=" + mcOn + ", mcValue=" + mcValue + ", rate=" + rate
				+ ", amount=" + amount + ", stoneAmount=" + stoneAmount + ", mcAmount=" + mcAmount + ", gstAmount="
				+ gstAmount + ", totalAmount=" + totalAmount + ", remarks=" + remarks + ", createdLotQuantity="
				+ createdLotQuantity + ", createdLotWeight=" + createdLotWeight + ", stoneDetails=" + stoneDetails
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
