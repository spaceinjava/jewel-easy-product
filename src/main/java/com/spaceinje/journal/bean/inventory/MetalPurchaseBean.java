package com.spaceinje.journal.bean.inventory;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;

public class MetalPurchaseBean extends BaseBean {

	@NotEmpty(message = "tenantCode can't be null")
	private String tenantCode;
	
	@NotEmpty(message = "purchase type can't be null")
	private String purchaseType;

	private String invoiceNo;

	@NotNull(message = "invoice date can't be null")
	private LocalDate invoiceDate;
	
	private String receiptNo;

	@NotEmpty(message = "party can't be null")
	private String partyId;

	@NotNull(message = "Product Details can't be null")
	private Set<ProductDetailsBean> productDetails;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Set<ProductDetailsBean> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(Set<ProductDetailsBean> productDetails) {
		this.productDetails = productDetails;
	}
	
	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	@Override
	public String toString() {
		return "MetalPurchaseBean [tenantCode=" + tenantCode + ", purchaseType=" + purchaseType + ", invoiceNo="
				+ invoiceNo + ", invoiceDate=" + invoiceDate + ", receiptNo=" + receiptNo + ", partyId=" + partyId
				+ ", productDetails=" + productDetails + "]";
	}



}
