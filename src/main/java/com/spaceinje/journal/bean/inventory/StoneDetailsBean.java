package com.spaceinje.journal.bean.inventory;

import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;

public class StoneDetailsBean extends BaseBean {

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "uom can't be null")
	private String uom;

	@NotNull(message = "weightPerGram can't be null")
	private Double weightPerGram;

//	@NotNull(message = "weightPerKarat can't be null")
	private Double weightPerKarat;

	@NotNull(message = "rate can't be null")
	private Double rate;

	@NotNull(message = "amount can't be null")
	private Double amount;

	private Double createdLotQuantity;

	private Double createdLotWeight;

	private Boolean isNoWeight;

	private Double gstAmount;

	private Double totalAmount;

	private String remarks;

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getWeightPerGram() {
		return weightPerGram;
	}

	public void setWeightPerGram(Double weightPerGram) {
		this.weightPerGram = weightPerGram;
	}

	public Double getWeightPerKarat() {
		return weightPerKarat;
	}

	public void setWeightPerKarat(Double weightPerKarat) {
		this.weightPerKarat = weightPerKarat;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	public Boolean getIsNoWeight() {
		return isNoWeight;
	}

	public void setIsNoWeight(Boolean isNoWeight) {
		this.isNoWeight = isNoWeight;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "StoneDetailsBean [stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor
				+ ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", quantity=" + quantity
				+ ", uom=" + uom + ", weightPerGram=" + weightPerGram + ", weightPerKarat=" + weightPerKarat + ", rate="
				+ rate + ", amount=" + amount + ", createdLotQuantity=" + createdLotQuantity + ", createdLotWeight="
				+ createdLotWeight + ", isNoWeight=" + isNoWeight + ", gstAmount=" + gstAmount + ", totalAmount="
				+ totalAmount + ", remarks=" + remarks + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ "]";
	}

}
