package com.spaceinje.journal.bean.inventory;

import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;

public class BrandedDetailsBean extends BaseBean {

	@NotNull(message = "subProduct can't be null")
	private BrandSubProductBean subProduct;

	@NotNull(message = "quantity can't be null")
	private Double quantity;

	@NotNull(message = "weight can't be null")
	private Double weight;
	@NotNull(message = "rate can't be null")
	private Double rate;

	@NotNull(message = "amount can't be null")
	private Double amount;

	@NotNull(message = "gstAmount can't be null")
	private Double gstAmount;

	@NotNull(message = "totalAmount can't be null")
	private Double totalAmount;

	@NotNull(message = "createdLotQuantity can't be null")
	private Double createdLotQuantity;

	@NotNull(message = "createdLotWeight can't be null")
	private Double createdLotWeight;
	
	private String remarks;

	public BrandSubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(BrandSubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getCreatedLotQuantity() {
		return createdLotQuantity;
	}

	public void setCreatedLotQuantity(Double createdLotQuantity) {
		this.createdLotQuantity = createdLotQuantity;
	}

	public Double getCreatedLotWeight() {
		return createdLotWeight;
	}

	public void setCreatedLotWeight(Double createdLotWeight) {
		this.createdLotWeight = createdLotWeight;
	}

	@Override
	public String toString() {
		return "BrandedDetailsBean [subProduct=" + subProduct + ", quantity=" + quantity + ", weight=" + weight
				+ ", rate=" + rate + ", amount=" + amount + ", gstAmount=" + gstAmount + ", totalAmount=" + totalAmount
				+ ", createdLotQuantity=" + createdLotQuantity + ", createdLotWeight=" + createdLotWeight + ", remarks="
				+ remarks + "]";
	}

	

}
