package com.spaceinje.journal.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;

public interface AWSService {

	public String uploadProfilePicToS3(MultipartFile multipartFile, String emp_code, String sourceName)
			throws IOException, AmazonServiceException, SdkClientException;

	public void deleteFile(String file_name) throws AmazonServiceException, SdkClientException;
}
