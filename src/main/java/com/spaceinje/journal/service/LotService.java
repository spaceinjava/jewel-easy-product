package com.spaceinje.journal.service;

import java.time.LocalDate;
import java.util.List;

import com.spaceinje.journal.bean.lot.BrandedLotBean;
import com.spaceinje.journal.bean.lot.MetalLotBean;
import com.spaceinje.journal.bean.lot.StoneLotBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface LotService {

	/*
	 * services for Lot
	 */
	public ResponseCachet fetchAllRcNos(String tenantCode, String supplierName, String lotType,
			String purchaseType, LocalDate fromDate, LocalDate toDate);

	public ResponseCachet fetchAllMetalDetails(String tenantCode, String rcNo, String purchaseType);

	public ResponseCachet saveOrUpdateMetalLot(List<MetalLotBean> beans);

	public ResponseCachet saveOrUpdateBrandedLot(List<BrandedLotBean> beans);

	public ResponseCachet saveOrUpdateGemStoneLot(List<StoneLotBean> bean);

	public ResponseCachet fetchLotDetailsByType(String tenantCode, String lotType, String partyId, String mainGroupId,
			LocalDate fromDate, LocalDate toDate);

}
