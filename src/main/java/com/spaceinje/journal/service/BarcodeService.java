package com.spaceinje.journal.service;

import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.journal.bean.barcode.BrandedBarcodeBean;
import com.spaceinje.journal.bean.barcode.MetalBarcodeBean;
import com.spaceinje.journal.bean.barcode.StoneBarcodeBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface BarcodeService {

	/*
	 * service for generating MetalBarcode
	 */
	public ResponseCachet saveMetalBarcode(MetalBarcodeBean bean, MultipartFile file);

	/*
	 * service for generating BrandedBarcode
	 */
	public ResponseCachet saveBrandedBarcode(BrandedBarcodeBean bean, MultipartFile file);

	/*
	 * service for generating StoneBarcode
	 */
	public ResponseCachet saveStoneBarcode(StoneBarcodeBean bean, MultipartFile file);

}
