package com.spaceinje.journal.service;

import java.util.Set;

import javax.validation.Valid;

import com.spaceinje.journal.bean.inventory.BrandedPurchaseBean;
import com.spaceinje.journal.bean.inventory.GemStonePurchaseBean;
import com.spaceinje.journal.bean.inventory.MetalPurchaseBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface InventoryService {

	/*
	 * services for metal purchase
	 */
	public ResponseCachet saveOrUpdateMetalPurchase(MetalPurchaseBean bean);

	public ResponseCachet fetchAllMetalPurchases(String tenantCode);

	public ResponseCachet deleteMetalPurchases(Set<String> metalPurchaseIds);

	/*
	 * services for Branded purchase
	 */
	public ResponseCachet saveOrUpdateBrandedPurchase(BrandedPurchaseBean bean);

	public ResponseCachet fetchAllBrandedPurchases(String tenantCode);

	public ResponseCachet deleteBrandedPurchases(Set<String> brandedPurchaseIds);

	/*
	 * services for GemStone purchase
	 */
	public ResponseCachet saveOrUpdateGemStonePurchase(@Valid GemStonePurchaseBean bean);

	public ResponseCachet fetchAllGemStonePurchases(String tenantCode);

	public ResponseCachet deleteGemStonePurchases(Set<String> gemStonePurchaseIds);

}
