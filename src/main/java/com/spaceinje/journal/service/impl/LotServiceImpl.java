package com.spaceinje.journal.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.spaceinje.journal.bean.inventory.BrandedPurchaseBean;
import com.spaceinje.journal.bean.inventory.GemStonePurchaseBean;
import com.spaceinje.journal.bean.inventory.MetalPurchaseBean;
import com.spaceinje.journal.bean.lot.BrandedLotBean;
import com.spaceinje.journal.bean.lot.MetalLotBean;
import com.spaceinje.journal.bean.lot.StoneLotBean;
import com.spaceinje.journal.model.inventory.BrandedDetails;
import com.spaceinje.journal.model.inventory.BrandedPurchase;
import com.spaceinje.journal.model.inventory.GemStonePurchase;
import com.spaceinje.journal.model.inventory.MetalPurchase;
import com.spaceinje.journal.model.inventory.ProductDetails;
import com.spaceinje.journal.model.inventory.StoneDetails;
import com.spaceinje.journal.model.lot.BrandedLot;
import com.spaceinje.journal.model.lot.MetalLot;
import com.spaceinje.journal.model.lot.StoneLot;
import com.spaceinje.journal.repository.inventory.BrandedDetailsRepository;
import com.spaceinje.journal.repository.inventory.BrandedPurchaseRepository;
import com.spaceinje.journal.repository.inventory.GemStonePurchaseRepository;
import com.spaceinje.journal.repository.inventory.MetalPurchaseRepository;
import com.spaceinje.journal.repository.inventory.ProductDetailsRepository;
import com.spaceinje.journal.repository.inventory.StoneDetailsRepository;
import com.spaceinje.journal.repository.lot.BrandedLotRepository;
import com.spaceinje.journal.repository.lot.MetalLotRepository;
import com.spaceinje.journal.repository.lot.StoneLotRepository;
import com.spaceinje.journal.service.LotService;
import com.spaceinje.journal.specification.LotSpecs;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.util.LotConstants;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("LotService")
public class LotServiceImpl implements LotService {

	private static final Logger log = LoggerFactory.getLogger(LotServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private MetalPurchaseRepository metalPurchaseRepository;

	@Autowired
	private BrandedPurchaseRepository brandedPurchaseRepository;

	@Autowired
	private GemStonePurchaseRepository gemStonePurchaseRepository;

	@Autowired
	private ProductDetailsRepository productDetailsRepository;

	@Autowired
	private BrandedDetailsRepository brandedDetailsRepository;

	@Autowired
	private StoneDetailsRepository stoneDetailsRepository;

	@Autowired
	private MetalLotRepository metalLotRepository;

	@Autowired
	private BrandedLotRepository brandedLotRepository;

	@Autowired
	private StoneLotRepository stoneLotRepository;

	@Autowired
	private ProductConstants constants;

	@Override
	@Transactional
	public ResponseCachet<?> saveOrUpdateMetalLot(List<MetalLotBean> beans) {
		ResponseCachet<List<MetalLotBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<MetalLot> metalLots = new ArrayList<MetalLot>();
			beans.forEach(bean -> {
				final MetalLot metalLot = modelMapper.map(bean, MetalLot.class);
				metalLot.setCreatedBarCodeQuantity(0.00);
				metalLot.setCreatedBarCodeWeight(0.00);
				if (Optional.ofNullable(metalLot.getStoneLot()).isPresent()) {
					metalLot.getStoneLot().stream().forEach(stone -> {
						Optional<StoneDetails> purchasedStoneDetails = stoneDetailsRepository
								.findById(stone.getPurchaseId());
						if (purchasedStoneDetails.isPresent()) {
							purchasedStoneDetails.get().setCreatedLotQuantity(Double
									.sum(purchasedStoneDetails.get().getCreatedLotQuantity(), stone.getQuantity()));
							purchasedStoneDetails.get().setCreatedLotWeight(Double
									.sum(purchasedStoneDetails.get().getCreatedLotWeight(), stone.getWeightPerGram()));
							stoneDetailsRepository.save(purchasedStoneDetails.get());
						}
						stone.setCreatedBarCodeQuantity(0.00);
						stone.setCreatedBarCodeWeight(0.00);
						stone.setMetalLot(metalLot);
					});
				}
				metalLot.setLotNo(generateLotNo(metalLot.getTenantCode()));
				metalLots.add(metalLot);
			});

			Optional<ProductDetails> purchasedProductDetails = productDetailsRepository
					.findById(beans.get(0).getPurchaseId());
			if (purchasedProductDetails.isPresent()) {
				purchasedProductDetails.get()
						.setCreatedLotQuantity(Double.sum(purchasedProductDetails.get().getCreatedLotQuantity(),
								beans.stream().mapToDouble(MetalLotBean::getQuantity).sum()));
				purchasedProductDetails.get()
						.setCreatedLotWeight(Double.sum(purchasedProductDetails.get().getCreatedLotWeight(),
								beans.stream().mapToDouble(MetalLotBean::getGrossWeight).sum()));
				productDetailsRepository.save(purchasedProductDetails.get());
			}
			cachet.setData(metalLotRepository.saveAll(metalLots).stream()
					.map(ele -> modelMapper.map(ele, MetalLotBean.class)).collect(Collectors.toList()));
			log.debug("lot created successfully for metal purchaseId: {}", beans.get(0).getPurchaseId());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("lot created successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving metal Lot: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	@Transactional
	public ResponseCachet<?> saveOrUpdateBrandedLot(List<BrandedLotBean> beans) {
		ResponseCachet<List<BrandedLotBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandedLot> brandedLots = new ArrayList<BrandedLot>();
			beans.forEach(bean -> {
				final BrandedLot brandedLot = modelMapper.map(bean, BrandedLot.class);
				brandedLot.setCreatedBarCodeQuantity(0.00);
				brandedLot.setCreatedBarCodeWeight(0.00);
				brandedLot.setLotNo(generateLotNo(brandedLot.getTenantCode()));
				brandedLots.add(brandedLot);
			});

			Optional<BrandedDetails> purchasedBrandDetails = brandedDetailsRepository
					.findById(beans.get(0).getPurchaseId());
			if (purchasedBrandDetails.isPresent()) {
				purchasedBrandDetails.get()
						.setCreatedLotQuantity(Double.sum(purchasedBrandDetails.get().getCreatedLotQuantity(),
								beans.stream().mapToDouble(BrandedLotBean::getQuantity).sum()));
				purchasedBrandDetails.get()
						.setCreatedLotWeight(Double.sum(purchasedBrandDetails.get().getCreatedLotWeight(),
								beans.stream().mapToDouble(BrandedLotBean::getWeight).sum()));
				brandedDetailsRepository.save(purchasedBrandDetails.get());
			}
			cachet.setData(brandedLotRepository.saveAll(brandedLots).stream()
					.map(ele -> modelMapper.map(ele, BrandedLotBean.class)).collect(Collectors.toList()));
			log.debug("lot created successfully for branded purchaseId : {}", beans.get(0).getPurchaseId());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("lot created successfully");
			return cachet;

		} catch (Exception e) {
			log.error("Exception while saving branded Lot: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	@Transactional
	public ResponseCachet<?> saveOrUpdateGemStoneLot(List<StoneLotBean> beans) {
		ResponseCachet<List<StoneLotBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneLot> stoneLots = new ArrayList<StoneLot>();
			beans.forEach(bean -> {
				final StoneLot stoneLot = modelMapper.map(bean, StoneLot.class);
				stoneLot.setCreatedBarCodeQuantity(0.00);
				stoneLot.setCreatedBarCodeWeight(0.00);
				stoneLot.setLotNo(generateLotNo(stoneLot.getTenantCode()));
				stoneLots.add(stoneLot);
			});

			Optional<StoneDetails> purchasedStoneDetails = stoneDetailsRepository
					.findById(beans.get(0).getPurchaseId());
			if (purchasedStoneDetails.isPresent()) {
				purchasedStoneDetails.get()
						.setCreatedLotQuantity(Double.sum(purchasedStoneDetails.get().getCreatedLotQuantity(),
								beans.stream().mapToDouble(StoneLotBean::getQuantity).sum()));
				purchasedStoneDetails.get()
						.setCreatedLotWeight(Double.sum(purchasedStoneDetails.get().getCreatedLotWeight(),
								beans.stream().mapToDouble(StoneLotBean::getWeightPerGram).sum()));
				stoneDetailsRepository.save(purchasedStoneDetails.get());
			}
			cachet.setData(stoneLotRepository.saveAll(stoneLots).stream()
					.map(ele -> modelMapper.map(ele, StoneLotBean.class)).collect(Collectors.toList()));
			log.debug("lot created successfully for gemstone purchaseId : {}", beans.get(0).getPurchaseId());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("lot created successfully");
			return cachet;

		} catch (Exception e) {
			log.error("Exception while saving GemStone Lot: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public synchronized ResponseCachet<List<?>> fetchAllRcNos(String tenantCode, String supplierName,
			String lotType, String purchaseType, LocalDate fromDate, LocalDate toDate) {
		ResponseCachet<List<?>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (Optional.ofNullable(fromDate).isPresent() && !Optional.ofNullable(toDate).isPresent()) {
			toDate = fromDate;
		}
		if (Optional.ofNullable(toDate).isPresent() && !Optional.ofNullable(fromDate).isPresent()) {
			fromDate = toDate;
		}
		try {
			List<Map<String, String>> invoiceList = new ArrayList<Map<String, String>>();
			if (lotType.equalsIgnoreCase(LotConstants.METAL.toString())) {
				List<MetalPurchase> metalList = metalPurchaseRepository.findAll(
						CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.puchaseTypeSpec(purchaseType))
								.and(LotSpecs.supplierNameSpec(supplierName)).or(CommonSpecs.dateRangeSpec("createdDate", fromDate, toDate)));
				metalList.forEach(metal -> {
					Map<String, String> map = new HashMap<String, String>();
					map.put("invoiceNo", metal.getInvoiceNo());
					map.put("recieptNo", metal.getReceiptNo());
					invoiceList.add(map);
				});

			} else if (lotType.equalsIgnoreCase(LotConstants.BRANDED.toString())) {
				List<BrandedPurchase> brandedList = brandedPurchaseRepository.findAll(
						CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.puchaseTypeSpec(purchaseType))
								.and(LotSpecs.supplierNameSpec(supplierName)).or(CommonSpecs.dateRangeSpec("createdDate", fromDate, toDate)));
				brandedList.forEach(branded -> {
					Map<String, String> map = new HashMap<String, String>();
					map.put("invoiceNo", branded.getInvoiceNo());
					map.put("recieptNo", branded.getReceiptNo());
					invoiceList.add(map);
				});
			} else {
				List<GemStonePurchase> gemList = gemStonePurchaseRepository.findAll(
						CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.puchaseTypeSpec(purchaseType))
								.and(LotSpecs.supplierNameSpec(supplierName)).or(CommonSpecs.dateRangeSpec("createdDate", fromDate, toDate)));
				gemList.forEach(gems -> {
					Map<String, String> map = new HashMap<String, String>();
					map.put("invoiceNo", gems.getInvoiceNo());
					map.put("recieptNo", gems.getReceiptNo());
					invoiceList.add(map);
				});

			}
			if (CollectionUtils.isEmpty(invoiceList)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No " + lotType + " purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No " + lotType + " purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all " + lotType + " purchases");
			cachet.setData(invoiceList);
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved " + lotType + " purchases");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all " + lotType + " purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public synchronized ResponseCachet<?> fetchAllMetalDetails(String tenantCode, String rcNo, String lotType) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setData("");
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (lotType.equalsIgnoreCase(LotConstants.METAL.toString())) {
				Optional<MetalPurchase> metalPurchase = metalPurchaseRepository
						.findOne(CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.rcNoSpec(rcNo)));
				if (metalPurchase.isPresent()) {
					cachet.setData(modelMapper.map(metalPurchase.get(), MetalPurchaseBean.class));
				}

			} else if (lotType.equalsIgnoreCase(LotConstants.BRANDED.toString())) {
				Optional<BrandedPurchase> brandedPurchase = brandedPurchaseRepository
						.findOne(CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.rcNoSpec(rcNo)));
				if (brandedPurchase.isPresent()) {
					cachet.setData(modelMapper.map(brandedPurchase.get(), BrandedPurchaseBean.class));
				}
			} else {
				Optional<GemStonePurchase> gemStonePurchase = gemStonePurchaseRepository
						.findOne(CommonSpecs.findByTenantCodeSpec(tenantCode).and(LotSpecs.rcNoSpec(rcNo)));
				if (gemStonePurchase.isPresent()) {
					cachet.setData(modelMapper.map(gemStonePurchase.get(), GemStonePurchaseBean.class));
				}
			}
			if (cachet.getData().equals("")) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No " + lotType + " purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No " + lotType + " purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all " + lotType + " purchases");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved " + lotType + " purchases");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all " + lotType + " purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

	private String generateLotNo(String tenantCode) {
		// TODO Auto-generated method stub
		Optional<String> lotId = Optional.ofNullable(metalLotRepository.getLotId(tenantCode));
		if (!lotId.isPresent()) {
			return "LOT" + "0000001";
		} else {
			int id = Integer.valueOf(lotId.get().replace("LOT", ""));
			return "LOT" + StringUtils.leftPad(String.valueOf(id + 1), 7, "0");
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized ResponseCachet fetchLotDetailsByType(String tenantCode, String lotType, String partyId,
			String mainGroupId, LocalDate fromDate, LocalDate toDate) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setData("");
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (Optional.ofNullable(fromDate).isPresent() && !Optional.ofNullable(toDate).isPresent()) {
			toDate = fromDate;
		}
		if (Optional.ofNullable(toDate).isPresent() && !Optional.ofNullable(fromDate).isPresent()) {
			fromDate = toDate;
		}
		try {
			Specification lotDetailsSpecification = CommonSpecs.findByTenantCodeSpec(tenantCode);
			if (StringUtils.isNotEmpty(partyId)) {
				lotDetailsSpecification = lotDetailsSpecification.and(CommonSpecs.partyIdSpec(partyId));
			}
			if (Optional.ofNullable(fromDate).isPresent() && Optional.ofNullable(toDate).isPresent()) {
				lotDetailsSpecification = lotDetailsSpecification.and(CommonSpecs.dateRangeSpec("createdDate", fromDate, toDate));
			}
			if (lotType.equalsIgnoreCase(LotConstants.METAL.toString())) {
				if (StringUtils.isNotEmpty(mainGroupId)) {
					lotDetailsSpecification = lotDetailsSpecification
							.and(LotSpecs.mainGroupBySubProductSpec(mainGroupId));
				}
				List<MetalLot> metalLotList = metalLotRepository.findAll(lotDetailsSpecification);
				cachet.setData(metalLotList.stream().map(ele -> modelMapper.map(ele, MetalLotBean.class))
						.collect(Collectors.toList()));
			} else if (lotType.equalsIgnoreCase(LotConstants.BRANDED.toString())) {
				if (StringUtils.isNotEmpty(mainGroupId)) {
					lotDetailsSpecification = lotDetailsSpecification
							.and(LotSpecs.mainGroupByBrandSubProductSpec(mainGroupId));
				}
				List<BrandedLot> brandedLotList = brandedLotRepository.findAll(lotDetailsSpecification);
				cachet.setData(brandedLotList.stream().map(ele -> modelMapper.map(ele, BrandedLotBean.class))
						.collect(Collectors.toList()));
			} else {
				List<StoneLot> stoneLotList = stoneLotRepository.findAll(lotDetailsSpecification);
				stoneLotList = stoneLotList.stream().filter(st -> st.getLotNo() != null).collect(Collectors.toList());
				cachet.setData(stoneLotList.stream().map(ele -> modelMapper.map(ele, StoneLotBean.class))
						.collect(Collectors.toList()));
			}
			if (cachet.getData().equals("")) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No " + lotType + " purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No " + lotType + " purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all " + lotType + " purchases");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved " + lotType + " purchases");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all " + lotType + " purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
