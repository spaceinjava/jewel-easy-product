package com.spaceinje.journal.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.journal.bean.inventory.BrandedPurchaseBean;
import com.spaceinje.journal.bean.inventory.GemStonePurchaseBean;
import com.spaceinje.journal.bean.inventory.MetalPurchaseBean;
import com.spaceinje.journal.model.inventory.BrandedDetails;
import com.spaceinje.journal.model.inventory.BrandedPurchase;
import com.spaceinje.journal.model.inventory.GemStonePurchase;
import com.spaceinje.journal.model.inventory.MetalPurchase;
import com.spaceinje.journal.model.inventory.StoneDetails;
import com.spaceinje.journal.repository.inventory.BrandedPurchaseRepository;
import com.spaceinje.journal.repository.inventory.GemStonePurchaseRepository;
import com.spaceinje.journal.repository.inventory.MetalPurchaseRepository;
import com.spaceinje.journal.service.InventoryService;
import com.spaceinje.journal.specification.InventorySpecs;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService {

	private static final Logger log = LoggerFactory.getLogger(InventoryServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private MetalPurchaseRepository metalPurchaseRepository;

	@Autowired
	private BrandedPurchaseRepository brandedPurchaseRepository;

	@Autowired
	private GemStonePurchaseRepository gemStonePurchaseRepository;

	@Autowired
	private ProductConstants constants;

	@Override
	public ResponseCachet<?> saveOrUpdateMetalPurchase(MetalPurchaseBean bean) {
		ResponseCachet<MetalPurchaseBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of metal purchase with given invoice number");
			bean.setReceiptNo(generateRcNo(bean.getTenantCode(), MetalPurchaseBean.class));
			Optional<MetalPurchase> retrievedMetalPurchase = metalPurchaseRepository
					.findOne(CommonSpecs.findByTenantCodeSpec(bean.getTenantCode())
							.and(InventorySpecs.findByrecieptNoSpec(bean.getReceiptNo())));
			if (retrievedMetalPurchase.isPresent() && !retrievedMetalPurchase.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Invoice Number already exists");
				log.debug("Invoice Number already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			final MetalPurchase metalPurchase = modelMapper.map(bean, MetalPurchase.class);
			metalPurchase.getProductDetails().stream().forEach(product -> {				
				product.setCreatedLotQuantity(0.00);
				product.setCreatedLotWeight(0.00);
				product.setTouchPercent(product.getTouchPercent() == null ? 0.00 : product.getTouchPercent());
				product.setTouchValue(product.getTouchValue() == null ? 0.00 : product.getTouchValue());
				product.getStoneDetails().stream().forEach(stone -> {
					stone.setCreatedLotQuantity(0.00);
					stone.setCreatedLotWeight(0.00);
					stone.setProductDetails(product);
				});
				product.setMetalPurchase(metalPurchase);
			});
			metalPurchaseRepository.save(metalPurchase);
			log.debug("successfully saved purchase with invoice : {}", metalPurchase.getInvoiceNo());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("metal purchase with invoice " + bean.getInvoiceNo() + " saved successfully");
			cachet.setData(modelMapper.map(metalPurchase, MetalPurchaseBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving metal purchase: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<MetalPurchaseBean>> fetchAllMetalPurchases(String tenantCode) {
		ResponseCachet<List<MetalPurchaseBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<MetalPurchase> list = metalPurchaseRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No metal purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No metal purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all metal purchases");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved metal purchases");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, MetalPurchaseBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all metal purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteMetalPurchases(Set<String> metalPurchaseIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
//			if (metalPurchaseRepository.metalPurchaseHasOrphans(metalPurchaseIds)) {
//				log.debug("metal purchase(s) has children data associated with given Id's");
//				cachet.setStatus(constants.getFAILURE());
//				cachet.setMessage("Metal Purchase(s) can't be deleted, as they are already in use.!");
//				return cachet;
//			}
			metalPurchaseRepository.deleteMetalPurchases(metalPurchaseIds);
			log.debug("deleted metal purchase(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted metal purchase(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting metal purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateBrandedPurchase(BrandedPurchaseBean bean) {
		ResponseCachet<BrandedPurchaseBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of branded purchase with given invoice number");
			bean.setReceiptNo(generateRcNo(bean.getTenantCode() , BrandedPurchaseBean.class));
			Optional<BrandedPurchase> retrievedBrandedPurchase = brandedPurchaseRepository
					.findOne(CommonSpecs.findByTenantCodeSpec(bean.getTenantCode())
							.and(InventorySpecs.findByrecieptNoSpec(bean.getReceiptNo())));
			if (retrievedBrandedPurchase.isPresent() && !retrievedBrandedPurchase.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Invoice Number already exists");
				log.debug("Invoice Number already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			BrandedPurchase brandedPurchase = modelMapper.map(bean, BrandedPurchase.class);
			Set<BrandedDetails> bdetails = brandedPurchase.getBrandedDetails();
			for (BrandedDetails bd : bdetails) {
				bd.setCreatedLotQuantity(0.00);
				bd.setCreatedLotWeight(0.00);
				bd.setBrandedPurchase(brandedPurchase);
			}
			brandedPurchase.setBrandedDetails(bdetails);
			brandedPurchase = brandedPurchaseRepository.save(brandedPurchase);
			log.debug("successfully saved purchase with invoice : {}", brandedPurchase.getInvoiceNo());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("branded purchase with invoice " + bean.getInvoiceNo() + " saved successfully");
			cachet.setData(modelMapper.map(brandedPurchase, BrandedPurchaseBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving Branded purchase: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandedPurchaseBean>> fetchAllBrandedPurchases(String tenantCode) {
		ResponseCachet<List<BrandedPurchaseBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandedPurchase> list = brandedPurchaseRepository
					.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No brnaded purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No brnaded purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all brnaded purchases");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brnaded purchases");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, BrandedPurchaseBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all metal purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteBrandedPurchases(Set<String> brandedPurchaseIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			brandedPurchaseRepository.deleteBrandedPurchases(brandedPurchaseIds);
			log.debug("deleted branded purchase(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted branded purchase(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting branded purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateGemStonePurchase(@Valid GemStonePurchaseBean bean) {
		// TODO Auto-generated method stub
		ResponseCachet<GemStonePurchaseBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of GemStone purchase with given invoice number");
			bean.setReceiptNo(generateRcNo(bean.getTenantCode() , GemStonePurchaseBean.class));
			Optional<GemStonePurchase> retrievedGemStonePurchase = gemStonePurchaseRepository
					.findOne(CommonSpecs.findByTenantCodeSpec(bean.getTenantCode())
							.and(InventorySpecs.findByrecieptNoSpec(bean.getReceiptNo())));
			if (retrievedGemStonePurchase.isPresent()
					&& !retrievedGemStonePurchase.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Invoice Number already exists");
				log.debug("Invoice Number already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			GemStonePurchase gemStonePurchase = modelMapper.map(bean, GemStonePurchase.class);
			Set<StoneDetails> gsDetails = gemStonePurchase.getStoneDetails();
			for (StoneDetails gs : gsDetails) {
				gs.setGemStonePurchase(gemStonePurchase);
				gs.setCreatedLotQuantity(0.00);
				gs.setCreatedLotWeight(0.00);
			}
			gemStonePurchase.setStoneDetails(gsDetails);
			gemStonePurchase = gemStonePurchaseRepository.save(gemStonePurchase);
			log.debug("successfully saved purchase with invoice : {}", gemStonePurchase.getInvoiceNo());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("gemStone purchase with invoice " + bean.getInvoiceNo() + " saved successfully");
			cachet.setData(modelMapper.map(gemStonePurchase, GemStonePurchaseBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving gemStone purchase: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

	@Override
	public ResponseCachet<List<GemStonePurchaseBean>> fetchAllGemStonePurchases(String tenantCode) {
		// TODO Auto-generated method stub
		ResponseCachet<List<GemStonePurchaseBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<GemStonePurchase> list = gemStonePurchaseRepository
					.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No GemStone purchases found");
				cachet.setData(new ArrayList<>());
				log.debug("No GemStone purchases found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all GemStone purchases");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved GemStone purchases");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, GemStonePurchaseBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all GemStone purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}

	}

	@Override
	public ResponseCachet<?> deleteGemStonePurchases(Set<String> gemStonePurchaseIds) {
		// TODO Auto-generated method stub
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			gemStonePurchaseRepository.deleteGemStonePurchases(gemStonePurchaseIds);
			log.debug("deleted gemStone purchase(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted gemStone purchase(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting gemStone purchases: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}
	
	private String generateRcNo(String tenantCode, Class<?> clazz) throws ClassNotFoundException {

		if (Class.forName(clazz.getName()).isInstance(new MetalPurchaseBean())) {
			Optional<MetalPurchase> metalPurchase = metalPurchaseRepository
					.findTopByTenantCodeOrderByCreatedDateDesc(tenantCode);
			return genarate(metalPurchase.isPresent() , metalPurchase.isPresent() == false ? null : metalPurchase.get().getReceiptNo());

		} else if (Class.forName(clazz.getName()).isInstance(new BrandedPurchaseBean())) {
			Optional<BrandedPurchase> brandedPurchase = brandedPurchaseRepository
					.findTopByTenantCodeOrderByCreatedDateDesc(tenantCode);
			System.out.println(brandedPurchase.isPresent());
			return genarate(brandedPurchase.isPresent() , brandedPurchase.isPresent() == false ? null : brandedPurchase.get().getReceiptNo());

		} else {
			Optional<GemStonePurchase> gemStonePurchase = gemStonePurchaseRepository
					.findTopByTenantCodeOrderByCreatedDateDesc(tenantCode);
			return genarate(gemStonePurchase.isPresent(), gemStonePurchase.isPresent() == false ? null : gemStonePurchase.get().getReceiptNo());

		}

	}

	private String genarate(boolean present , String rcNo) {
		// TODO Auto-generated method stub
		if (!present) {
			rcNo = "RC" + "0000001";
		} else {
			int id = Integer.valueOf(rcNo.replace("RC", ""));
			rcNo = "RC" + StringUtils.leftPad(String.valueOf(id + 1), 7, "0");

		}
		return rcNo;
	}


}
