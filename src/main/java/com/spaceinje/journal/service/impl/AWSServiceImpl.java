package com.spaceinje.journal.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.spaceinje.journal.service.AWSService;

@Service("awsService")
public class AWSServiceImpl implements AWSService {

	private static final Logger log = LoggerFactory.getLogger(AWSServiceImpl.class);

	@Autowired
	private AmazonS3 amazonS3;

	@Value("${aws.s3.bucket.name}")
	String awsS3BucketName;

	@Value("${aws.s3.bucket.url}")
	String awsS3BucketUrl;

	/**
	 * service to upload profile pic image to S3 bucket
	 * 
	 * @return url of the image
	 * 
	 * @throws IOException            - if the file was not found or unavailable
	 * 
	 * @throws AmazonServiceException - while uploading image to s3 bucket
	 */
	@Override
	public String uploadProfilePicToS3(MultipartFile multipartFile, String tenentCode, String sourceName)
			throws IOException, AmazonServiceException, SdkClientException {
		log.debug("converting multipart file to file");
		final File file = new File(multipartFile.getOriginalFilename());
		final FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.write(multipartFile.getBytes());
		fileOutputStream.close();
		log.debug("uploading file to S3 bucket");
		final String fileName = tenentCode + file.getName();
		String bucketName = awsS3BucketName + "/" + tenentCode + "/" + sourceName;
		final PutObjectRequest request = new PutObjectRequest(bucketName, fileName, file);
		request.withCannedAcl(CannedAccessControlList.PublicRead);
		amazonS3.putObject(request);

		// deleting the local image
		file.delete();
		return amazonS3.getUrl(bucketName, fileName).toExternalForm();
	}

	@Override
	public void deleteFile(String fileName) throws AmazonServiceException, SdkClientException {
		log.debug("deleting file : {}, from S3", fileName);
		fileName = fileName.replace(awsS3BucketUrl, "");
		final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(awsS3BucketName, fileName);
		amazonS3.deleteObject(deleteObjectRequest);
		log.debug("deleted file successfully");
	}

}
