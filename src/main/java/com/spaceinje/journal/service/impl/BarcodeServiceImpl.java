package com.spaceinje.journal.service.impl;

import java.util.Date;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.spaceinje.journal.bean.barcode.BrandedBarcodeBean;
import com.spaceinje.journal.bean.barcode.MetalBarcodeBean;
import com.spaceinje.journal.bean.barcode.StoneBarcodeBean;
import com.spaceinje.journal.model.barcode.BrandedBarcode;
import com.spaceinje.journal.model.barcode.MetalBarcode;
import com.spaceinje.journal.model.barcode.StoneBarcode;
import com.spaceinje.journal.model.lot.BrandedLot;
import com.spaceinje.journal.model.lot.MetalLot;
import com.spaceinje.journal.model.lot.StoneLot;
import com.spaceinje.journal.repository.barcode.BrandedBarcodeRepository;
import com.spaceinje.journal.repository.barcode.MetalBarcodeRepository;
import com.spaceinje.journal.repository.barcode.StoneBarcodeRepository;
import com.spaceinje.journal.repository.lot.BrandedLotRepository;
import com.spaceinje.journal.repository.lot.MetalLotRepository;
import com.spaceinje.journal.repository.lot.StoneLotRepository;
import com.spaceinje.journal.service.AWSService;
import com.spaceinje.journal.service.BarcodeService;
import com.spaceinje.products.util.BarCodeGenarator;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Service("barcodeService")
public class BarcodeServiceImpl implements BarcodeService {

	private static final Logger log = LoggerFactory.getLogger(BarcodeServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private MetalLotRepository metalLotRepository;

	@Autowired
	private MetalBarcodeRepository metalBarcodeRepository;

	@Autowired
	private BrandedLotRepository brandedLotRepository;

	@Autowired
	private BrandedBarcodeRepository brandedBarcodeRepository;

	@Autowired
	private StoneLotRepository stoneLotRepository;

	@Autowired
	private StoneBarcodeRepository stoneBarcodeRepository;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private AWSService awsService;

	@Autowired
	private BarCodeGenarator barCodeGenarator;

	@Override
	public ResponseCachet<?> saveMetalBarcode(MetalBarcodeBean bean, MultipartFile file) {
		ResponseCachet<MetalBarcodeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether barcode can be created for given lot number");
			Optional<MetalLot> retrievedLot = metalLotRepository.findByLotNoAndTenantCode(bean.getLotNo(),
					bean.getTenantCode());
			if (retrievedLot.isPresent()) {
				MetalLot lot = retrievedLot.get();
				if (validateQuantityAndWeight((lot.getQuantity() - lot.getCreatedBarCodeQuantity()), bean.getQuantity(),
						(lot.getGrossWeight() - lot.getCreatedBarCodeWeight()), bean.getGrossWeight())) {
					// generating code
					Optional<String> code = fetchLastGeneratedCode(bean.getTenantCode(), new Date());
					bean.setCode(barCodeGenarator.generateBarCode(code, bean.getTenantCode(),
							lot.getSubProduct().getProduct().getShortCode()));

					// pushing image to S3 bucket
					if (file != null && !file.isEmpty()) {
						String photoUrl = "";
						try {
							photoUrl = awsService.uploadProfilePicToS3(file, bean.getTenantCode(), "Barcode/Metal");
						} catch (Exception ex) {
							log.error("error occured while uploading profile picture" + ex.getMessage());
						}
						bean.setPhotoUrl(photoUrl);
					}
					bean.setIsAddedToStock(Boolean.FALSE);
					bean.setIsItemSoldOut(Boolean.FALSE);
					final MetalBarcode barcode = modelMapper.map(bean, MetalBarcode.class);
					barcode.getStoneDetails().stream().forEach(ele -> ele.setMetalBarcode(barcode));
					metalBarcodeRepository.save(barcode);
					lot.setCreatedBarCodeQuantity(Double.sum(lot.getCreatedBarCodeQuantity(), barcode.getQuantity()));
					lot.setCreatedBarCodeWeight(Double.sum(lot.getCreatedBarCodeWeight(), barcode.getGrossWeight()));
					metalLotRepository.save(lot);
					log.debug("successfully saved metal with code : {}", barcode.getCode());
					cachet.setStatus(constants.getSUCCESS());
					cachet.setMessage("metal with code " + bean.getCode() + " saved successfully");
					cachet.setData(modelMapper.map(barcode, MetalBarcodeBean.class));
					return cachet;
				} else {
					cachet.setStatus(constants.getFAILURE());
					cachet.setMessage("Quantity / Weight not allowed to generate barcode");
					log.debug("Quantity / Weight not allowed to generate barcode");
					return cachet;
				}
			} else {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Lot Number doesn't exists");
				log.debug("Lot Number doesn't exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
		} catch (Exception e) {
			log.error("Exception while creating metal barcode: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveBrandedBarcode(BrandedBarcodeBean bean, MultipartFile file) {
		ResponseCachet<BrandedBarcodeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether barcode can be created for given lot number");
			Optional<BrandedLot> retrievedLot = brandedLotRepository.findByLotNoAndTenantCode(bean.getLotNo(),
					bean.getTenantCode());
			if (retrievedLot.isPresent()) {
				BrandedLot lot = retrievedLot.get();
				if (validateQuantityAndWeight((lot.getQuantity() - lot.getCreatedBarCodeQuantity()), bean.getQuantity(),
						(lot.getWeight() - lot.getCreatedBarCodeWeight()), bean.getWeight())) {
					// generating code
					Optional<String> code = fetchLastGeneratedCode(bean.getTenantCode(), new Date());
					bean.setCode(barCodeGenarator.generateBarCode(code, bean.getTenantCode(),
							lot.getBrandSubProduct().getBrandProduct().getShortCode()));

					// pushing image to S3 bucket
					if (file != null && !file.isEmpty()) {
						String photoUrl = "";
						try {
							photoUrl = awsService.uploadProfilePicToS3(file, bean.getTenantCode(), "Barcode/Branded");
						} catch (Exception ex) {
							log.error("error occured while uploading profile picture" + ex.getMessage());
						}
						bean.setPhotoUrl(photoUrl);
					}
					bean.setIsAddedToStock(Boolean.FALSE);
					bean.setIsItemSoldOut(Boolean.FALSE);
					BrandedBarcode barcode = modelMapper.map(bean, BrandedBarcode.class);
					barcode = brandedBarcodeRepository.save(barcode);
					lot.setCreatedBarCodeQuantity(Double.sum(lot.getCreatedBarCodeQuantity(), barcode.getQuantity()));
					lot.setCreatedBarCodeWeight(Double.sum(lot.getCreatedBarCodeWeight(), barcode.getWeight()));
					brandedLotRepository.save(lot);
					log.debug("successfully saved brand with code : {}", barcode.getCode());
					cachet.setStatus(constants.getSUCCESS());
					cachet.setMessage("brand with code " + bean.getCode() + " saved successfully");
					cachet.setData(modelMapper.map(barcode, BrandedBarcodeBean.class));
					return cachet;
				} else {
					cachet.setStatus(constants.getFAILURE());
					cachet.setMessage("Quantity / Weight not allowed to generate barcode");
					log.debug("Quantity / Weight not allowed to generate barcode");
					return cachet;
				}
			} else {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Lot Number doesn't exists");
				log.debug("Lot Number doesn't exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
		} catch (Exception e) {
			log.error("Exception while creating metal barcode: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveStoneBarcode(StoneBarcodeBean bean, MultipartFile file) {
		ResponseCachet<StoneBarcodeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether barcode can be created for given lot number");
			Optional<StoneLot> retrievedLot = stoneLotRepository.findByLotNoAndTenantCode(bean.getLotNo(),
					bean.getTenantCode());
			if (retrievedLot.isPresent()) {
				StoneLot lot = retrievedLot.get();
				if (validateQuantityAndWeight((lot.getQuantity() - lot.getCreatedBarCodeQuantity()), bean.getQuantity(),
						(lot.getWeightPerGram() - lot.getCreatedBarCodeWeight()), bean.getWeightPerGram())) {
					// generating code
					Optional<String> code = fetchLastGeneratedCode(bean.getTenantCode(), new Date());
					bean.setCode(barCodeGenarator.generateBarCode(code, bean.getTenantCode(),
							lot.getStone().getShortCode()));

					// pushing image to S3 bucket
					if (file != null && !file.isEmpty()) {
						String photoUrl = "";
						try {
							photoUrl = awsService.uploadProfilePicToS3(file, bean.getTenantCode(), "Barcode/Stone");
						} catch (Exception ex) {
							log.error("error occured while uploading profile picture" + ex.getMessage());
						}
						bean.setPhotoUrl(photoUrl);
					}
					bean.setIsAddedToStock(Boolean.FALSE);
					bean.setIsItemSoldOut(Boolean.FALSE);
					StoneBarcode barcode = modelMapper.map(bean, StoneBarcode.class);
					barcode = stoneBarcodeRepository.save(barcode);
					lot.setCreatedBarCodeQuantity(Double.sum(lot.getCreatedBarCodeQuantity(), barcode.getQuantity()));
					lot.setCreatedBarCodeWeight(Double.sum(lot.getCreatedBarCodeWeight(), barcode.getWeightPerGram()));
					stoneLotRepository.save(lot);
					log.debug("successfully saved gemstone with code : {}", barcode.getCode());
					cachet.setStatus(constants.getSUCCESS());
					cachet.setMessage("gemstone with code " + bean.getCode() + " saved successfully");
					cachet.setData(modelMapper.map(barcode, StoneBarcodeBean.class));
					return cachet;
				} else {
					cachet.setStatus(constants.getFAILURE());
					cachet.setMessage("Quantity / Weight not allowed to generate barcode");
					log.debug("Quantity / Weight not allowed to generate barcode");
					return cachet;
				}
			} else {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Lot Number doesn't exists");
				log.debug("Lot Number doesn't exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
		} catch (Exception e) {
			log.error("Exception while creating gemstone barcode: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	public Optional<String> fetchLastGeneratedCode(String tenantCode, Date date) {
		return Optional.ofNullable(metalBarcodeRepository.fetchLastCodeGeneratedByTenantCode(tenantCode, date));
	}

	/**
	 * 
	 * @param pendingQty    - balance quantity, which generation should be done
	 * @param actualQty     - current/actual quantity which user is trying to
	 *                      generate
	 * @param pendingWeight - balance weight, which generation should be done
	 * @param actualWeight  - current/actual weight which user is trying to generate
	 */
	public Boolean validateQuantityAndWeight(Double pendingQty, Double actualQty, Double pendingWeight,
			Double actualWeight) {
		Boolean qtyValid = Boolean.FALSE;
		if (actualQty <= pendingQty) {
			if (actualQty < pendingQty) {
				qtyValid = Boolean.TRUE;
			} else if (actualQty.equals(pendingQty) && actualWeight.equals(pendingWeight)) {
				qtyValid = Boolean.TRUE;
			} else {
				qtyValid = Boolean.FALSE;
			}
		}
		Boolean weightValid = Boolean.FALSE;
		if (actualWeight <= pendingWeight) {
			if (actualWeight < pendingWeight) {
				weightValid = Boolean.TRUE;
			} else if (actualWeight.equals(pendingWeight) && actualQty.equals(pendingQty)) {
				weightValid = Boolean.TRUE;
			} else {
				weightValid = Boolean.FALSE;
			}
		}
		return qtyValid && weightValid;
	}

}
