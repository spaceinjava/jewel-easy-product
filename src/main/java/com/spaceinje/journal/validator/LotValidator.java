package com.spaceinje.journal.validator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.spaceinje.journal.bean.lot.BrandedLotBean;
import com.spaceinje.journal.bean.lot.MetalLotBean;
import com.spaceinje.journal.bean.lot.StoneLotBean;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class LotValidator {

	private static final Logger log = LoggerFactory.getLogger(LotValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ProductConstants constants;

	public ResponseEntity<ResponseCachet> metalLotValidator(List<MetalLotBean> beans) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(beans)) {
			log.debug("Metal Lot details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Metal Lot details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("metal Lot purchase validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> brandedLotValidator(List<BrandedLotBean> beans) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(beans)) {
			log.debug("branded Lot details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("branded Lot details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("branded Lot validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	
	public ResponseEntity<ResponseCachet> gemStoneLotValidator(List<StoneLotBean>  beans) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(beans)) {
			log.debug("gemStone Lot details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("gemStone Lot details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("gemStone Lot validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
}
