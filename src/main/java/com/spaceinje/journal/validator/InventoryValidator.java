package com.spaceinje.journal.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.spaceinje.journal.bean.inventory.BrandedPurchaseBean;
import com.spaceinje.journal.bean.inventory.GemStonePurchaseBean;
import com.spaceinje.journal.bean.inventory.MetalPurchaseBean;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class InventoryValidator {

	private static final Logger log = LoggerFactory.getLogger(InventoryValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ProductConstants constants;

	public ResponseEntity<ResponseCachet> metalPurchaseValidator(MetalPurchaseBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(bean.getProductDetails())) {
			log.debug("product details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("metal purchase validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> brandedPurchaseValidator(BrandedPurchaseBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(bean.getBrandedDetails())) {
			log.debug("branded details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("branded details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("branded purchase validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> gemStonePurchaseValidator(GemStonePurchaseBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(bean.getStoneDetails())) {
			log.debug("gemStone details can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("gemStone details can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("gemStone purchase validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
