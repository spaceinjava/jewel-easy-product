package com.spaceinje.products.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class BrandMasterValidator {
	
	private static final Logger log = LoggerFactory.getLogger(BrandMasterValidator.class);

	@Autowired
	private Tracer tracer;
	
	@Autowired
	private ProductConstants constants;

	public ResponseEntity<ResponseCachet> brandValidator(BrandBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getMainGroup().getId())) {
			log.debug("main group id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("brand params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
	public ResponseEntity<ResponseCachet> brandProductValidator(BrandProductBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getBrand().getId())) {
			log.debug("brand id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("brand id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("brand product params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
	public ResponseEntity<ResponseCachet> brandSubProductValidator(BrandSubProductBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getBrandProduct().getId())) {
			log.debug("brand product id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("brand product id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("brand sub product params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
