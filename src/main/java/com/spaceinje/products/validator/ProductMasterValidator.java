package com.spaceinje.products.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.products.bean.product.master.PartyWastageBean;
import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SaleWastageBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class ProductMasterValidator {

	private static final Logger log = LoggerFactory.getLogger(ProductMasterValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ProductConstants constants;

	public ResponseEntity<ResponseCachet> productValidator(ProductBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getMainGroup().getId())) {
			log.debug("main group id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("product params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> subProductValidator(SubProductBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getProduct().getId())) {
			log.debug("product id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("sub product params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> productSizeValidator(ProductSizeBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getProduct().getId())) {
			log.debug("product id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("product size params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> partyWastageValidator(PartyWastageBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getSubProduct().getId())) {
			log.debug("sub product can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("sub product can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(bean.getProductSize().getId())) {
			log.debug("product size can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product size can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(bean.getPurity().getId())) {
			log.debug("purity can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("purity can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("party wastage params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> saleWastageValidator(SaleWastageBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getSubProduct().getId())) {
			log.debug("sub product can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("sub product can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(bean.getProductSize().getId())) {
			log.debug("product size can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product size can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		} else if (StringUtils.isEmpty(bean.getPurity().getId())) {
			log.debug("purity can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("purity can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("sale wastage params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
