package com.spaceinje.products.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.products.bean.other.master.GSTBean;
import com.spaceinje.products.bean.other.master.HSNCodeBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.util.OtherConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class OtherMasterValidator {

	private static final Logger log = LoggerFactory.getLogger(StoneMasterValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private OtherConstants constants;

	public ResponseEntity<ResponseCachet> hsnValidator(HSNCodeBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getMainGroup().getId())) {
			log.debug("main group id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("rate params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> gstValidator(GSTBean bean) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		if (StringUtils.isEmpty(bean.getMetalType().getId())) {
			log.debug("metal type id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("metal type id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("gst params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> purityValidator(PurityBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getMainGroup().getId())) {
			log.debug("main group id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("purity params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
