package com.spaceinje.products.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.spaceinje.products.bean.stone.master.PartyStoneRateBean;
import com.spaceinje.products.bean.stone.master.SaleStoneRateBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@Component
@SuppressWarnings("rawtypes")
public class StoneMasterValidator {

	private static final Logger log = LoggerFactory.getLogger(StoneMasterValidator.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ProductConstants constants;

	public ResponseEntity<ResponseCachet> stoneGroupValidator(StoneGroupBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getStoneType().getId())) {
			log.debug("stone type can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("stone type can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("stone group validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> stoneValidator(StoneBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getStoneGroup().getId())) {
			log.debug("stone group can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("stone group can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("stone params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> stoneSizeValidator(StoneSizeBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getStoneGroup().getId())) {
			log.debug("stone group can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("stone group can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("stone size params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> partyRateValidator(PartyStoneRateBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getStone().getId())) {
			log.debug("stone can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("stone can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("party stone rate params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	public ResponseEntity<ResponseCachet> saleRateValidator(SaleStoneRateBean bean) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(bean.getStone().getId())) {
			log.debug("stone can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("stone can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("sale stone rate params validated successfully");
		log.debug("Validation Successfull");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
