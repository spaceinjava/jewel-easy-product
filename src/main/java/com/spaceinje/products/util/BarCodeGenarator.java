package com.spaceinje.products.util;

import java.time.LocalDate;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.spaceinje.journal.service.impl.BarcodeServiceImpl;

@Component
public class BarCodeGenarator {

	private static final Logger log = LoggerFactory.getLogger(BarcodeServiceImpl.class);

	@Value("${prod.barcode.generation.starts.at}")
	private String codeStartsAt;

	@Value("${prod.barcode.generation.type}")
	private Integer type;

	public String generateBarCode(Optional<String> code, String tanentCode, String shortCode) {

		LocalDate today = LocalDate.now();
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(today.getYear()).substring(2, 4));
		sb.append(today.getMonthValue());
		sb.append(today.getDayOfMonth());
		if (type == 0) {
			return sb.append(generateBarCode(code)).toString();
		} else {
			sb.append(shortCode);
			sb.append(generateBarCode(code));
			return sb.toString();
		}

	}

	private String generateBarCode(Optional<String> code) {
		// TODO Auto-generated method stub
		if (!code.isPresent()) {
			return codeStartsAt;
		} else {
			int id = Integer.valueOf(code.get().substring(code.get().length() - 5));
			return StringUtils.leftPad(String.valueOf(id + 1), 5, "0");
		}
	}
}
