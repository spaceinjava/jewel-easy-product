package com.spaceinje.products.util;

import org.springframework.stereotype.Component;

@Component
public class OtherConstants {
	
	String SUCCESS = "SUCCESS";
	String FAILURE = "FAILURE";
	String EXCEPTION = "EXCEPTION";
	
	public String getSUCCESS() {
		return SUCCESS;
	}
	public String getFAILURE() {
		return FAILURE;
	}
	public String getEXCEPTION() {
		return EXCEPTION;
	}

}
