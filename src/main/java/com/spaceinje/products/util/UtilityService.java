package com.spaceinje.products.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class UtilityService {

	/**
	 * method which rounds the given double value to 3 decimals
	 * 
	 * @param doubleValue
	 * @return 
	 * 		1. ceiled value, if input is not null <br>
	 * 		2. zero (0), if input is null
	 */
	public static Double ceilDoubleValue(Double doubleValue) {
		return Optional.ofNullable(doubleValue).isPresent()
				? new BigDecimal(doubleValue).setScale(3, RoundingMode.CEILING).doubleValue()
				: Double.valueOf(0);
	}

}
