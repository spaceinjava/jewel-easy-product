package com.spaceinje.products.specification;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes" })
public class StoneSpecs extends CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on stoneTypeId with reference as
	 * stoneType</b>
	 * 
	 * @param stoneTypeId
	 * @return
	 */
	public static Specification stoneTypeIdSpec(String stoneTypeId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("stoneType").get("id"), stoneTypeId);
	}

	/**
	 * <b>creates criteria to retrieve based on stoneGroupId with reference as
	 * stoneGroup</b>
	 * 
	 * @param stoneGroupId
	 * @return
	 */
	public static Specification stoneGroupIdSpec(String stoneGroupId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("stoneGroup").get("id"), stoneGroupId);
	}
	
	/**
	 * <b>creates criteria to retrieve based on stoneGroupId with reference as stone
	 * \nstoneGroup from stone object</b>
	 * 
	 * @param stoneGroupId
	 * @return
	 */
	public static Specification stoneGroupIdOnStoneSpec(String stoneGroupId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("stone").get("stoneGroup").get("id"), stoneGroupId);
	}
	
	/**
	 * <b>creates criteria to retrieve based on stoneId with reference as
	 * stone</b>
	 * 
	 * @param stoneId
	 * @return
	 */
	public static Specification stoneIdSpec(String stoneId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("stone").get("id"), stoneId);
	}
	
	/**
	 * <b>creates criteria to retrieve based on stoneSizeId with reference as
	 * stoneSize</b>
	 * 
	 * @param stoneId
	 * @return
	 */
	public static Specification stoneSizeIdSpec(String stoneSizeId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("stoneSize").get("id"), stoneSizeId);
	}

}
