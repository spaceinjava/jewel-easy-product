package com.spaceinje.products.specification;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes" })
public class OtherSpecs extends CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on metatTypeId with reference as
	 * metalType</b>
	 * 
	 * @param metalTypeId
	 * @return
	 */
	public static Specification metalTypeIdSpec(String metalTypeId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("metalType").get("id"), metalTypeId);
	}

	/**
	 * <b>creates criteria to retrieve based on metatTypeId with reference as
	 * metalType</b>
	 * 
	 * @param metalTypeId
	 * @return
	 */
	public static Specification metalTypeSpec(String metalType) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("metalType"), metalType);
	}

	/**
	 * <b>creates criteria to retrieve based on purity</b>
	 * 
	 * @param purity
	 * @return
	 */
	public static Specification puritySpec(double purity) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("purity"), purity);
	}

	/**
	 * <b>creates criteria to retrieve based on purityId with reference as
	 * Purity</b>
	 * 
	 * @param purityId
	 * @return
	 */
	public static Specification purityIdSpec(String purityId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("purity").get("id"), purityId);
	}

}
