package com.spaceinje.products.specification;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes" })
public class ProductSpecs extends CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on productId with reference as
	 * product</b>
	 * 
	 * @param productId
	 * @return
	 */
	public static Specification productIdSpec(String productId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("product").get("id"), productId);
	}

	/**
	 * <b>creates criteria to retrieve based on productSize</b>
	 * 
	 * @param productSize
	 * @return
	 */
	public static Specification productSizeSpec(double productSize) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("productSize"), productSize);
	}

	/**
	 * <b>creates criteria to retrieve based on productSizeId with reference as
	 * productSize</b>
	 * 
	 * @param productSizeId
	 * @return
	 */
	public static Specification productSizeIdSpec(String productSizeId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("productSize").get("id"),
				productSizeId);
	}

	/**
	 * <b>creates criteria to retrieve based on subProductId with reference as
	 * subProduct</b>
	 * 
	 * @param subProductId
	 * @return
	 */
	public static Specification subProductIdSpec(String subProductId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("subProduct").get("id"), subProductId);
	}

}
