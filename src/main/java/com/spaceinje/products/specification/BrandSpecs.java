package com.spaceinje.products.specification;

import org.springframework.data.jpa.domain.Specification;

@SuppressWarnings({ "rawtypes" })
public class BrandSpecs extends CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on brandId with reference as brand</b>
	 * 
	 * @param brandId
	 * @return
	 */
	public static Specification brandIdSpec(String brandId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("brand").get("id"), brandId);
	}
	
	/**
	 * <b>creates criteria to retrieve based on productId with reference as brandProduct</b>
	 * 
	 * @param brandProductId
	 * @return
	 */
	public static Specification brandProductIdSpec(String brandProductId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("brandProduct").get("id"), brandProductId);
	}

}
