package com.spaceinje.products.specification;

import java.time.LocalDate;

import org.springframework.data.jpa.domain.Specification;

import com.spaceinje.products.model.master.MetalType;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class CommonSpecs {

	/**
	 * <b>creates criteria to retrieve based on isDeleted false
	 * 
	 * @return
	 */
	protected static Specification isDeletedSpec() {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isDeleted"), false);
	}

	/**
	 * return criteria to retrieve based on given tenantCode
	 * 
	 * @param tenantCode
	 * @return
	 */
	private static Specification tenantCodeSpec(String tenantCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("tenantCode"), tenantCode);
	}

	/**
	 * return criteria to retrieve based on given shortCode
	 * 
	 * @param shortCode
	 * @return
	 */
	private static Specification shortCodeSpec(String shortCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("shortCode"), shortCode);
	}

	/**
	 * return criteria to retrieve based on given hsnCode
	 * 
	 * @param hsnCode
	 * @return
	 */
	private static Specification hsnCodeSpec(String hsnCode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("hsnCode"), hsnCode);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and tenantCode</b>
	 * 
	 * @param tenantCode
	 * @return
	 */
	public static Specification findByTenantCodeSpec(String tenantCode) {
		return Specification.where(tenantCodeSpec(tenantCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false and shortCode</b>
	 * 
	 * @param shortCode
	 * @return
	 */
	public static Specification findByShortCodeSpec(String shortCode) {
		return Specification.where(shortCodeSpec(shortCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false, tenantCode and
	 * shortCode</b>
	 * 
	 * @param tenantCode
	 * @param shortCode
	 * @return
	 */
	public static Specification findByTenantAndShortCodeSpec(String tenantCode, String shortCode) {
		return Specification.where(findByTenantCodeSpec(tenantCode)).and(findByShortCodeSpec(shortCode))
				.and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on metalType with reference as
	 * metalType</b>
	 * 
	 * @param metalType
	 * @return
	 */

	public static Specification findByMetalTypeSpec(MetalType metalType) {
		// TODO Auto-generated method stub
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("metalType"), metalType);
	}

	/**
	 * <b>creates criteria to retrieve based on mainGroupId with reference as
	 * mainGroup</b>
	 * 
	 * @param mainGroupId
	 * @return
	 */
	public static Specification mainGroupIdSpec(String mainGroupId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("mainGroup").get("id"), mainGroupId);
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false , hsnCode and
	 * tenantCode</b>
	 * 
	 * @param tenantCode
	 * @return
	 */
	public static Specification findByTenantAndHSNCodeSpec(String tenantCode, String hsnCode) {
		return Specification.where(tenantCodeSpec(tenantCode)).and(hsnCodeSpec(hsnCode)).and(isDeletedSpec());
	}

	/**
	 * <b>creates criteria to retrieve based on isDeleted false , gst percent and
	 * tenantCode</b>
	 * 
	 * @param tenantCode
	 * @return
	 */
	public static Specification findByTenantAndGSTSpec(String tenantCode, double percent) {
		return Specification.where(tenantCodeSpec(tenantCode)).and(gstPercentSpec(percent)).and(isDeletedSpec());
	}

	private static Specification gstPercentSpec(double percent) {
		// TODO Auto-generated method stub
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("percent"), percent);
	}

	/**
	 * <b>creates criteria to retrieve based on partyId</b>
	 * 
	 * @param partyId
	 * @return
	 */
	public static Specification partyIdSpec(String partyId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("partyId"), partyId);
	}

	/**
	 * <b>creates criteria to retrieve based on given date range (i.e., between two
	 * dates) on given field</b>
	 * 
	 * @param field
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public static Specification dateRangeSpec(String field, LocalDate fromDate, LocalDate toDate) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.between(
				criteriaBuilder.function("DATE_TRUNC", LocalDate.class, criteriaBuilder.literal("Day"),
						root.get(field)),
				criteriaBuilder.function("TO_DATE", LocalDate.class, criteriaBuilder.literal(fromDate),
						criteriaBuilder.literal("YYYY-MM-DD")),
				criteriaBuilder.function("TO_DATE", LocalDate.class, criteriaBuilder.literal(toDate),
						criteriaBuilder.literal("YYYY-MM-DD")));
	}

	/**
	 * <b>creates criteria to retrieve based on given date on given field</b>
	 * 
	 * @param field
	 * @param date
	 * @return
	 */
	public static Specification dateSpec(String field, LocalDate date) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(
				criteriaBuilder.function("DATE_TRUNC", LocalDate.class, criteriaBuilder.literal("Day"),
						root.get(field)),
				criteriaBuilder.function("TO_DATE", LocalDate.class, criteriaBuilder.literal(date),
						criteriaBuilder.literal("YYYY-MM-DD")));
	}

}
