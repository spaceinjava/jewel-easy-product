package com.spaceinje.products.resource;

import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.service.BrandMasterService;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;
import com.spaceinje.products.validator.BrandMasterValidator;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/brand")
@SuppressWarnings("rawtypes")
public class BrandMasterResource {

	private static final Logger log = LoggerFactory.getLogger(BrandMasterResource.class);

	@Autowired
	private BrandMasterService brandMasterService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private BrandMasterValidator brandMasterValidator;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateBrand(@Valid @RequestBody BrandBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = brandMasterValidator.brandValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating brand bean");
			return entity;
		}
		log.debug("calling brandMaster service");
		ResponseCachet cachet = brandMasterService.saveOrUpdateBrand(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting brandMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllBrands(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling brandMaster service");
		cachet = brandMasterService.fetchAllBrands(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllBrands service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/main-group", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchBrandsByMainGroup(@RequestParam(value = "tenant_code") String tenantCode,
			@RequestParam(value = "main_group_id") String mainGroupId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(mainGroupId)) {
			log.debug("mainGroupId can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling brandMaster service");
		cachet = brandMasterService.fetchBrandsByMainGroup(tenantCode, mainGroupId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchBrandsByMainGroup service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteBrands(@RequestParam(value = "brand_ids") Set<String> brandIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(brandIds)) {
			log.debug("BrandIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("brand ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling branchMaster service");
		cachet = brandMasterService.deleteBrands(brandIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteBrands service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateProduct(@Valid @RequestBody BrandProductBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = brandMasterValidator.brandProductValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating brand product bean");
			return entity;
		}
		log.debug("calling saveOrUpdateProduct service");
		ResponseCachet cachet = brandMasterService.saveOrUpdateProduct(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllProducts(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllProducts service");
		cachet = brandMasterService.fetchAllProducts(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
	@GetMapping(value = "/product/brand", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchProductsByBrand(@RequestParam(value = "tenant_code") String tenantCode,
			@RequestParam(value = "brand_id") String brandId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(brandId)) {
			log.debug("brandId can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("brand id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchProductsByBrand service");
		cachet = brandMasterService.fetchProductsByBrand(tenantCode, brandId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchProductsByBrand service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
	@GetMapping(value="/product/{product_id}/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchSubProductsByProduct(@RequestParam(value = "tenant_code") String tenantCode,
			@PathVariable(value = "product_id") String productId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(productId)) {
			log.debug("productId can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("productId can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchSubProductsByProduct service");
		cachet = brandMasterService.fetchSubProductsByProduct(tenantCode, productId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchSubProductsByProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteProducts(@RequestParam(value = "product_ids") Set<String> productIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(productIds)) {
			log.debug("productIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteProducts service");
		cachet = brandMasterService.deleteProducts(productIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateSubProduct(@Valid @RequestBody BrandSubProductBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = brandMasterValidator.brandSubProductValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating brand product bean");
			return entity;
		}
		log.debug("calling saveOrUpdateSubProduct service");
		ResponseCachet cachet = brandMasterService.saveOrUpdateSubProduct(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateSubProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllSubProducts(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllSubProducts service");
		cachet = brandMasterService.fetchAllSubProducts(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllSubProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteSubProducts(
			@RequestParam(value = "sub_product_ids") Set<String> subProductIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(subProductIds)) {
			log.debug("subproductIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("subproduct ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteSubProducts service");
		cachet = brandMasterService.deleteSubProducts(subProductIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteSubProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
