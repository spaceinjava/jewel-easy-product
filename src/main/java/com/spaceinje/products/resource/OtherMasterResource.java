package com.spaceinje.products.resource;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.products.bean.other.master.CounterBean;
import com.spaceinje.products.bean.other.master.FloorBean;
import com.spaceinje.products.bean.other.master.GSTBean;
import com.spaceinje.products.bean.other.master.HSNCodeBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.other.master.RateBean;
import com.spaceinje.products.service.OtherMasterService;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;
import com.spaceinje.products.validator.OtherMasterValidator;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/other")
@SuppressWarnings("rawtypes")
public class OtherMasterResource {

	private static final Logger log = LoggerFactory.getLogger(OtherMasterResource.class);

	@Autowired
	private OtherMasterService otherMasterService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private OtherMasterValidator otherMasterValidator;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@PostMapping(value = "/floor", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateFloor(@Valid @RequestBody FloorBean bean)
			throws MethodArgumentNotValidException {
		log.debug("calling otherMaster service");
		ResponseCachet cachet = otherMasterService.saveOrUpdateFloor(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/floor", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllFloors(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllFloors(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/floor", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteFloors(@RequestParam(value = "floor_ids") Set<String> floorIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(floorIds)) {
			log.debug("FloorIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("floor ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.deleteFloors(floorIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/counter", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateCounter(@Valid @RequestBody CounterBean bean)
			throws MethodArgumentNotValidException {
		log.debug("calling otherMaster service");
		ResponseCachet cachet = otherMasterService.saveOrUpdateCounter(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/counter", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllCounters(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllCounters(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/counter", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteCounters(@RequestParam(value = "counter_ids") Set<String> counterIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(counterIds)) {
			log.debug("FloorIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("floor ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.deleteCounters(counterIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/purity", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdatePurity(@Valid @RequestBody PurityBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = otherMasterValidator.purityValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating purity bean");
			return entity;
		}
		log.debug("calling saveOrUpdatePurity service");
		ResponseCachet cachet = otherMasterService.saveOrUpdatePurity(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdatePurity service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/purity", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllPurities(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllPurities service");
		cachet = otherMasterService.findAllPurities(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllPurities service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/purity", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deletePurities(@RequestParam(value = "purity_ids") Set<String> purityIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(purityIds)) {
			log.debug("purityIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("purity ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deletePurities service");
		cachet = otherMasterService.deletePurities(purityIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deletePurities service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/gst", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateGST(@Valid @RequestBody GSTBean bean)
			throws MethodArgumentNotValidException {
		log.debug("calling otherMaster service");
		ResponseEntity<ResponseCachet> entity = otherMasterValidator.gstValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating gst bean");
			return entity;
		}
		log.debug("calling saveOrUpdateGST service");
		ResponseCachet cachet = otherMasterService.saveOrUpdateGST(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/gst", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllGSTs(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllGSTs(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/gst", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteGSTs(@RequestParam(value = "gst_ids") Set<String> gstIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(gstIds)) {
			log.debug("GSTIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("gst ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.deleteGSTs(gstIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/gst/metalType", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllGSTsByMetalType(@RequestParam(value = "tenant_code") String tenantCode , @RequestParam(value = "metal_type") String metalType) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllGSTsByMetalType(tenantCode , metalType);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/hsncode", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateHSNCode(@Valid @RequestBody HSNCodeBean bean)
			throws MethodArgumentNotValidException {
		log.debug("calling otherMaster service");
		ResponseEntity<ResponseCachet> entity = otherMasterValidator.hsnValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating hsn code bean");
			return entity;
		}
		log.debug("calling saveOrUpdateHSNCode service");
		ResponseCachet cachet = otherMasterService.saveOrUpdateHSN(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/hsncode", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllHSNCodes(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllHSNs(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/hsncode", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteHSNCodes(@RequestParam(value = "hsn_ids") Set<String> hsnids) {

		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(hsnids)) {
			log.debug("FloorIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("floor ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling otherMasterService ");
		cachet = otherMasterService.deleteHSNs(hsnids);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/metalType", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllMetalTypes() {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));

		log.debug("calling otherMasterService ");
		cachet = otherMasterService.findAllMetalTypes();
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMasterService ");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/rate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateRate(@Valid @RequestBody List<RateBean> bean)
			throws MethodArgumentNotValidException {
		log.debug("calling otherMaster service");
		ResponseCachet cachet = new ResponseCachet<>();
		if (CollectionUtils.isEmpty(bean)) {
			log.debug("error while validating rate bean");
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling saveOrUpdateRate service");
		cachet = otherMasterService.saveOrUpdateRate(bean);

		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting otherMaster service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/rate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchRates(@RequestParam(value = "tenant_code") String tenantCode,
			@RequestParam(name = "groupId", required = false) String groupId,
			@RequestParam(name = "perGramRate", required = false) Double perGramRate) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}

		log.debug("calling fetchPuritiesByMainGroup service");
		cachet = otherMasterService.fetchRates(tenantCode, groupId, perGramRate);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchPuritiesByMainGroup service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
}
