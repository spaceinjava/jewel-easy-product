package com.spaceinje.products.resource;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.service.CommonMasterService;
import com.spaceinje.products.service.OtherMasterService;
import com.spaceinje.products.service.ProductMastersService;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products")
@SuppressWarnings("rawtypes")
public class CommonMasterResource {

	private static final Logger log = LoggerFactory.getLogger(CommonMasterResource.class);

	@Autowired
	private CommonMasterService commonMasterService;
	
	@Autowired
	private ProductMastersService productMastersService;
	
	@Autowired
	private OtherMasterService otherMasterService;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@GetMapping(value = "/party-type", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllPartyTypes() {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		
		log.debug("calling fetchAllPartyTypes service");
		cachet = commonMasterService.fetchAllPartyTypes();
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllPartyTypes service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@PostMapping(value = "/main-group", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateMainGroup(@Valid @RequestBody MainGroupBean bean)
			throws MethodArgumentNotValidException {
		log.debug("calling saveOrUpdateMainGroup service");
		ResponseCachet<MainGroupBean> cachet = commonMasterService.saveOrUpdateMainGroup(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting saveOrUpdateMainGroup service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@GetMapping(value = "/main-group", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet<List<MainGroupBean>>> fetchAllMainGroups(
			@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet<List<MainGroupBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet<List<MainGroupBean>>>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllMainGroups service");
		cachet = commonMasterService.fetchAllMainGroups(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet<List<MainGroupBean>>>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet<List<MainGroupBean>>>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllMainGroups service");
		return new ResponseEntity<ResponseCachet<List<MainGroupBean>>>(cachet, HttpStatus.OK);
	}
	
	@GetMapping(value = "/main-group/{group_id}/products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchProductsByMainGroup(
			@RequestParam(value = "tenant_code") String tenantCode, @PathVariable(value = "group_id") String groupId) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(groupId)) {
			log.debug("groupId can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("groupId can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchProductsByMainGroup service");
		cachet = productMastersService.fetchProductsByMainGroup(tenantCode, groupId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchProductsByMainGroup service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}
	
	@GetMapping(value = "/main-group/{group_id}/purity", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchPuritiesByMainGroup(
			@RequestParam(value = "tenant_code") String tenantCode, @PathVariable(value = "group_id") String groupId) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(groupId)) {
			log.debug("groupId can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("groupId can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchPuritiesByMainGroup service");
		cachet = otherMasterService.fetchPuritiesByMainGroup(tenantCode, groupId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchPuritiesByMainGroup service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	@DeleteMapping(value = "/main-group", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteMainGroups(
			@RequestParam(value = "main_group_ids") Set<String> mainGroupIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(mainGroupIds)) {
			log.debug("mainGroupIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("main group ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteMainGroups service");
		cachet = commonMasterService.deleteMainGroups(mainGroupIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteMainGroups service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
