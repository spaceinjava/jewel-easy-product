package com.spaceinje.products.resource;

import java.util.Set;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spaceinje.products.bean.product.master.PartyWastageBean;
import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SaleWastageBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.service.ProductMastersService;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;
import com.spaceinje.products.validator.ProductMasterValidator;

import brave.Tracer;

@RestController
@RequestMapping("/v1/products/product")
@SuppressWarnings("rawtypes")
public class ProductMastersResource {

	private static final Logger log = LoggerFactory.getLogger(ProductMastersResource.class);

	@Autowired
	private ProductConstants constants;

	@Autowired
	private ProductMastersService productMastersService;

	@Autowired
	private ProductMasterValidator productMasterValidator;

	@Autowired
	private Tracer tracer;

	@SuppressWarnings("unchecked")
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public JSONObject handleValidationExceptions(MethodArgumentNotValidException ex) {
		JSONObject errors = new JSONObject();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	/*
	 * Product Services
	 */

	/**
	 * service to save product
	 * 
	 * @param bean ProductBean
	 * 
	 * @return {@value ResponseEntity<ResponseCachet<ProductBean>>}
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateProduct(@Valid @RequestBody ProductBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = productMasterValidator.productValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating product bean");
			return entity;
		}
		log.info("calling product master service");
		ResponseCachet<ProductBean> cachet = productMastersService.saveOrUpdateProduct(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Entering saveOrUpdateProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to retrieve list of products
	 * 
	 * @param tenantCode
	 * 
	 * @return
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllProducts(@RequestParam(value = "tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllProducts service");
		cachet = productMastersService.fetchAllProducts(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to retrieve sub products based on product 
	 * @param tenantCode
	 * @param productId
	 * @return
	 */
	@GetMapping(value = "/{product_id}/sub-products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchSubProductsByProduct(
			@RequestParam(value = "tenant_code") String tenantCode,
			@PathVariable(value = "product_id") String productId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(productId)) {
			log.debug("product id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchSubProductsByProduct service");
		cachet = productMastersService.fetchSubProductsByProduct(tenantCode, productId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchSubProductsByProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to retrieve product sizes based on product
	 * @param tenantCode
	 * @param productId
	 * @return
	 */
	@GetMapping(value = "/{product_id}/sizes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchSizesByProduct(@RequestParam(value = "tenant_code") String tenantCode,
			@PathVariable(value = "product_id") String productId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		if (StringUtils.isEmpty(productId)) {
			log.debug("product id can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product id can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchSizesByProduct service");
		cachet = productMastersService.fetchSizesByProduct(tenantCode, productId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchSizesByProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete products
	 * 
	 * @param productIds
	 * 
	 * @return
	 */
	@DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteProducts(@RequestParam(value = "product_ids") Set<String> productIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(productIds)) {
			log.debug("productIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("product ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteProducts service");
		cachet = productMastersService.deleteProducts(productIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/*
	 * Sub Product Services
	 */
	/**
	 * service to save sub product
	 * 
	 * @param bean SubProductBean
	 * 
	 * @return
	 */
	@PostMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateSubProduct(@Valid @RequestBody SubProductBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = productMasterValidator.subProductValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating sub product bean");
			return entity;
		}
		log.info("calling sub product master service");
		ResponseCachet<SubProductBean> cachet = productMastersService.saveOrUpdateSubProduct(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Entering saveOrUpdateSubProduct service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to list all the sub products
	 * 
	 * @param tenantCode
	 * 
	 * @return
	 */
	@GetMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllSubProducts(@RequestParam("tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllSubProducts service");
		cachet = productMastersService.fetchAllSubProducts(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllSubProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete sub products
	 * 
	 * @param subProductIds
	 * 
	 * @return
	 */
	@DeleteMapping(value = "/sub-product", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteSubProducts(
			@RequestParam("sub_product_ids") Set<String> subProductIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(subProductIds)) {
			log.debug("subProductIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("subProduct ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteSubProducts service");
		cachet = productMastersService.deleteSubProducts(subProductIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteSubProducts service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);

	}

	/*
	 * Product Size services
	 */
	/**
	 * service to save product size
	 * 
	 * @param bean ProductSizeBean
	 * 
	 * @return
	 */
	@PostMapping(value = "/size", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateProductSize(@Valid @RequestBody ProductSizeBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = productMasterValidator.productSizeValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating sub product bean");
			return entity;
		}
		log.info("calling product size master service");
		ResponseCachet<ProductSizeBean> cachet = productMastersService.saveOrUpdateProductSize(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Entering saveOrUpdateProductSize service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to list all the products sizes
	 * 
	 * @param tenantCode
	 * 
	 * @return
	 */
	@GetMapping(value = "/size", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchAllProductSizes(@RequestParam("tenant_code") String tenantCode) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchAllProductSizes service");
		cachet = productMastersService.fetchAllProductSizes(tenantCode);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchAllProductSizes service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete products sizes
	 * 
	 * @param productSizeIds
	 * 
	 * @return
	 */
	@DeleteMapping(value = "/size", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteProductSizes(@RequestParam("size_ids") Set<String> productSizeIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(productSizeIds)) {
			log.debug("productSizeIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("productSize ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteProductSizes service");
		cachet = productMastersService.deleteProductSizes(productSizeIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteProductSizes service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);

	}

	/*
	 * Party Wastage services
	 */
	/**
	 * service to save party wastage
	 * 
	 * @param bean PartyWastageBean
	 * 
	 * @return
	 */
	@PostMapping(value = "/party-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdatePartyWastage(@Valid @RequestBody PartyWastageBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = productMasterValidator.partyWastageValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating party wastage bean");
			return entity;
		}
		log.info("calling party wastage service");
		ResponseCachet<PartyWastageBean> cachet = productMastersService.saveOrUpdatePartyWastage(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Entering saveOrUpdatePartyWastage service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to list all the party wastages
	 * 
	 * @param tenantCode
	 * 
	 * @return
	 */
	@GetMapping(value = "/party-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchPartyWastages(@RequestParam("tenant_code") String tenantCode, 
			@RequestParam(value = "party_id", required = false) String partyId,
			@RequestParam(value = "sub_product_id", required = false) String subProductId,
			@RequestParam(value = "product_size_id", required = false) String productSizeId,
			@RequestParam(value = "purity_id", required = false) String purityId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchPartyWastages service");
		cachet = productMastersService.fetchPartyWastages(tenantCode, partyId, subProductId, productSizeId, purityId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchPartyWastages service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete party wastages
	 * 
	 * @param partyWastageIds
	 * 
	 * @return
	 */
	@DeleteMapping(value = "/party-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deletePartyWastages(
			@RequestParam("party_wastage_ids") Set<String> partyWastageIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(partyWastageIds)) {
			log.debug("partyWastageIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("partyWastage ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deletePartyWastages service");
		cachet = productMastersService.deletePartyWastages(partyWastageIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deletePartyWastages service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/*
	 * Sale Wastage services
	 */
	/**
	 * service to save sale wastage
	 * 
	 * @param bean ProductSizeBean
	 * 
	 * @return
	 */
	@PostMapping(value = "/sale-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> saveOrUpdateSaleWastage(@Valid @RequestBody SaleWastageBean bean)
			throws MethodArgumentNotValidException {
		ResponseEntity<ResponseCachet> entity = productMasterValidator.saleWastageValidator(bean);
		if (entity.getStatusCode() != HttpStatus.OK) {
			log.debug("error while validating party wastage bean");
			return entity;
		}
		log.info("calling sale wastage service");
		ResponseCachet<SaleWastageBean> cachet = productMastersService.saveOrUpdateSaleWastage(bean);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Entering saveOrUpdateSaleWastage service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to list all the sale wastages
	 * 
	 * @param tenant_code
	 * 
	 * @return
	 */
	@GetMapping(value = "/sale-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> fetchSaleWastages(@RequestParam("tenant_code") String tenantCode,
			@RequestParam(value = "sub_product_id", required = false) String subProductId,
			@RequestParam(value = "product_size_id", required = false) String productSizeId,
			@RequestParam(value = "purity_id", required = false) String purityId) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (StringUtils.isEmpty(tenantCode)) {
			log.debug("Tenant code can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("Tenant code can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling fetchSaleWastages service");
		cachet = productMastersService.fetchSaleWastages(tenantCode, subProductId, productSizeId, purityId);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting fetchSaleWastages service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

	/**
	 * service to delete sale wastages
	 * 
	 * @param saleWastage_ids
	 * 
	 * @return
	 */
	@DeleteMapping(value = "/sale-wastage", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseCachet> deleteSaleWastages(
			@RequestParam("sale_wastage_ids") Set<String> saleWastageIds) {
		ResponseCachet cachet = new ResponseCachet();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		if (CollectionUtils.isEmpty(saleWastageIds)) {
			log.debug("saleWastageIds can't be empty");
			cachet.setStatus(constants.getFAILURE());
			cachet.setMessage("saleWastage ids can't be empty");
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_ACCEPTABLE);
		}
		log.debug("calling deleteSaleWastages service");
		cachet = productMastersService.deleteSaleWastages(saleWastageIds);
		if (cachet.getStatus().equals(constants.getFAILURE())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.NOT_IMPLEMENTED);
		} else if (cachet.getStatus().equals(constants.getEXCEPTION())) {
			return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.debug("Exiting deleteSaleWastages service");
		return new ResponseEntity<ResponseCachet>(cachet, HttpStatus.OK);
	}

}
