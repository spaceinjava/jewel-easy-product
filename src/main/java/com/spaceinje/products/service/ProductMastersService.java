package com.spaceinje.products.service;

import java.util.List;
import java.util.Set;

import com.spaceinje.products.bean.product.master.PartyWastageBean;
import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SaleWastageBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface ProductMastersService {

	/**
	 * Product Services
	 */
	ResponseCachet<ProductBean> saveOrUpdateProduct(ProductBean bean);
	ResponseCachet<List<ProductBean>> fetchAllProducts(String tenantCode);
	ResponseCachet fetchProductsByMainGroup(String tenantCode, String groupId);
	ResponseCachet deleteProducts(Set<String> productIds);
	
	/**
	 * Sub Product Services
	 */
	ResponseCachet<SubProductBean> saveOrUpdateSubProduct(SubProductBean bean);
	ResponseCachet<List<SubProductBean>> fetchAllSubProducts(String tenantCode);
	ResponseCachet fetchSubProductsByProduct(String tenantCode, String productId);
	ResponseCachet fetchSizesByProduct(String tenantCode, String productId);
	ResponseCachet deleteSubProducts(Set<String> subProductIds);
	
	/**
	 * Product Size services
	 */
	ResponseCachet<ProductSizeBean> saveOrUpdateProductSize(ProductSizeBean bean);
	ResponseCachet<List<ProductSizeBean>> fetchAllProductSizes(String tenantCode);
	ResponseCachet deleteProductSizes(Set<String> productSizeIds);
	
	/**
	 * Party Wastage services
	 */
	ResponseCachet<PartyWastageBean> saveOrUpdatePartyWastage(PartyWastageBean bean);
	ResponseCachet<List<PartyWastageBean>> fetchPartyWastages(String tenantCode, String partyId, String subProductId, String productSizeId, String purityId);
	ResponseCachet deletePartyWastages(Set<String> partyWastageIds);
	
	/**
	 * Sale Wastage services
	 */
	ResponseCachet<SaleWastageBean> saveOrUpdateSaleWastage(SaleWastageBean bean);
	ResponseCachet<List<SaleWastageBean>> fetchSaleWastages(String tenantCode, String subProductId, String productSizeId, String purityId);
	ResponseCachet deleteSaleWastages(Set<String> saleWastageIds);

}
