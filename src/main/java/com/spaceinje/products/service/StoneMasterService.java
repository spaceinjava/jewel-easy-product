package com.spaceinje.products.service;

import java.util.Set;

import com.spaceinje.products.bean.stone.master.PartyStoneRateBean;
import com.spaceinje.products.bean.stone.master.SaleStoneRateBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface StoneMasterService {

	/*
	 * services for stone group
	 */
	public ResponseCachet saveOrUpdateStoneGroup(StoneGroupBean bean);
	public ResponseCachet fetchAllStoneGroups(String tenantCode);
	public ResponseCachet fetchStoneGroupsByType(String tenantCode, String stoneTypeId);
	public ResponseCachet fetchStonesByStoneGroup(String tenantCode, String groupId);
	public ResponseCachet fetchStoneSizesByStoneGroup(String tenantCode, String groupId);
	public ResponseCachet deleteStoneGroups(Set<String> stoneGroupIds);
	
	/*
	 * services for stone
	 */
	public ResponseCachet saveOrUpdateStone(StoneBean bean);
	public ResponseCachet fetchAllStones(String tenantCode);
	public ResponseCachet deleteStones(Set<String> stoneIds);
	
	/*
	 * services for stone size
	 */
	public ResponseCachet saveOrUpdateStoneSize(StoneSizeBean bean);
	public ResponseCachet fetchAllStoneSizes(String tenantCode);
	public ResponseCachet deleteStoneSizes(Set<String> stoneSizeIds);
	
	/*
	 * services for stone color
	 */
	public ResponseCachet saveOrUpdateStoneColor(StoneColorBean bean);
	public ResponseCachet fetchAllStoneColors(String tenantCode);
	public ResponseCachet deleteStoneColors(Set<String> stoneColorIds);
	
	/*
	 * services for stone clarity
	 */
	public ResponseCachet saveOrUpdateStoneClarity(StoneClarityBean bean);
	public ResponseCachet fetchAllStoneClarities(String tenantCode);
	public ResponseCachet deleteStoneClarities(Set<String> stoneClarityIds);
	
	/*
	 * services for stone polish
	 */
	public ResponseCachet saveOrUpdateStonePolish(StonePolishBean bean);
	public ResponseCachet fetchAllStonePolishes(String tenantCode);
	public ResponseCachet deleteStonePolishes(Set<String> stonePolishIds);
	
	/*
	 * services for stone type
	 */
	public ResponseCachet fetchAllStoneTypes();
	
	/*
	 * services for party stone rate
	 */
	public ResponseCachet saveOrUpdatePartyRate(PartyStoneRateBean bean);
	public ResponseCachet fetchPartyRates(String tenantCode, String partyId, String stoneId, String stoneSizeId);
	public ResponseCachet deletePartyRates(Set<String> partyRateIds);
	
	/*
	 * services for sale stone rate
	 */
	public ResponseCachet saveOrUpdateSaleRate(SaleStoneRateBean bean);
	public ResponseCachet fetchSaleRates(String tenantCode, String stoneId, String stoneSizeId);
	public ResponseCachet deleteSaleRates(Set<String> saleRateIds);

}
