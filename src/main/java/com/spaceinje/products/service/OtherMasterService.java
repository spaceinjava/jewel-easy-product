package com.spaceinje.products.service;

import java.util.List;
import java.util.Set;

import com.spaceinje.products.bean.master.MetalTypeBean;
import com.spaceinje.products.bean.other.master.CounterBean;
import com.spaceinje.products.bean.other.master.FloorBean;
import com.spaceinje.products.bean.other.master.GSTBean;
import com.spaceinje.products.bean.other.master.HSNCodeBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.other.master.RateBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface OtherMasterService {

	/*
	 * services methods for Floor
	 */
	ResponseCachet<FloorBean> saveOrUpdateFloor(FloorBean floor);

	ResponseCachet<List<FloorBean>> findAllFloors(String tenantCode);

	ResponseCachet deleteFloors(Set<String> floorIds);

	/*
	 * services methods for Counter
	 */
	ResponseCachet<CounterBean> saveOrUpdateCounter(CounterBean counter);

	ResponseCachet<List<CounterBean>> findAllCounters(String tenantCode);

	ResponseCachet deleteCounters(Set<String> counteIds);

	/*
	 * services methods for Purity
	 */
	ResponseCachet<PurityBean> saveOrUpdatePurity(PurityBean purity);

	ResponseCachet<List<PurityBean>> findAllPurities(String tenantCode);

	ResponseCachet fetchPuritiesByMainGroup(String tenantCode, String groupId);

	ResponseCachet deletePurities(Set<String> purityIds);

	/*
	 * services methods for Rate
	 */
	ResponseCachet<List<RateBean>> saveOrUpdateRate(List<RateBean> rate);

	/*
	 * services methods for GST
	 */
	ResponseCachet<GSTBean> saveOrUpdateGST(GSTBean gst);

	ResponseCachet<List<GSTBean>> findAllGSTs(String tenantCode);

	ResponseCachet deleteGSTs(Set<String> gstIds);

	ResponseCachet<List<GSTBean>> findAllGSTsByMetalType(String tenantCode, String metalName);

	/*
	 * services methods for HSN Code
	 */
	ResponseCachet<HSNCodeBean> saveOrUpdateHSN(HSNCodeBean hsn);

	ResponseCachet<List<HSNCodeBean>> findAllHSNs(String tenantCode);

	ResponseCachet deleteHSNs(Set<String> hsnIds);

	ResponseCachet<List<MetalTypeBean>> findAllMetalTypes();

	ResponseCachet fetchRates(String tenantCode, String groupId, Double perGramRate);


}
