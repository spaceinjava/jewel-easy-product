package com.spaceinje.products.service;

import java.util.List;
import java.util.Set;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface CommonMasterService {

	/*
	 * service for party type
	 */
	public ResponseCachet fetchAllPartyTypes();
	
	/*
	 * services for Main group
	 */
	public ResponseCachet<MainGroupBean> saveOrUpdateMainGroup(MainGroupBean bean);
	public ResponseCachet<List<MainGroupBean>> fetchAllMainGroups(String tenantCode);
	public ResponseCachet deleteMainGroups(Set<String> mainGroupIds);
	
}
