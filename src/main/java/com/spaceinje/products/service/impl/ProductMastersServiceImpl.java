package com.spaceinje.products.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.products.bean.product.master.PartyWastageBean;
import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SaleWastageBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.converter.product.PartyWastageConverter;
import com.spaceinje.products.converter.product.ProductConverter;
import com.spaceinje.products.converter.product.ProductSizeConverter;
import com.spaceinje.products.converter.product.SaleWastageConverter;
import com.spaceinje.products.converter.product.SubProductConverter;
import com.spaceinje.products.model.product.master.PartyWastage;
import com.spaceinje.products.model.product.master.Product;
import com.spaceinje.products.model.product.master.ProductSize;
import com.spaceinje.products.model.product.master.SaleWastage;
import com.spaceinje.products.model.product.master.SubProduct;
import com.spaceinje.products.repository.product.PartyWastageRepository;
import com.spaceinje.products.repository.product.ProductRepository;
import com.spaceinje.products.repository.product.ProductSizeRepository;
import com.spaceinje.products.repository.product.SaleWastageRepository;
import com.spaceinje.products.repository.product.SubProductRepository;
import com.spaceinje.products.service.ProductMastersService;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.specification.OtherSpecs;
import com.spaceinje.products.specification.ProductSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service
public class ProductMastersServiceImpl implements ProductMastersService {

	private static final Logger log = LoggerFactory.getLogger(ProductMastersServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private SubProductRepository subProductRepository;

	@Autowired
	private ProductSizeRepository productSizeRepository;

	@Autowired
	private PartyWastageRepository partyWastageRepository;

	@Autowired
	private SaleWastageRepository saleWastageRepository;

	@Autowired
	private ProductConverter productConverter;

	@Autowired
	private SubProductConverter subProductConverter;

	@Autowired
	private ProductSizeConverter productSizeConverter;

	@Autowired
	private PartyWastageConverter partyWastageConverter;

	@Autowired
	private SaleWastageConverter saleWastageConverter;

	/**
	 * service to save product
	 */
	@Override
	public ResponseCachet<ProductBean> saveOrUpdateProduct(ProductBean bean) {
		ResponseCachet<ProductBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of product with given shortcode");
			Optional<Product> retrievedProduct = productRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedProduct.isPresent() && !retrievedProduct.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			Product product = productConverter.beanToEntity(bean);
			product = productRepository.save(product);
			log.debug("successfully saved product with name : {}", product.getProductName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("product with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(productConverter.entityToBean(product));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving product: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to retrieve all the products for the tenant
	 */
	@Override
	public ResponseCachet<List<ProductBean>> fetchAllProducts(String tenantCode) {
		ResponseCachet<List<ProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Product> list = productRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Product found");
				cachet.setData(new ArrayList<>());
				log.debug("No Product found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all products");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved products");
			cachet.setData(list.stream().map(ele -> productConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to retrieve all the products based on main group for the tenant
	 */
	@Override
	public ResponseCachet fetchProductsByMainGroup(String tenantCode, String groupId) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Product> list = productRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(CommonSpecs.mainGroupIdSpec(groupId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Product found for given main group");
				cachet.setData(new ArrayList<>());
				log.debug("No Product found for given main group to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all products for given main group id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved products for given main group id");
			cachet.setData(list.stream().map(ele -> productConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all products for given main group id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to delete products
	 */
	@Override
	public ResponseCachet deleteProducts(Set<String> productIds) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (productRepository.productHasOrphans(productIds)) {
				log.debug("Product(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Product(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			productRepository.deleteProducts(productIds);
			log.debug("deleted product(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted product(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to save sub product
	 */
	@Override
	public ResponseCachet<SubProductBean> saveOrUpdateSubProduct(SubProductBean bean) {
		ResponseCachet<SubProductBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of sub product with given shortcode");
			Optional<SubProduct> retrievedSubProduct = subProductRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedSubProduct.isPresent() && !retrievedSubProduct.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			SubProduct subProduct = subProductConverter.beanToEntity(bean);
			subProduct = subProductRepository.save(subProduct);
			log.debug("successfully saved sub product with name : {}", subProduct.getSubProductName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("sub product with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(subProductConverter.entityToBean(subProduct));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving product: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to retrieve the list of subproducts for tenant
	 */
	@Override
	public ResponseCachet<List<SubProductBean>> fetchAllSubProducts(String tenantCode) {
		ResponseCachet<List<SubProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<SubProduct> list = subProductRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Sub Product found");
				cachet.setData(new ArrayList<>());
				log.debug("No Sub Product found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all sub products");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved sub products");
			cachet.setData(
					list.stream().map(ele -> subProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all sub products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet fetchSubProductsByProduct(String tenantCode, String productId) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<SubProduct> list = subProductRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(ProductSpecs.productIdSpec(productId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Sub Product found for given product");
				cachet.setData(new ArrayList<>());
				log.debug("No Sub Product found for given product to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all sub products for given product id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved sub products for given product id");
			cachet.setData(
					list.stream().map(ele -> subProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all sub products for given product id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet fetchSizesByProduct(String tenantCode, String productId) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<ProductSize> list = productSizeRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(ProductSpecs.productIdSpec(productId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Product Sizes found for given product");
				cachet.setData(new ArrayList<>());
				log.debug("No Product Sizes found for given product to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all product sizes for given product id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved product sizes for given product id");
			cachet.setData(
					list.stream().map(ele -> productSizeConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all product sizes for given product id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to delete sub products
	 */
	@Override
	public ResponseCachet deleteSubProducts(Set<String> subProductIds) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (subProductRepository.subProductHasOrphans(subProductIds)) {
				log.debug("Sub Product(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Sub Product(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			subProductRepository.deleteSubProducts(subProductIds);
			log.debug("deleted sub product(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted sub product(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting sub products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to save or update product size
	 */
	@Override
	public ResponseCachet<ProductSizeBean> saveOrUpdateProductSize(ProductSizeBean bean) {
		ResponseCachet<ProductSizeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of given size for the input product");
			Optional<ProductSize> retrievedProductSize = productSizeRepository
					.findOne(CommonSpecs.findByTenantCodeSpec(bean.getTenantCode())
							.and(ProductSpecs.productIdSpec(bean.getProduct().getId()))
							.and(ProductSpecs.productSizeSpec(bean.getProductSize())));
			if (retrievedProductSize.isPresent() && !retrievedProductSize.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("size " + bean.getProductSize() + " for the product already exists");
				log.debug("product size already exists to the product for tenant {}", bean.getTenantCode());
				return cachet;
			}
			ProductSize productSize = productSizeConverter.beanToEntity(bean);
			productSize = productSizeRepository.save(productSize);
			log.debug("successfully saved product with size : {}", productSize.getProductSize());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("sub product with size " + bean.getProductSize() + " saved successfully");
			cachet.setData(productSizeConverter.entityToBean(productSize));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving product size : {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to retrieve list of product sizes for tenant
	 */
	@Override
	public ResponseCachet<List<ProductSizeBean>> fetchAllProductSizes(String tenantCode) {
		ResponseCachet<List<ProductSizeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<ProductSize> list = productSizeRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Product Sizes found");
				cachet.setData(new ArrayList<>());
				log.debug("No Product Sizes found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all product sizes");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved product sizes");
			cachet.setData(
					list.stream().map(ele -> productSizeConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all product sizes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to delete product sizes
	 */
	@Override
	public ResponseCachet deleteProductSizes(Set<String> productSizeIds) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (productSizeRepository.productSizeHasOrphans(productSizeIds)) {
				log.debug("Product Size(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Product Size(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			productSizeRepository.deleteProductSizes(productSizeIds);
			log.debug("deleted product size(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted product size(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting product sizes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to save or update party wastage
	 */
	@Override
	public ResponseCachet<PartyWastageBean> saveOrUpdatePartyWastage(PartyWastageBean bean) {
		ResponseCachet<PartyWastageBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			PartyWastage partyWastage = partyWastageConverter.beanToEntity(bean);
			partyWastage = partyWastageRepository.save(partyWastage);
			log.debug("successfully saved party wastage.!");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully saved party wastage.!");
			cachet.setData(partyWastageConverter.entityToBean(partyWastage));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving party wastage: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service justifies following cases \n
	 * case 1: return all party wastages for tenant \n
	 * case 2: returns party wastages based on tenant, party, purity, subProduct, productSize
	 */
	@Override
	public ResponseCachet<List<PartyWastageBean>> fetchPartyWastages(String tenantCode, String partyId,
			String subProductId, String productSizeId, String purityId) {
		ResponseCachet<List<PartyWastageBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Specification partyWastageSpecification = CommonSpecs.findByTenantCodeSpec(tenantCode);
			if (StringUtils.isNotEmpty(partyId)) {
				partyWastageSpecification = partyWastageSpecification.and(CommonSpecs.partyIdSpec(partyId));
			}
			if (StringUtils.isNotEmpty(purityId)) {
				partyWastageSpecification = partyWastageSpecification.and(OtherSpecs.purityIdSpec(purityId));
			}
			if (StringUtils.isNotEmpty(subProductId)) {
				partyWastageSpecification = partyWastageSpecification.and(ProductSpecs.subProductIdSpec(subProductId));
			}
			if (StringUtils.isNotEmpty(productSizeId)) {
				partyWastageSpecification = partyWastageSpecification
						.and(ProductSpecs.productSizeIdSpec(productSizeId));
			}
			List<PartyWastage> list = partyWastageRepository.findAll(partyWastageSpecification);
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No party wastages found");
				cachet.setData(new ArrayList<>());
				log.debug("No party wastages found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved party wastages");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved party wastages");
			cachet.setData(
					list.stream().map(ele -> partyWastageConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving party wastages: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to delete party wastages
	 */
	@Override
	public ResponseCachet deletePartyWastages(Set<String> partyWastageIds) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			partyWastageRepository.deletePartyWastages(partyWastageIds);
			log.debug("deleted party wastage(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted party wastage(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting party wastages: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to save or update sale wastage
	 */
	@Override
	public ResponseCachet<SaleWastageBean> saveOrUpdateSaleWastage(SaleWastageBean bean) {
		ResponseCachet<SaleWastageBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			SaleWastage saleWastage = saleWastageConverter.beanToEntity(bean);
			saleWastage = saleWastageRepository.save(saleWastage);
			log.debug("successfully saved sale wastage.!");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully saved party wastage.!");
			cachet.setData(saleWastageConverter.entityToBean(saleWastage));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving sale wastage: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service justifies following cases \n
	 * case 1: return all sale wastages for tenant \n
	 * case 2: returns sale wastages based on tenant, party, purity, subProduct, productSize
	 */
	@Override
	public ResponseCachet<List<SaleWastageBean>> fetchSaleWastages(String tenantCode, String subProductId,
			String productSizeId, String purityId) {
		ResponseCachet<List<SaleWastageBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Specification saleWastageSpecification = CommonSpecs.findByTenantCodeSpec(tenantCode);
			if (StringUtils.isNotEmpty(purityId)) {
				saleWastageSpecification = saleWastageSpecification.and(OtherSpecs.purityIdSpec(purityId));
			}
			if (StringUtils.isNotEmpty(subProductId)) {
				saleWastageSpecification = saleWastageSpecification.and(ProductSpecs.subProductIdSpec(subProductId));
			}
			if (StringUtils.isNotEmpty(productSizeId)) {
				saleWastageSpecification = saleWastageSpecification.and(ProductSpecs.productSizeIdSpec(productSizeId));
			}
			List<SaleWastage> list = saleWastageRepository.findAll(saleWastageSpecification);
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No sale wastages found");
				cachet.setData(new ArrayList<>());
				log.debug("No sale wastages found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved sale wastages");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved sale wastages");
			cachet.setData(
					list.stream().map(ele -> saleWastageConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving sale wastages: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to delete sale wastages
	 */
	@Override
	public ResponseCachet deleteSaleWastages(Set<String> saleWastageIds) {
		ResponseCachet cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			saleWastageRepository.deleteSaleWastages(saleWastageIds);
			log.debug("deleted sale wastage(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted sale wastage(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting sale wastages: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
