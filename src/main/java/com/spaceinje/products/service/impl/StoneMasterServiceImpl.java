package com.spaceinje.products.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.products.bean.stone.master.PartyStoneRateBean;
import com.spaceinje.products.bean.stone.master.SaleStoneRateBean;
import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneClarityBean;
import com.spaceinje.products.bean.stone.master.StoneColorBean;
import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.bean.stone.master.StonePolishBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;
import com.spaceinje.products.bean.stone.master.StoneTypeBean;
import com.spaceinje.products.converter.stone.StoneConverter;
import com.spaceinje.products.converter.stone.StoneGroupConverter;
import com.spaceinje.products.converter.stone.StoneSizeConverter;
import com.spaceinje.products.model.stone.master.PartyStoneRate;
import com.spaceinje.products.model.stone.master.SaleStoneRate;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneClarity;
import com.spaceinje.products.model.stone.master.StoneColor;
import com.spaceinje.products.model.stone.master.StoneGroup;
import com.spaceinje.products.model.stone.master.StonePolish;
import com.spaceinje.products.model.stone.master.StoneSize;
import com.spaceinje.products.model.stone.master.StoneType;
import com.spaceinje.products.repository.stone.PartyStoneRateRepository;
import com.spaceinje.products.repository.stone.SaleStoneRateRepository;
import com.spaceinje.products.repository.stone.StoneClarityRepository;
import com.spaceinje.products.repository.stone.StoneColorRepository;
import com.spaceinje.products.repository.stone.StoneGroupRepository;
import com.spaceinje.products.repository.stone.StonePolishRepository;
import com.spaceinje.products.repository.stone.StoneRepository;
import com.spaceinje.products.repository.stone.StoneSizeRepository;
import com.spaceinje.products.repository.stone.StoneTypeRepository;
import com.spaceinje.products.service.StoneMasterService;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.specification.StoneSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Service("stoneMasterService")
public class StoneMasterServiceImpl implements StoneMasterService {

	private static final Logger log = LoggerFactory.getLogger(StoneMasterServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private StoneClarityRepository stoneClarityRepository;

	@Autowired
	private StoneColorRepository stoneColorRepository;

	@Autowired
	private StoneGroupRepository stoneGroupRepository;

	@Autowired
	private StonePolishRepository stonePolishRepository;

	@Autowired
	private StoneRepository stoneRepository;

	@Autowired
	private StoneSizeRepository stoneSizeRepository;

	@Autowired
	private StoneTypeRepository stoneTypeRepository;

	@Autowired
	private PartyStoneRateRepository partyStoneRateRepository;

	@Autowired
	private SaleStoneRateRepository saleStoneRateRepository;

	@Autowired
	private StoneConverter stoneConverter;

	@Autowired
	private StoneGroupConverter stoneGroupConverter;

	@Autowired
	private StoneSizeConverter stoneSizeConverter;

	@Autowired
	private ProductConstants constants;

	@Override
	public ResponseCachet<?> saveOrUpdateStoneGroup(StoneGroupBean bean) {
		ResponseCachet<StoneGroupBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone group with given shortcode");
			Optional<StoneGroup> retrievedStoneGroup = stoneGroupRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStoneGroup.isPresent() && !retrievedStoneGroup.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			StoneGroup stoneGroup = stoneGroupConverter.beanToEntity(bean);
			stoneGroup = stoneGroupRepository.save(stoneGroup);
			log.debug("successfully saved stone group with name : {}", stoneGroup.getGroupName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone group with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(stoneGroupConverter.entityToBean(stoneGroup));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone group: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneGroupBean>> fetchAllStoneGroups(String tenantCode) {
		ResponseCachet<List<StoneGroupBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneGroup> list = stoneGroupRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stone group found");
				cachet.setData(new ArrayList<>());
				log.debug("No stone group found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone groups");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone groups");
			cachet.setData(
					list.stream().map(ele -> stoneGroupConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone groups: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneGroupBean>> fetchStoneGroupsByType(String tenantCode, String stoneTypeId) {
		ResponseCachet<List<StoneGroupBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneGroup> list = stoneGroupRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(StoneSpecs.stoneTypeIdSpec(stoneTypeId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stone group found for given stone type");
				cachet.setData(new ArrayList<>());
				log.debug("No stone group found for given stone type to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone groups associated to the given type id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone groups associated to the given type id");
			cachet.setData(
					list.stream().map(ele -> stoneGroupConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone groups associated to the given type id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneBean>> fetchStonesByStoneGroup(String tenantCode, String groupId) {
		ResponseCachet<List<StoneBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Stone> list = stoneRepository.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
					.and(StoneSpecs.stoneGroupIdSpec(groupId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stones found for given stone group");
				cachet.setData(new ArrayList<>());
				log.debug("No stones found for given stone group to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stones associated to the given group id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stones associated to the given group id");
			cachet.setData(list.stream().map(ele -> stoneConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stones associated to the given group id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneSizeBean>> fetchStoneSizesByStoneGroup(String tenantCode, String groupId) {
		ResponseCachet<List<StoneSizeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneSize> list = stoneSizeRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(StoneSpecs.stoneGroupIdSpec(groupId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stone sizes found for given stone group");
				cachet.setData(new ArrayList<>());
				log.debug("No stone sizes found for given stone group to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone sizes associated to the given group id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone sizes associated to the given group id");
			cachet.setData(list.stream().map(ele -> stoneSizeConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone sizes associated to the given group id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStoneGroups(Set<String> stoneGroupIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stoneGroupRepository.stoneGroupHasOrphans(stoneGroupIds)) {
				log.debug("stone group(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone Group(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stoneGroupRepository.deleteStoneGroups(stoneGroupIds);
			log.debug("deleted stone group(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone group(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stone groups: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateStone(StoneBean bean) {
		ResponseCachet<StoneBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone with given shortcode");
			Optional<Stone> retrievedStone = stoneRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStone.isPresent() && !retrievedStone.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			Stone stone = stoneConverter.beanToEntity(bean);
			stone = stoneRepository.save(stone);
			log.debug("successfully saved stone with name : {}", stone.getStoneName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(stoneConverter.entityToBean(stone));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneBean>> fetchAllStones(String tenantCode) {
		ResponseCachet<List<StoneBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Stone> list = stoneRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stones found");
				cachet.setData(new ArrayList<>());
				log.debug("No stones found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stones");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stones");
			cachet.setData(list.stream().map(ele -> stoneConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stones: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStones(Set<String> stoneIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stoneRepository.stoneHasOrphans(stoneIds)) {
				log.debug("stone(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stoneRepository.deleteStones(stoneIds);
			log.debug("deleted stone(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stones: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateStoneSize(StoneSizeBean bean) {
		ResponseCachet<StoneSizeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone size with given shortcode");
			Optional<StoneSize> retrievedStoneSize = stoneSizeRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStoneSize.isPresent() && !retrievedStoneSize.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			StoneSize stoneSize = stoneSizeConverter.beanToEntity(bean);
			stoneSize = stoneSizeRepository.save(stoneSize);
			log.debug("successfully saved stone with size : {}", stoneSize.getStoneSize());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone size with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(stoneSizeConverter.entityToBean(stoneSize));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone size: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneSizeBean>> fetchAllStoneSizes(String tenantCode) {
		ResponseCachet<List<StoneSizeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneSize> list = stoneSizeRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Stone Sizes found");
				cachet.setData(new ArrayList<>());
				log.debug("No Stone Sizes found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone sizes");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone sizes");
			cachet.setData(list.stream().map(ele -> stoneSizeConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone sizes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStoneSizes(Set<String> stoneSizeIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stoneSizeRepository.stoneSizeHasOrphans(stoneSizeIds)) {
				log.debug("stone size(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone Size(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stoneSizeRepository.deleteStoneSizes(stoneSizeIds);
			log.debug("deleted stone size(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone size(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stone sizes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateStoneColor(StoneColorBean bean) {
		ResponseCachet<StoneColorBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone color with given shortcode");
			Optional<StoneColor> retrievedStoneColor = stoneColorRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStoneColor.isPresent() && !retrievedStoneColor.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			StoneColor stoneColor = modelMapper.map(bean, StoneColor.class);
			stoneColor = stoneColorRepository.save(stoneColor);
			log.debug("successfully saved stone with color name : {}", stoneColor.getColorName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone color with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(stoneColor, StoneColorBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone color: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneColorBean>> fetchAllStoneColors(String tenantCode) {
		ResponseCachet<List<StoneColorBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneColor> list = stoneColorRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Stone Colors found");
				cachet.setData(new ArrayList<>());
				log.debug("No Stone Colors found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone colors");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone colors");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, StoneColorBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone colors: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStoneColors(Set<String> stoneColorIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stoneColorRepository.stoneColorHasOrphans(stoneColorIds)) {
				log.debug("Stone Color(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone Color(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stoneColorRepository.deleteStoneColors(stoneColorIds);
			log.debug("deleted stone color(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone color(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stone colors: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateStoneClarity(StoneClarityBean bean) {
		ResponseCachet<StoneClarityBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone clarity with given shortcode");
			Optional<StoneClarity> retrievedStoneClarity = stoneClarityRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStoneClarity.isPresent() && !retrievedStoneClarity.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			StoneClarity stoneClarity = modelMapper.map(bean, StoneClarity.class);
			stoneClarity = stoneClarityRepository.save(stoneClarity);
			log.debug("successfully saved stone with clarity name : {}", stoneClarity.getClarityName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone clarity with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(stoneClarity, StoneClarityBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone clarity: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneClarityBean>> fetchAllStoneClarities(String tenantCode) {
		ResponseCachet<List<StoneClarityBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneClarity> list = stoneClarityRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Stone Clarities found");
				cachet.setData(new ArrayList<>());
				log.debug("No Stone Clarities found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone clarities");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone clarities");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, StoneClarityBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone clarities: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStoneClarities(Set<String> stoneClarityIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stoneClarityRepository.stoneClarityHasOrphans(stoneClarityIds)) {
				log.debug("Stone Clarity(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone Clarity(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stoneClarityRepository.deleteStoneClarities(stoneClarityIds);
			log.debug("deleted stone clarity(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone clarity(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stone clarities: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateStonePolish(StonePolishBean bean) {
		ResponseCachet<StonePolishBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of stone polish with given shortcode");
			Optional<StonePolish> retrievedStonePolish = stonePolishRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedStonePolish.isPresent() && !retrievedStonePolish.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			StonePolish stonePolish = modelMapper.map(bean, StonePolish.class);
			stonePolish = stonePolishRepository.save(stonePolish);
			log.debug("successfully saved stone with polish name : {}", stonePolish.getPolishName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("stone polish with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(stonePolish, StonePolishBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving stone polish: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StonePolishBean>> fetchAllStonePolishes(String tenantCode) {
		ResponseCachet<List<StonePolishBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StonePolish> list = stonePolishRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Stone Polishes found");
				cachet.setData(new ArrayList<>());
				log.debug("No Stone Polishes found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone polishes");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone polishes");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, StonePolishBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone polishes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteStonePolishes(Set<String> stonePolishIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (stonePolishRepository.stonePolishHasOrphans(stonePolishIds)) {
				log.debug("stone polish(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Stone Polish(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			stonePolishRepository.deleteStonePolishes(stonePolishIds);
			log.debug("deleted stone polish(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted stone polish(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting stone polishes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<StoneTypeBean>> fetchAllStoneTypes() {
		ResponseCachet<List<StoneTypeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<StoneType> list = stoneTypeRepository.findAll();
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No stone types found");
				cachet.setData(new ArrayList<>());
				log.debug("No stone types found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all stone types");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved stone types");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, StoneTypeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all stone types: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdatePartyRate(PartyStoneRateBean bean) {
		ResponseCachet<PartyStoneRateBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			PartyStoneRate partyStoneRate = modelMapper.map(bean, PartyStoneRate.class);
			partyStoneRate = partyStoneRateRepository.save(partyStoneRate);
			log.debug("successfully saved party stone rate.!");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully saved party stone rate.!");
			cachet.setData(modelMapper.map(partyStoneRate, PartyStoneRateBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving party stone rate: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<PartyStoneRateBean>> fetchPartyRates(String tenantCode, String partyId,
			String stoneId, String stoneSizeId) {
		ResponseCachet<List<PartyStoneRateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Specification partyRateSpecification = CommonSpecs.findByTenantCodeSpec(tenantCode);
			if (StringUtils.isNotEmpty(partyId)) {
				partyRateSpecification = partyRateSpecification.and(CommonSpecs.partyIdSpec(partyId));
			} if (StringUtils.isNotEmpty(stoneId)) {
				partyRateSpecification = partyRateSpecification.and(StoneSpecs.stoneIdSpec(stoneId));
			} if (StringUtils.isNotEmpty(stoneSizeId)) {
				partyRateSpecification = partyRateSpecification.and(StoneSpecs.stoneSizeIdSpec(stoneSizeId));
			}
			List<PartyStoneRate> list = partyStoneRateRepository.findAll(partyRateSpecification);
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Party Stone Rates found");
				cachet.setData(new ArrayList<>());
				log.debug("No Party Stone Rates found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved party stone rates");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved party stone rates");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, PartyStoneRateBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving party stone rates: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deletePartyRates(Set<String> partyRateIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			partyStoneRateRepository.deletePartyRates(partyRateIds);
			log.debug("deleted party stone rate(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted party stone rate(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting party stone rates: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateSaleRate(SaleStoneRateBean bean) {
		ResponseCachet<SaleStoneRateBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {

			/**
			 * TODO: operation based on "update existing stock" flag, should be finalized
			 * and then to be implemented, as of now requirement is, based on all the input
			 * data combination, rates should be updated, so for now kept this functionality
			 * on hold
			 */

			SaleStoneRate saleStoneRate = modelMapper.map(bean, SaleStoneRate.class);
			saleStoneRate = saleStoneRateRepository.save(saleStoneRate);
			log.debug("successfully saved sale stone rate.!");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully saved sale stone rate.!");
			cachet.setData(modelMapper.map(saleStoneRate, SaleStoneRateBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving sale stone rate: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<SaleStoneRateBean>> fetchSaleRates(String tenantCode, String stoneId, String stoneSizeId) {
		ResponseCachet<List<SaleStoneRateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Specification saleRateSpecification = CommonSpecs.findByTenantCodeSpec(tenantCode);
			if (StringUtils.isNotEmpty(stoneId)) {
				saleRateSpecification = saleRateSpecification.and(StoneSpecs.stoneIdSpec(stoneId));
			} if (StringUtils.isNotEmpty(stoneSizeId)) {
				saleRateSpecification = saleRateSpecification.and(StoneSpecs.stoneSizeIdSpec(stoneSizeId));
			}
			List<SaleStoneRate> list = saleStoneRateRepository.findAll(saleRateSpecification);
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Sale Stone Rates found");
				cachet.setData(new ArrayList<>());
				log.debug("No Sale Stone Rates found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all sale stone rates");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved sale stone rates");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, SaleStoneRateBean.class))
					.collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all sale stone rates: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteSaleRates(Set<String> saleRateIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			saleStoneRateRepository.deleteSaleRates(saleRateIds);
			log.debug("deleted sale stone rate(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted sale stone rate(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting sale stone rates: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
