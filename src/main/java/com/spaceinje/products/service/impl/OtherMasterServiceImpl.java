package com.spaceinje.products.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.products.bean.master.MetalTypeBean;
import com.spaceinje.products.bean.other.master.CounterBean;
import com.spaceinje.products.bean.other.master.FloorBean;
import com.spaceinje.products.bean.other.master.GSTBean;
import com.spaceinje.products.bean.other.master.HSNCodeBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.other.master.RateBean;
import com.spaceinje.products.converter.other.CounterConverter;
import com.spaceinje.products.converter.other.PurityConverter;
import com.spaceinje.products.converter.other.RateConverter;
import com.spaceinje.products.model.master.MetalType;
import com.spaceinje.products.model.other.master.Counter;
import com.spaceinje.products.model.other.master.Floor;
import com.spaceinje.products.model.other.master.GST;
import com.spaceinje.products.model.other.master.HSNCode;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.other.master.Rate;
import com.spaceinje.products.repository.other.CounterRepository;
import com.spaceinje.products.repository.other.FloorRepository;
import com.spaceinje.products.repository.other.GSTRepository;
import com.spaceinje.products.repository.other.HSNCodeRepository;
import com.spaceinje.products.repository.other.MetalTypeRepository;
import com.spaceinje.products.repository.other.PurityRepository;
import com.spaceinje.products.repository.other.RateRepository;
import com.spaceinje.products.service.OtherMasterService;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.specification.OtherSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;
import com.spaceinje.products.util.UtilityService;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service("otherMasterService")
public class OtherMasterServiceImpl implements OtherMasterService {

	private static final Logger log = LoggerFactory.getLogger(OtherMasterServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private FloorRepository floorRepository;

	@Autowired
	private CounterRepository counterRepository;

	@Autowired
	private PurityRepository purityRepository;

	@Autowired
	private CounterConverter counterConverter;

	@Autowired
	private PurityConverter purityConverter;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private MetalTypeRepository metalTypeRepository;

	@Autowired
	private GSTRepository gstRepository;

	@Autowired
	private RateRepository rateRepository;

	@Autowired
	private HSNCodeRepository hsnCodeRepository;

	@Autowired
	private RateConverter rateConverter;

	@Override
	public ResponseCachet<FloorBean> saveOrUpdateFloor(FloorBean bean) {
		ResponseCachet<FloorBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of brand with given shortcode");
			Optional<Floor> retrievedFloor = floorRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedFloor.isPresent() && !retrievedFloor.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("floor short code already exists to the tenant {}", bean.getTenantCode());

				return cachet;
			}
			Floor floor = modelMapper.map(bean, Floor.class);
			floor = floorRepository.save(floor);
			log.debug("successfully saved Floor with name : {}", floor.getFloorName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("Floor with name " + bean.getFloorName() + " saved successfully");
			cachet.setData(modelMapper.map(floor, FloorBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving Floor: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<FloorBean>> findAllFloors(String tenantCode) {
		ResponseCachet<List<FloorBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Floor> list = floorRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Floors found");
				cachet.setData(new ArrayList<>());
				log.debug("No Floors found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Floors");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved Floors");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, FloorBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all floors: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteFloors(Set<String> floorIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (floorRepository.floorHasOrphans(floorIds)) {
				log.debug("floor(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("floor(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			floorRepository.deleteFloors(floorIds);
			log.debug("deleted floor(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted floor(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting floors: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<CounterBean> saveOrUpdateCounter(CounterBean bean) {
		ResponseCachet<CounterBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of counter with given shortcode");
			Optional<Counter> retrievedCounter = counterRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedCounter.isPresent() && !retrievedCounter.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Counter short code already exists to the tenant {}", bean.getTenantCode());

				return cachet;
			}
			Counter counter = counterConverter.beanToEntity(bean);
			counter = counterRepository.save(counter);
			log.debug("successfully saved Counter with name : {}", counter.getCounterName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("Counter with name " + bean.getCounterName() + " saved successfully");
			cachet.setData(counterConverter.entityToBean(counter));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving Counter: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<CounterBean>> findAllCounters(String tenantCode) {
		ResponseCachet<List<CounterBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Counter> list = counterRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Counters found");
				cachet.setData(new ArrayList<>());
				log.debug("No Counters found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Counters");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved Counters");
			cachet.setData(list.stream().map(ele -> counterConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Counters: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteCounters(Set<String> counteIds) {

		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			counterRepository.deleteCounters(counteIds);
			log.debug("deleted counter(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted counter(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting counters: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<PurityBean> saveOrUpdatePurity(PurityBean bean) {
		ResponseCachet<PurityBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of main group with given purity");
			Optional<Purity> retrievedStoneGroup = purityRepository
					.findOne(CommonSpecs.findByTenantCodeSpec(bean.getTenantCode())
							.and(CommonSpecs.mainGroupIdSpec(bean.getMainGroup().getId()))
							.and(OtherSpecs.puritySpec(bean.getPurity())));
			if (retrievedStoneGroup.isPresent() && !retrievedStoneGroup.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("purity for this maingroup already exists");
				log.debug("purity for this maingroup {} already exists to the tenant {}",
						bean.getMainGroup().getGroupName(), bean.getTenantCode());
				return cachet;
			}
			Purity purity = purityConverter.beanToEntity(bean);
			purity = purityRepository.save(purity);
			log.debug("successfully saved Purity with name : {}", purity.getPurity());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage(
					bean.getPurity() + "purity for " + bean.getMainGroup().getGroupName() + " saved successfully");
			cachet.setData(purityConverter.entityToBean(purity));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving Purity: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<PurityBean>> findAllPurities(String tenantCode) {
		ResponseCachet<List<PurityBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Purity> list = purityRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Purities found");
				cachet.setData(new ArrayList<>());
				log.debug("No Purities found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all Purities");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved Purities");
			cachet.setData(list.stream().map(ele -> purityConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all Purities: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/**
	 * service to retrieve all the purities based on main group for the tenant
	 */
	@Override
	public ResponseCachet<List<PurityBean>> fetchPuritiesByMainGroup(String tenantCode, String groupId) {
		ResponseCachet<List<PurityBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Purity> list = purityRepository.findAll(Specification
					.where(CommonSpecs.findByTenantCodeSpec(tenantCode)).and(CommonSpecs.mainGroupIdSpec(groupId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Purities found for given main group");
				cachet.setData(new ArrayList<>());
				log.debug("No Purities found for given main group to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all purities for given main group id");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved purities for given main group id");
			cachet.setData(list.stream().map(ele -> purityConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all purities for given main group id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deletePurities(Set<String> purityIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (purityRepository.purityHasOrphans(purityIds)) {
				log.debug("purity(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Purity(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			purityRepository.deletePurities(purityIds);
			log.debug("deleted purity(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted purity(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting purities: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<GSTBean> saveOrUpdateGST(GSTBean bean) {
		ResponseCachet<GSTBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of with given percent");
			Optional<GST> retrievedGst = gstRepository
					.findOne(CommonSpecs.findByTenantAndGSTSpec(bean.getTenantCode(), bean.getPercent()));
			if (retrievedGst.isPresent() && !retrievedGst.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("tenent code and gst percent already exists");
				log.debug("Gst Per already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			GST gst = modelMapper.map(bean, GST.class);
			gst = gstRepository.save(gst);
			log.debug("successfully saved GST for : {}", gst.getMetalType());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("GST for " + gst.getMetalType().getMetalType() + " saved successfully");
			cachet.setData(modelMapper.map(gst, GSTBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving GST: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<GSTBean>> findAllGSTs(String tenantCode) {
		ResponseCachet<List<GSTBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<GST> list = gstRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No GST found");
				cachet.setData(new ArrayList<>());
				log.debug("No GST found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all GST's");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved GST's");
			cachet.setData(list.stream().map(ele -> modelMapper.map(ele, GSTBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all gsts: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteGSTs(Set<String> gstIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			gstRepository.deleteGSTs(gstIds);
			log.debug("deleted gst(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted gst(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting gsts: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<HSNCodeBean> saveOrUpdateHSN(HSNCodeBean bean) {
		ResponseCachet<HSNCodeBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of with given hsn");
			Optional<HSNCode> retrievedHsnCode = hsnCodeRepository
					.findOne(CommonSpecs.findByTenantAndHSNCodeSpec(bean.getTenantCode(), bean.getHsnCode()));
			if (retrievedHsnCode.isPresent() && !retrievedHsnCode.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("tenent code and hsnCode already exists");
				log.debug("HsnCode already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			HSNCode hsnCode = modelMapper.map(bean, HSNCode.class);
			hsnCode = hsnCodeRepository.save(hsnCode);
			log.debug("successfully saved HSNCode of name : {}", hsnCode.getHsnCode());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("HSN code of name " + bean.getHsnCode() + " saved successfully");
			cachet.setData(modelMapper.map(hsnCode, HSNCodeBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving GST: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<HSNCodeBean>> findAllHSNs(String tenantCode) {
		ResponseCachet<List<HSNCodeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<HSNCode> list = hsnCodeRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No hsncodes found");
				cachet.setData(new ArrayList<>());
				log.debug("No hsncodes found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all hsncodes");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved hsncodes");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, HSNCodeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all hsncodes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteHSNs(Set<String> hsnIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			hsnCodeRepository.deleteHSNCodes(hsnIds);
			log.debug("deleted hsncode(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted hsncode(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting hsncodes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<MetalTypeBean>> findAllMetalTypes() {
		ResponseCachet<List<MetalTypeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<MetalType> list = metalTypeRepository.findAll();
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No MetalTypes found");
				cachet.setData(new ArrayList<>());
				log.debug("No MetalTypes found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all MetalTypes");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved MetalTypes");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, MetalTypeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all metalTypes: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<RateBean>> saveOrUpdateRate(List<RateBean> rateBeans) {
		ResponseCachet<List<RateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			String mainGroupId = rateBeans.get(0).getMainGroup().getId();
			log.info("delete rates if mainGroup has already rates defined and insert new rates");
			if (rateRepository.count(CommonSpecs.mainGroupIdSpec(mainGroupId)) > 0) {
				rateRepository.deleteRatesByMainGroup(mainGroupId);
				log.info("deleted rates for mainGroupId {}", mainGroupId);
			}
			List<Rate> rates = rateConverter.beansToEntities(rateBeans);
			rateRepository.saveAll(rates);
			log.info("successfully save Rates for mainGroupId {} ", mainGroupId);
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("Rates saved successfully");
			cachet.setData(rateConverter.entitiesToBeans(rates));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving rate: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<RateBean>> fetchRates(String tenantCode, String groupId, Double perGramRate) {
		ResponseCachet<List<RateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			return (StringUtils.isNotEmpty(groupId) && Optional.ofNullable(perGramRate).isPresent())
					? calculateRatesByMainGroup(tenantCode, groupId, UtilityService.ceilDoubleValue(perGramRate))
					: fetchAllRates(tenantCode, groupId);
		} catch (Exception e) {
			log.error("Exception while retrieving all rates for given main group id: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	/*
	 * this method is used for following scenarios \n 
	 * 1. to retrieve rates based on mainGroupId \n 
	 * 2. to retrieve all active rates for the tenant
	 */
	private ResponseCachet<List<RateBean>> fetchAllRates(String tenantCode, String groupId) {
		ResponseCachet<List<RateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		List<Rate> rateList = StringUtils.isNotEmpty(groupId)
				? rateRepository.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
						.and(CommonSpecs.mainGroupIdSpec(groupId)))
				: rateRepository.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode)));
		if (CollectionUtils.isEmpty(rateList)) {
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("No Rates found");
			cachet.setData(new ArrayList<>());
			log.debug("No Rates found to the tenant");
			return cachet;
		}
		log.debug("Successfully retrieved all rates for given main group id");
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("successfully retrieved rates for given main group id");
		cachet.setData(rateList.stream().map(ele -> rateConverter.entityToBean(ele)).collect(Collectors.toList()));
		return cachet;
	}

	// Calculate rates based on given main group and per gram rate
	private ResponseCachet<List<RateBean>> calculateRatesByMainGroup(String tenantCode, String groupId,
			Double perGramRate) {
		ResponseCachet<List<RateBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		List<Purity> list = purityRepository.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
				.and(CommonSpecs.mainGroupIdSpec(groupId)));
		if (CollectionUtils.isEmpty(list)) {
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("No Purities found to the given main group");
			cachet.setData(new ArrayList<>());
			log.debug("No Purities found to the given main group");
			return cachet;
		}
		List<Rate> rateList = list.stream().map(purity -> {
			Rate rate = new Rate();
			try {
				rate.setTenantCode(purity.getTenantCode());
				rate.setIsDeleted(false);
				rate.setMainGroup(purity.getMainGroup());
				rate.setPurityRate(UtilityService.ceilDoubleValue((purity.getPurity() * perGramRate) / 100));
				rate.setDisplay(false);
				rate.setPerGramRate(perGramRate);
				rate.setPurity(purity);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return rate;
		}).collect(Collectors.toList());
		log.debug("Successfully calculated purity rates for given main group id");
		cachet.setStatus(constants.getSUCCESS());
		cachet.setMessage("Calculated purity rates for given main group id");
		cachet.setData(rateList.stream().map(ele -> rateConverter.entityToBean(ele)).collect(Collectors.toList()));
		return cachet;
	}

	@Override
	public ResponseCachet<List<GSTBean>> findAllGSTsByMetalType(String tenantCode, String metalType) {
		ResponseCachet<List<GSTBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			Optional<MetalType> retrievedMetalType = metalTypeRepository.findOne(OtherSpecs.metalTypeSpec(metalType));
			if (retrievedMetalType.isPresent()) {
				List<GST> list = gstRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode)
						.and(CommonSpecs.findByMetalTypeSpec(retrievedMetalType.get())));
				if (CollectionUtils.isEmpty(list)) {
					cachet.setStatus(constants.getSUCCESS());
					cachet.setMessage("No GST found");
					cachet.setData(new ArrayList<>());
					log.debug("No GST found to the tenant");
					return cachet;
				}
				log.debug("Successfully retrieved all GST's");
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("successfully retrieved GST's");
				cachet.setData(
						list.stream().map(ele -> modelMapper.map(ele, GSTBean.class)).collect(Collectors.toList()));
			}
			return cachet;

		} catch (Exception e) {
			log.error("Exception while retrieving all gsts: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
