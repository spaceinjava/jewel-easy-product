package com.spaceinje.products.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.bean.master.PartyTypeBean;
import com.spaceinje.products.model.master.MainGroup;
import com.spaceinje.products.model.master.PartyType;
import com.spaceinje.products.repository.MainGroupRepository;
import com.spaceinje.products.repository.PartyTypeRepository;
import com.spaceinje.products.service.CommonMasterService;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service
public class CommonMasterServiceImpl implements CommonMasterService {

	private static final Logger log = LoggerFactory.getLogger(CommonMasterServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PartyTypeRepository partyTypeRepository;

	@Autowired
	private MainGroupRepository mainGroupRepository;

	@Autowired
	private ProductConstants constants;

	@Override
	public ResponseCachet<List<PartyTypeBean>> fetchAllPartyTypes() {
		ResponseCachet<List<PartyTypeBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<PartyType> list = partyTypeRepository.findAll();
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No party types found");
				cachet.setData(new ArrayList<>());
				log.debug("No party types found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all party types");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved party types");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, PartyTypeBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all party types: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<MainGroupBean> saveOrUpdateMainGroup(MainGroupBean bean) {
		ResponseCachet<MainGroupBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of main group with given shortcode");
			Optional<MainGroup> retrievedMainGroup = mainGroupRepository
					.findOne(CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedMainGroup.isPresent() && !retrievedMainGroup.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			MainGroup mainGroup = modelMapper.map(bean, MainGroup.class);
			mainGroup = mainGroupRepository.save(mainGroup);
			log.debug("successfully saved main group with name : {}", mainGroup.getGroupName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("main group with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(modelMapper.map(mainGroup, MainGroupBean.class));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving main group: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<MainGroupBean>> fetchAllMainGroups(String tenantCode) {
		ResponseCachet<List<MainGroupBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<MainGroup> list = mainGroupRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Main Group found");
				cachet.setData(new ArrayList<>());
				log.debug("No Main Group found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all main groups");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved main groups");
			cachet.setData(
					list.stream().map(ele -> modelMapper.map(ele, MainGroupBean.class)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all main groups: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteMainGroups(Set<String> mainGroupIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (mainGroupRepository.mainGroupHasOrphans(mainGroupIds)) {
				log.debug("main group(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Main Group(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			mainGroupRepository.deleteMainGroups(mainGroupIds);
			log.debug("deleted main group(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted main group(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting main groups: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
