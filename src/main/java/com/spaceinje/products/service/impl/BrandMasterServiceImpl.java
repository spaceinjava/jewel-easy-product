package com.spaceinje.products.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.converter.brand.BrandConverter;
import com.spaceinje.products.converter.brand.BrandProductConverter;
import com.spaceinje.products.converter.brand.BrandSubProductConverter;
import com.spaceinje.products.model.brand.master.Brand;
import com.spaceinje.products.model.brand.master.BrandProduct;
import com.spaceinje.products.model.brand.master.BrandSubProduct;
import com.spaceinje.products.repository.brand.BrandProductRepository;
import com.spaceinje.products.repository.brand.BrandRepository;
import com.spaceinje.products.repository.brand.BrandSubProductRepository;
import com.spaceinje.products.service.BrandMasterService;
import com.spaceinje.products.specification.BrandSpecs;
import com.spaceinje.products.specification.CommonSpecs;
import com.spaceinje.products.util.ProductConstants;
import com.spaceinje.products.util.ResponseCachet;

import brave.Tracer;

@SuppressWarnings("unchecked")
@Service
public class BrandMasterServiceImpl implements BrandMasterService {

	private static final Logger log = LoggerFactory.getLogger(BrandMasterServiceImpl.class);

	@Autowired
	private Tracer tracer;

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private BrandProductRepository brandProductRepository;

	@Autowired
	private BrandSubProductRepository brandSubProductRepository;

	@Autowired
	private ProductConstants constants;

	@Autowired
	private BrandConverter brandConverter;

	@Autowired
	private BrandProductConverter brandProductConverter;

	@Autowired
	private BrandSubProductConverter brandSubProductConverter;

	@Override
	public ResponseCachet<?> saveOrUpdateBrand(BrandBean bean) {
		ResponseCachet<BrandBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of brand with given shortcode");
			Optional<Brand> retrievedBrand = brandRepository.findOne(
					CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedBrand.isPresent() && !retrievedBrand.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			Brand brand = brandConverter.beanToEntity(bean);
			brand = brandRepository.save(brand);
			log.debug("successfully saved brand with name : {}", brand.getBrandName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("brand with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(brandConverter.entityToBean(brand));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving brand: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandBean>> fetchAllBrands(String tenantCode) {
		ResponseCachet<List<BrandBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Brand> list = brandRepository.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Brand found");
				cachet.setData(new ArrayList<>());
				log.debug("No Brand found to the tenant");
				return cachet;
			}
			log.info("Successfully retrieved all brands");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brands");
			cachet.setData(list.stream().map(ele -> brandConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all brands: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandBean>> fetchBrandsByMainGroup(String tenantCode, String mainGroupId) {
		ResponseCachet<List<BrandBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<Brand> list = brandRepository
					.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
							.and(CommonSpecs.mainGroupIdSpec(mainGroupId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Brand found for given mainGroup");
				cachet.setData(new ArrayList<>());
				log.debug("No Brand found to the given mainGroup");
				return cachet;
			}
			log.info("Successfully retrieved all brands associated to given mainGroup");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brands associated to given mainGroup");
			cachet.setData(list.stream().map(ele -> brandConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving brands associated to given mainGroup: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteBrands(Set<String> brandIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (brandRepository.brandHasOrphans(brandIds)) {
				log.debug("Brand(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Brand(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			brandRepository.deleteBrands(brandIds);
			log.debug("deleted brand(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted brand(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting brands: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateProduct(BrandProductBean bean) {
		ResponseCachet<BrandProductBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of brand product with given shortcode");
			Optional<BrandProduct> retrievedProduct = brandProductRepository.findOne(
					CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedProduct.isPresent() && !retrievedProduct.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			BrandProduct brandProduct = brandProductConverter.beanToEntity(bean);
			brandProduct = brandProductRepository.save(brandProduct);
			log.debug("successfully saved brand product with name : {}", brandProduct.getProductName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("brand product with shortcode " + bean.getShortCode() + " saved successfully");
			cachet.setData(brandProductConverter.entityToBean(brandProduct));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving brand products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandProductBean>> fetchAllProducts(String tenantCode) {
		ResponseCachet<List<BrandProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandProduct> list = brandProductRepository
					.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Brand Product found");
				cachet.setData(new ArrayList<>());
				log.debug("No Brand Product found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all brand products");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brand products");
			cachet.setData(
					list.stream().map(ele -> brandProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all brand products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandProductBean>> fetchProductsByBrand(String tenantCode, String brandId) {
		ResponseCachet<List<BrandProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandProduct> list = brandProductRepository
					.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
							.and(BrandSpecs.brandIdSpec(brandId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Brand Product found for given brand");
				cachet.setData(new ArrayList<>());
				log.debug("No Brand Product found to the given brand");
				return cachet;
			}
			log.info("Successfully retrieved brand products associated to given brand");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brand products associated to given brand");
			cachet.setData(
					list.stream().map(ele -> brandProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving brand products associated to given brand: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}
	
	@Override
	public ResponseCachet<List<BrandSubProductBean>> fetchSubProductsByProduct(String tenantCode, String productId) {
		ResponseCachet<List<BrandSubProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandSubProduct> list = brandSubProductRepository
					.findAll(Specification.where(CommonSpecs.findByTenantCodeSpec(tenantCode))
							.and(BrandSpecs.brandProductIdSpec(productId)));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Sub Product found for given Product");
				cachet.setData(new ArrayList<>());
				log.debug("No Sub Product found to the given Product");
				return cachet;
			}
			log.info("Successfully retrieved sub products associated to given product");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved sub products associated to given product");
			cachet.setData(
					list.stream().map(ele -> brandSubProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving sub products associated to given product: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteProducts(Set<String> productIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			if (brandProductRepository.brandProductsHasOrphans(productIds)) {
				log.debug("Brand Product(s) has children data associated with given Id's");
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("Brand Product(s) can't be deleted, as they are already in use.!");
				return cachet;
			}
			brandProductRepository.deleteProducts(productIds);
			log.debug("deleted brand product(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted brand product(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting brand products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> saveOrUpdateSubProduct(BrandSubProductBean bean) {
		ResponseCachet<BrandSubProductBean> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			log.debug("check whether tenant consists of brand sub product with given shortcode");
			Optional<BrandSubProduct> retrievedSubProduct = brandSubProductRepository.findOne(
					CommonSpecs.findByTenantAndShortCodeSpec(bean.getTenantCode(), bean.getShortCode()));
			if (retrievedSubProduct.isPresent() && !retrievedSubProduct.get().getId().equals(bean.getId())) {
				cachet.setStatus(constants.getFAILURE());
				cachet.setMessage("short code already exists");
				log.debug("Short code already exists to the tenant {}", bean.getTenantCode());
				return cachet;
			}
			BrandSubProduct brandSubProduct = brandSubProductConverter.beanToEntity(bean);
			brandSubProduct = brandSubProductRepository.save(brandSubProduct);
			log.debug("successfully saved brand sub product with name : {}", brandSubProduct.getSubProductName());
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("brand sub product with shortCode " + bean.getShortCode() + " saved successfully");
			cachet.setData(brandSubProductConverter.entityToBean(brandSubProduct));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while saving brand sub product: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<List<BrandSubProductBean>> fetchAllSubProducts(String tenantCode) {
		ResponseCachet<List<BrandSubProductBean>> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			List<BrandSubProduct> list = brandSubProductRepository
					.findAll(CommonSpecs.findByTenantCodeSpec(tenantCode));
			if (CollectionUtils.isEmpty(list)) {
				cachet.setStatus(constants.getSUCCESS());
				cachet.setMessage("No Brand Sub Product found");
				cachet.setData(new ArrayList<>());
				log.debug("No Brand Sub Product found to the tenant");
				return cachet;
			}
			log.debug("Successfully retrieved all brand sub products");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("successfully retrieved brand sub products");
			cachet.setData(
					list.stream().map(ele -> brandSubProductConverter.entityToBean(ele)).collect(Collectors.toList()));
			return cachet;
		} catch (Exception e) {
			log.error("Exception while retrieving all brand sub products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

	@Override
	public ResponseCachet<?> deleteSubProducts(Set<String> subProductIds) {
		ResponseCachet<?> cachet = new ResponseCachet<>();
		cachet.setTraceId(tracer.currentSpan().toString().replace("NoopSpan(", "").replace(")", "").split("/")[0]
				.replace("LazySpan(", ""));
		try {
			brandSubProductRepository.deleteSubProducts(subProductIds);
			log.debug("deleted brand sub product(s) successfully");
			cachet.setStatus(constants.getSUCCESS());
			cachet.setMessage("deleted brand sub product(s) successfully");
			return cachet;
		} catch (Exception e) {
			log.error("Exception while deleting brand sub products: {}", e);
			cachet.setStatus(constants.getEXCEPTION());
			cachet.setMessage(e.getMessage());
			return cachet;
		}
	}

}
