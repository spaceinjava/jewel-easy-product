package com.spaceinje.products.service;

import java.util.Set;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.util.ResponseCachet;

@SuppressWarnings("rawtypes")
public interface BrandMasterService {

	/*
	 * services for brand
	 */
	public ResponseCachet saveOrUpdateBrand(BrandBean bean);
	public ResponseCachet fetchAllBrands(String tenantCode);
	public ResponseCachet fetchBrandsByMainGroup(String tenantCode, String mainGroupId);
	public ResponseCachet deleteBrands(Set<String> brandIds);
	
	/*
	 * services for brand products
	 */
	public ResponseCachet saveOrUpdateProduct(BrandProductBean bean);
	public ResponseCachet fetchAllProducts(String tenantCode);
	public ResponseCachet fetchProductsByBrand(String tenantCode, String brandId);
	public ResponseCachet fetchSubProductsByProduct(String tenantCode, String productId);
	public ResponseCachet deleteProducts(Set<String> productIds);
	
	/*
	 * services for brand sub products
	 */
	public ResponseCachet saveOrUpdateSubProduct(BrandSubProductBean bean);
	public ResponseCachet fetchAllSubProducts(String tenantCode);
	public ResponseCachet deleteSubProducts(Set<String> productIds);

}
