package com.spaceinje.products.bean.brand.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class BrandSubProductBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "sub product name can't be null")
	private String subProductName;

	@NotNull(message = "brand product can't be null")
	private BrandProductBean brandProduct;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSubProductName() {
		return subProductName;
	}

	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}

	public BrandProductBean getBrandProduct() {
		return brandProduct;
	}

	public void setBrandProduct(BrandProductBean brandProduct) {
		this.brandProduct = brandProduct;
	}

	@Override
	public String toString() {
		return "BrandSubProductBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", subProductName="
				+ subProductName + ", brandProduct=" + brandProduct + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}

}
