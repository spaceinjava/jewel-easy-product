package com.spaceinje.products.bean.brand.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.master.MainGroupBean;

public class BrandBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "brand name can't be null")
	private String brandName;

	@NotNull(message = "main group can't be null")
	private MainGroupBean mainGroup;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public MainGroupBean getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroupBean mainGroup) {
		this.mainGroup = mainGroup;
	}

	@Override
	public String toString() {
		return "BrandBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", brandName=" + brandName
				+ ", mainGroup=" + mainGroup + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
