package com.spaceinje.products.bean.brand.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class BrandProductBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "brand product can't be null")
	private String productName;

	@NotNull(message = "GST can't be null")
	private double gst;

	@NotNull(message = "tray can't be null")
	private boolean tray;

	@NotNull(message = "brand name can't be null")
	private BrandBean brand;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getGst() {
		return gst;
	}

	public void setGst(double gst) {
		this.gst = gst;
	}

	public boolean isTray() {
		return tray;
	}

	public void setTray(boolean tray) {
		this.tray = tray;
	}

	public BrandBean getBrand() {
		return brand;
	}

	public void setBrand(BrandBean brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "BrandProductBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", productName="
				+ productName + ", gst=" + gst + ", tray=" + tray + ", brand=" + brand + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
