package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.master.MainGroupBean;

public class PurityBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotNull(message = "main group should not be empty")
	private MainGroupBean mainGroup;

	@NotNull(message = "purity should not be empty")
	private double purity;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public MainGroupBean getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroupBean mainGroup) {
		this.mainGroup = mainGroup;
	}

	public double getPurity() {
		return purity;
	}

	public void setPurity(double purity) {
		this.purity = purity;
	}

	@Override
	public String toString() {
		return "PurityBean [tenantCode=" + tenantCode + ", mainGroup=" + mainGroup + ", purity=" + purity + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
