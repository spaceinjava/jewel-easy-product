package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class CounterBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotNull(message = "floor details should not be empty")
	private FloorBean floor;

	@NotEmpty(message = "short code should not be empty")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	private String counterName;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public FloorBean getFloor() {
		return floor;
	}

	public void setFloor(FloorBean floor) {
		this.floor = floor;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCounterName() {
		return counterName;
	}

	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}

	@Override
	public String toString() {
		return "CounterBean [tenantCode=" + tenantCode + ", floor=" + floor + ", shortCode=" + shortCode
				+ ", counterName=" + counterName + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
