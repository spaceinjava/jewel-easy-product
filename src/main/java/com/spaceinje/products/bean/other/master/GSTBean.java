package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.master.MetalTypeBean;

public class GSTBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotNull(message = "metal type should not be empty")
	private MetalTypeBean metalType;

	@NotNull(message = "gst percent should not be empty")
	private double percent;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public MetalTypeBean getMetalType() {
		return metalType;
	}

	public void setMetalType(MetalTypeBean metalType) {
		this.metalType = metalType;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return "GSTBean [tenantCode=" + tenantCode + ", metalType=" + metalType + ", percent=" + percent + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
