package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.master.MainGroupBean;

public class RateBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotNull(message = "main group should not be empty")
	private MainGroupBean mainGroup;

	@NotNull(message = "perGramRate should not be empty")
	private double perGramRate;

	@NotNull(message = "purityBean should not be empty")
	private PurityBean purity;

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	@NotNull(message = "purityRate should not be empty")
	private double purityRate;

	@NotNull(message = "display should not be empty")
	private boolean display;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public MainGroupBean getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroupBean mainGroup) {
		this.mainGroup = mainGroup;
	}

	public double getPerGramRate() {
		return perGramRate;
	}

	public void setPerGramRate(double perGramRate) {
		this.perGramRate = perGramRate;
	}

	

	public double getPurityRate() {
		return purityRate;
	}

	public void setPurityRate(double purityRate) {
		this.purityRate = purityRate;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	@Override
	public String toString() {
		return "RateBean [tenantCode=" + tenantCode + ", mainGroup=" + mainGroup + ", perGramRate=" + perGramRate
				+ ", purity=" + purity + ", purityRate=" + purityRate + ", display=" + display + "]";
	}

	

}
