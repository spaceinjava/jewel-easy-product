package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class FloorBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotEmpty(message = "floor name should not be empty")
	private String floorName;

	@NotEmpty(message = "short code should not be empty")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	@Override
	public String toString() {
		return "FloorBean [tenantCode=" + tenantCode + ", floorName=" + floorName + ", shortCode=" + shortCode
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
