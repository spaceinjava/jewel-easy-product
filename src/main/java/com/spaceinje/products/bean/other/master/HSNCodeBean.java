package com.spaceinje.products.bean.other.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.master.MainGroupBean;

public class HSNCodeBean extends BaseBean {

	@NotEmpty(message = "tenant code should not be empty")
	private String tenantCode;

	@NotNull(message = "main group should not be empty")
	private MainGroupBean mainGroup;

	@NotEmpty(message = "HSN code should not be empty")
	private String hsnCode;

	@NotEmpty(message = "description should not be empty")
	private String description;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public MainGroupBean getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroupBean mainGroup) {
		this.mainGroup = mainGroup;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "HSNCodeBean [tenantCode=" + tenantCode + ", mainGroup=" + mainGroup + ", hsnCode=" + hsnCode
				+ ", description=" + description + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
