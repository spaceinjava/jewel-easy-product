package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class StoneTypeBean extends BaseBean {


	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "stone type name can't be null")
	private String typeName;


	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		return "StoneTypeBean [shortCode=" + shortCode + ", typeName=" + typeName + "]";
	}

	

}
