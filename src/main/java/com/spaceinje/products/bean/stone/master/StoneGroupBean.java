package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class StoneGroupBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotNull(message = "stone type can't be null")
	private StoneTypeBean stoneType;

	@NotEmpty(message = "stone group name can't be null")
	private String groupName;

	@NotNull(message = "diamond can't be null")
	private boolean diamond;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public StoneTypeBean getStoneType() {
		return stoneType;
	}

	public void setStoneType(StoneTypeBean stoneType) {
		this.stoneType = stoneType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isDiamond() {
		return diamond;
	}

	public void setDiamond(boolean diamond) {
		this.diamond = diamond;
	}

	@Override
	public String toString() {
		return "StoneGroupBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", stoneType=" + stoneType
				+ ", groupName=" + groupName + ", diamond=" + diamond + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}

}
