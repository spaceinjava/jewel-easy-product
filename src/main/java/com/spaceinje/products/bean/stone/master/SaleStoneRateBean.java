package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;

public class SaleStoneRateBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotNull(message = "min rate can't be null")
	private double minRate;

	@NotNull(message = "max rate can't be null")
	private double maxRate;

	@NotEmpty(message = "UOM can't be null")
	private String uom;

	@NotNull(message = "noWeight can't be null")
	private boolean noWeight;

	@NotNull(message = "fixed rate sale can't be null")
	private boolean saleRate;

	@NotNull(message = "fixed rate barcode can't be null")
	private boolean barcodeRate;

	@NotNull(message = "update existing stock can't be null")
	private boolean updateStock;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public double getMinRate() {
		return minRate;
	}

	public void setMinRate(double minRate) {
		this.minRate = minRate;
	}

	public double getMaxRate() {
		return maxRate;
	}

	public void setMaxRate(double maxRate) {
		this.maxRate = maxRate;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public boolean isNoWeight() {
		return noWeight;
	}

	public void setNoWeight(boolean noWeight) {
		this.noWeight = noWeight;
	}

	public boolean isSaleRate() {
		return saleRate;
	}

	public void setSaleRate(boolean saleRate) {
		this.saleRate = saleRate;
	}

	public boolean isBarcodeRate() {
		return barcodeRate;
	}

	public void setBarcodeRate(boolean barcodeRate) {
		this.barcodeRate = barcodeRate;
	}

	public boolean isUpdateStock() {
		return updateStock;
	}

	public void setUpdateStock(boolean updateStock) {
		this.updateStock = updateStock;
	}

	@Override
	public String toString() {
		return "SaleStoneRateBean [tenantCode=" + tenantCode + ", stone=" + stone + ", stoneSize=" + stoneSize
				+ ", stoneColor=" + stoneColor + ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish
				+ ", minRate=" + minRate + ", maxRate=" + maxRate + ", uom=" + uom + ", noWeight=" + noWeight
				+ ", saleRate=" + saleRate + ", barcodeRate=" + barcodeRate + ", updateStock=" + updateStock
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
