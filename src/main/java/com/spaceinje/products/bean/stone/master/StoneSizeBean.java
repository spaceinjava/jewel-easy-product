package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class StoneSizeBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;
	
	@NotNull(message = "stone size can't be null")
	private double stoneSize;

	@NotNull(message = "stone group can't be null")
	private StoneGroupBean stoneGroup;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public double getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(double stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneGroupBean getStoneGroup() {
		return stoneGroup;
	}

	public void setStoneGroup(StoneGroupBean stoneGroup) {
		this.stoneGroup = stoneGroup;
	}

	@Override
	public String toString() {
		return "StoneSizeBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", stoneSize=" + stoneSize
				+ ", stoneGroup=" + stoneGroup + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}
	
}
