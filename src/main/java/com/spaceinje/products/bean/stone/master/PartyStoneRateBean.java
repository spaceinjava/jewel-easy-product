package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;

public class PartyStoneRateBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotNull(message = "party can't be null")
	private String partyId;

	@NotNull(message = "stone can't be null")
	private StoneBean stone;

	private StoneSizeBean stoneSize;

	private StoneColorBean stoneColor;

	private StoneClarityBean stoneClarity;

	private StonePolishBean stonePolish;

	@NotNull(message = "min rate can't be null")
	private double minRate;

	@NotNull(message = "max rate can't be null")
	private double maxRate;

	@NotEmpty(message = "UOM can't be null")
	private String uom;

	@NotNull(message = "noWeight can't be null")
	private boolean noWeight;

	@NotNull(message = "fixedRate can't be null")
	private boolean fixedRate;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public StoneBean getStone() {
		return stone;
	}

	public void setStone(StoneBean stone) {
		this.stone = stone;
	}

	public StoneSizeBean getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSizeBean stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColorBean getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColorBean stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarityBean getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarityBean stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolishBean getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolishBean stonePolish) {
		this.stonePolish = stonePolish;
	}

	public double getMinRate() {
		return minRate;
	}

	public void setMinRate(double minRate) {
		this.minRate = minRate;
	}

	public double getMaxRate() {
		return maxRate;
	}

	public void setMaxRate(double maxRate) {
		this.maxRate = maxRate;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public boolean isNoWeight() {
		return noWeight;
	}

	public void setNoWeight(boolean noWeight) {
		this.noWeight = noWeight;
	}

	public boolean isFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(boolean fixedRate) {
		this.fixedRate = fixedRate;
	}

	@Override
	public String toString() {
		return "PartyStoneRateBean [tenantCode=" + tenantCode + ", partyId=" + partyId + ", stone=" + stone
				+ ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor + ", stoneClarity=" + stoneClarity
				+ ", stonePolish=" + stonePolish + ", minRate=" + minRate + ", maxRate=" + maxRate + ", uom=" + uom
				+ ", noWeight=" + noWeight + ", fixedRate=" + fixedRate + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + "]";
	}

}
