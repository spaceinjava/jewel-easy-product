package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class StonePolishBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "stone polish name can't be null")
	private String polishName;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getPolishName() {
		return polishName;
	}

	public void setPolishName(String polishName) {
		this.polishName = polishName;
	}

	@Override
	public String toString() {
		return "StonePolishBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", polishName=" + polishName
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
