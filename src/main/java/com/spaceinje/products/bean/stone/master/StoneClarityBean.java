package com.spaceinje.products.bean.stone.master;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.spaceinje.products.bean.base.BaseBean;

public class StoneClarityBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "short code can't be null")
	@Length(min = 3, max = 3, message = "short code should be 3 characters")
	private String shortCode;

	@NotEmpty(message = "stone clarity name can't be null")
	private String clarityName;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getClarityName() {
		return clarityName;
	}

	public void setClarityName(String clarityName) {
		this.clarityName = clarityName;
	}

	@Override
	public String toString() {
		return "StoneClarityBean [tenantCode=" + tenantCode + ", shortCode=" + shortCode + ", clarityName="
				+ clarityName + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
