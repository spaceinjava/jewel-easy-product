package com.spaceinje.products.bean.master;

import javax.validation.constraints.NotEmpty;

import com.spaceinje.products.bean.base.BaseBean;

public class PartyTypeBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotEmpty(message = "party type can't be null")
	private String partyType;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	@Override
	public String toString() {
		return "PartyTypeBean [tenantCode=" + tenantCode + ", partyType=" + partyType + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
