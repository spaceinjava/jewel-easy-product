package com.spaceinje.products.bean.product.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;

public class ProductSizeBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotNull(message = "size can't be null")
	private double productSize;

	@NotNull(message = "product can't be null")
	private ProductBean product;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public double getProductSize() {
		return productSize;
	}

	public void setProductSize(double productSize) {
		this.productSize = productSize;
	}

	public ProductBean getProduct() {
		return product;
	}

	public void setProduct(ProductBean product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductSizeBean [tenantCode=" + tenantCode + ", productSize=" + productSize + ", product=" + product
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
