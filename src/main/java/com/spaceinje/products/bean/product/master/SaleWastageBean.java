package com.spaceinje.products.bean.product.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.other.master.PurityBean;

public class SaleWastageBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotNull(message = "sub product can't be null")
	private SubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	@NotNull(message = "product size can't be null")
	private ProductSizeBean productSize;

	@NotNull(message = "from weight can't be null")
	private double fromWeight;

	@NotNull(message = "to weight can't be null")
	private double toWeight;

	@NotNull(message = "max wst per gram can't be null")
	private double maxWstPerGram;

	@NotNull(message = "min wst per gram can't be null")
	private double minWstPerGram;

	@NotNull(message = "max dir wst can't be null")
	private double maxDirWst;

	@NotNull(message = "min dir wst can't be null")
	private double minDirWst;

	@NotNull(message = "max mc per gram can't be null")
	private double maxMcPerGram;

	@NotNull(message = "min mc per gram can't be null")
	private double minMcPerGram;

	@NotNull(message = "min dir mc can't be null")
	private double minDirMc;

	@NotNull(message = "max dir mc can't be null")
	private double maxDirMc;

	@NotEmpty(message = "mcOn can't be null")
	private String mcOn;

	@NotEmpty(message = "wastageOn can't be null")
	private String wastageOn;

	private boolean incWstMc;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public SubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public ProductSizeBean getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSizeBean productSize) {
		this.productSize = productSize;
	}

	public double getFromWeight() {
		return fromWeight;
	}

	public void setFromWeight(double fromWeight) {
		this.fromWeight = fromWeight;
	}

	public double getToWeight() {
		return toWeight;
	}

	public void setToWeight(double toWeight) {
		this.toWeight = toWeight;
	}

	public double getMaxWstPerGram() {
		return maxWstPerGram;
	}

	public void setMaxWstPerGram(double maxWstPerGram) {
		this.maxWstPerGram = maxWstPerGram;
	}

	public double getMinWstPerGram() {
		return minWstPerGram;
	}

	public void setMinWstPerGram(double minWstPerGram) {
		this.minWstPerGram = minWstPerGram;
	}

	public double getMaxDirWst() {
		return maxDirWst;
	}

	public void setMaxDirWst(double maxDirWst) {
		this.maxDirWst = maxDirWst;
	}

	public double getMinDirWst() {
		return minDirWst;
	}

	public void setMinDirWst(double minDirWst) {
		this.minDirWst = minDirWst;
	}

	public double getMaxMcPerGram() {
		return maxMcPerGram;
	}

	public void setMaxMcPerGram(double maxMcPerGram) {
		this.maxMcPerGram = maxMcPerGram;
	}

	public double getMinMcPerGram() {
		return minMcPerGram;
	}

	public void setMinMcPerGram(double minMcPerGram) {
		this.minMcPerGram = minMcPerGram;
	}

	public double getMinDirMc() {
		return minDirMc;
	}

	public void setMinDirMc(double minDirMc) {
		this.minDirMc = minDirMc;
	}

	public double getMaxDirMc() {
		return maxDirMc;
	}

	public void setMaxDirMc(double maxDirMc) {
		this.maxDirMc = maxDirMc;
	}

	public String getMcOn() {
		return mcOn;
	}

	public void setMcOn(String mcOn) {
		this.mcOn = mcOn;
	}

	public String getWastageOn() {
		return wastageOn;
	}

	public void setWastageOn(String wastageOn) {
		this.wastageOn = wastageOn;
	}

	public boolean isIncWstMc() {
		return incWstMc;
	}

	public void setIncWstMc(boolean incWstMc) {
		this.incWstMc = incWstMc;
	}

	@Override
	public String toString() {
		return "SaleWastageBean [tenantCode=" + tenantCode + ", subProduct=" + subProduct + ", purity=" + purity
				+ ", productSize=" + productSize + ", fromWeight=" + fromWeight + ", toWeight=" + toWeight
				+ ", maxWstPerGram=" + maxWstPerGram + ", minWstPerGram=" + minWstPerGram + ", maxDirWst=" + maxDirWst
				+ ", minDirWst=" + minDirWst + ", maxMcPerGram=" + maxMcPerGram + ", minMcPerGram=" + minMcPerGram
				+ ", minDirMc=" + minDirMc + ", maxDirMc=" + maxDirMc + ", mcOn=" + mcOn + ", wastageOn=" + wastageOn
				+ ", incWstMc=" + incWstMc + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
