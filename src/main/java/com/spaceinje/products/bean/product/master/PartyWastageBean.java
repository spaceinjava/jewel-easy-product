package com.spaceinje.products.bean.product.master;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.spaceinje.products.bean.base.BaseBean;
import com.spaceinje.products.bean.other.master.PurityBean;

public class PartyWastageBean extends BaseBean {

	@NotEmpty(message = "Tenant code can't be null")
	private String tenantCode;

	@NotNull(message = "party can't be null")
	private String partyId;

	@NotNull(message = "sub product can't be null")
	private SubProductBean subProduct;

	@NotNull(message = "purity can't be null")
	private PurityBean purity;

	@NotNull(message = "product size can't be null")
	private ProductSizeBean productSize;

	@NotNull(message = "touch value can't be null")
	private double touchValue;

	@NotNull(message = "touch percent can't be null")
	private double touchPercent;

	@NotNull(message = "mc per gram can't be null")
	private double mcPerGram;

	@NotNull(message = "mc per dir can't be null")
	private double mcPerDir;

	@NotNull(message = "wastage per gram can't be null")
	private double wastagePerGram;

	@NotNull(message = "wastage per dir can't be null")
	private double wastagePerDir;

//	@NotEmpty(message = "mcOn can't be null")
//	private String mcOn;
//
//	@NotEmpty(message = "wastageOn can't be null")
//	private String wastageOn;

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public SubProductBean getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProductBean subProduct) {
		this.subProduct = subProduct;
	}

	public PurityBean getPurity() {
		return purity;
	}

	public void setPurity(PurityBean purity) {
		this.purity = purity;
	}

	public ProductSizeBean getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSizeBean productSize) {
		this.productSize = productSize;
	}

	public double getTouchValue() {
		return touchValue;
	}

	public void setTouchValue(double touchValue) {
		this.touchValue = touchValue;
	}

	public double getTouchPercent() {
		return touchPercent;
	}

	public void setTouchPercent(double touchPercent) {
		this.touchPercent = touchPercent;
	}

	public double getMcPerGram() {
		return mcPerGram;
	}

	public void setMcPerGram(double mcPerGram) {
		this.mcPerGram = mcPerGram;
	}

	public double getMcPerDir() {
		return mcPerDir;
	}

	public void setMcPerDir(double mcPerDir) {
		this.mcPerDir = mcPerDir;
	}

	public double getWastagePerGram() {
		return wastagePerGram;
	}

	public void setWastagePerGram(double wastagePerGram) {
		this.wastagePerGram = wastagePerGram;
	}

	public double getWastagePerDir() {
		return wastagePerDir;
	}

	public void setWastagePerDir(double wastagePerDir) {
		this.wastagePerDir = wastagePerDir;
	}

//	public String getMcOn() {
//		return mcOn;
//	}
//
//	public void setMcOn(String mcOn) {
//		this.mcOn = mcOn;
//	}
//
//	public String getWastageOn() {
//		return wastageOn;
//	}
//
//	public void setWastageOn(String wastageOn) {
//		this.wastageOn = wastageOn;
//	}

	@Override
	public String toString() {
		return "PartyWastageBean [tenantCode=" + tenantCode + ", partyId=" + partyId + ", subProduct=" + subProduct
				+ ", purity=" + purity + ", productSize=" + productSize + ", touchValue=" + touchValue
				+ ", touchPercent=" + touchPercent + ", mcPerGram=" + mcPerGram + ", mcPerDir=" + mcPerDir
				+ ", wastagePerGram=" + wastagePerGram + ", wastagePerDir=" + wastagePerDir + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
