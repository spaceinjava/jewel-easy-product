package com.spaceinje.products.repository.product;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.product.master.SubProduct;

@Repository
public interface SubProductRepository extends JpaRepository<SubProduct, String>, JpaSpecificationExecutor<SubProduct> {

	@Query(value = "select case when count(sp) > 0 then true else false end from SubProduct sp "
			+ " left join sp.partyWastages pw on pw.subProduct = sp left join sp.saleWastages sw on sw.subProduct = sp "
			+ " left join sp.productDetails pd on pd.subProduct = sp left join sp.brandedDetails bd on bd.subProduct = sp "
			+ " where (pw.isDeleted = false or sw.isDeleted = false or pd.isDeleted = false or bd.isDeleted = false) and sp.id in (?1)")
	public boolean subProductHasOrphans(Set<String> subProductIds);

	@Modifying
	@Transactional
	@Query(value = "update SubProduct set isDeleted=true where id in (?1)")
	public void deleteSubProducts(Set<String> subProductIds);

}
