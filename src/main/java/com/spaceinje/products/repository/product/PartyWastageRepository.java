package com.spaceinje.products.repository.product;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.product.master.PartyWastage;

@Repository
public interface PartyWastageRepository
		extends JpaRepository<PartyWastage, String>, JpaSpecificationExecutor<PartyWastage> {

	@Modifying
	@Transactional
	@Query(value = "update PartyWastage set isDeleted=true where id in (?1)")
	public void deletePartyWastages(Set<String> partyWastageIds);

}
