package com.spaceinje.products.repository.product;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.product.master.ProductSize;

@Repository
public interface ProductSizeRepository
		extends JpaRepository<ProductSize, String>, JpaSpecificationExecutor<ProductSize> {

	@Query(value = "select case when count(ps) > 0 then true else false end from ProductSize ps left join ps.partyWastages pw on pw.productSize = ps "
			+ " left join ps.saleWastages sw on sw.productSize = ps left join ps.productDetails pd on pd.productSize = ps "
			+ " where (pw.isDeleted = false or sw.isDeleted = false or pd.isDeleted = false) and ps.id in (?1)")
	public boolean productSizeHasOrphans(Set<String> productSizeIds);

	@Modifying
	@Transactional
	@Query(value = "update ProductSize set isDeleted=true where id in (?1)")
	public void deleteProductSizes(Set<String> productSizeIds);

}
