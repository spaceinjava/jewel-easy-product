package com.spaceinje.products.repository.product;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.product.master.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String>, JpaSpecificationExecutor<Product> {

	@Query(value = "select case when count(p) > 0 then true else false end from Product p left join p.subProducts sp on sp.product = p "
			+ " left join p.productSizes ps on ps.product = p where (sp.isDeleted = false or ps.isDeleted = false) and p.id in (?1)")
	public boolean productHasOrphans(Set<String> productIds);

	@Modifying
	@Transactional
	@Query(value = "update Product set isDeleted = true where id in (?1)")
	public void deleteProducts(Set<String> productIds);

}
