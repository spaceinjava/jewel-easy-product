package com.spaceinje.products.repository.product;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.product.master.SaleWastage;

@Repository
public interface SaleWastageRepository
		extends JpaRepository<SaleWastage, String>, JpaSpecificationExecutor<SaleWastage> {

	@Modifying
	@Transactional
	@Query(value = "update SaleWastage set isDeleted=true where id in (?1)")
	public void deleteSaleWastages(Set<String> saleWastageIds);

}
