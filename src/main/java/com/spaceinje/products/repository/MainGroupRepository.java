package com.spaceinje.products.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.master.MainGroup;

@Repository
public interface MainGroupRepository extends JpaRepository<MainGroup, String>, JpaSpecificationExecutor<MainGroup> {

	@Query(value = "select case when count(mg) > 0 then true else false end from MainGroup mg left join mg.brands b on b.mainGroup = mg "
			+ " left join mg.products p on p.mainGroup = mg left join mg.purities pu on pu.mainGroup = mg "
			+ " where (b.isDeleted = false or p.isDeleted = false or pu.isDeleted = false) and mg.id in (?1)")
	public boolean mainGroupHasOrphans(Set<String> mainGroupIds);

	@Modifying
	@Transactional
	@Query(value = "update MainGroup set isDeleted = true where id in (?1)")
	public void deleteMainGroups(Set<String> mainGroupIds);

}
