package com.spaceinje.products.repository.brand;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.brand.master.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, String>, JpaSpecificationExecutor<Brand> {

	@Query(value = "select case when count(b) > 0 then true else false end from Brand b left join b.brandProducts bp on bp.brand = b "
			+ " where bp.isDeleted = false and b.id in (?1)")
	public boolean brandHasOrphans(Set<String> brandIds);

	@Modifying
	@Transactional
	@Query(value = "update Brand set isDeleted=true where id in (?1)")
	public void deleteBrands(Set<String> brandIds);

}
