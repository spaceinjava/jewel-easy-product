package com.spaceinje.products.repository.brand;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.brand.master.BrandSubProduct;

@Repository
public interface BrandSubProductRepository extends JpaRepository<BrandSubProduct, String>, JpaSpecificationExecutor<BrandSubProduct> {

	@Modifying
	@Transactional
	@Query(value = "update BrandSubProduct set isDeleted=true where id in (?1)")
	public void deleteSubProducts(Set<String> subProductIds);

}
