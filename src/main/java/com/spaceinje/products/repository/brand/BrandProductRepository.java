package com.spaceinje.products.repository.brand;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.brand.master.BrandProduct;

@Repository
public interface BrandProductRepository
		extends JpaRepository<BrandProduct, String>, JpaSpecificationExecutor<BrandProduct> {

	@Query(value = "select case when count(bp) > 0 then true else false end from BrandProduct bp "
			+ " left join bp.brandSubProducts bsp on bsp.brandProduct = bp where bsp.isDeleted = false and bp.id in (?1)")
	public boolean brandProductsHasOrphans(Set<String> productIds);

	@Modifying
	@Transactional
	@Query(value = "update BrandProduct set isDeleted=true where id in (?1)")
	public void deleteProducts(Set<String> productIds);

}
