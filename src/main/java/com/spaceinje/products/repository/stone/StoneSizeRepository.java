package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.StoneSize;

@Repository
public interface StoneSizeRepository extends JpaRepository<StoneSize, String>, JpaSpecificationExecutor<StoneSize> {

	@Query(value = "select case when count(ss) > 0 then true else false end from StoneSize ss "
			+ " left join ss.partyStoneRates psr on psr.stoneSize = ss left join ss.saleStoneRates ssr on ssr.stoneSize = ss "
			+ " left join ss.stoneDetails sd on sd.stoneSize = ss "
			+ " where (psr.isDeleted = false or ssr.isDeleted = false or sd.isDeleted = false) and ss.id in (?1)")
	public boolean stoneSizeHasOrphans(Set<String> stoneSizeIds);

	@Modifying
	@Transactional
	@Query(value = "update StoneSize set isDeleted=true where id in (?1)")
	public void deleteStoneSizes(Set<String> stoneSizeIds);

}
