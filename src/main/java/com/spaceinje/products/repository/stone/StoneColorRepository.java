package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.StoneColor;

@Repository
public interface StoneColorRepository extends JpaRepository<StoneColor, String>, JpaSpecificationExecutor<StoneColor> {

	@Query(value = "select case when count(sc) > 0 then true else false end from StoneColor sc "
			+ " left join sc.partyStoneRates psr on psr.stoneClarity = sc left join sc.saleStoneRates ssr on ssr.stoneClarity = sc "
			+ " where (psr.isDeleted = false or ssr.isDeleted = false) and sc.id in (?1)")
	public boolean stoneColorHasOrphans(Set<String> stoneColorIds);

	@Modifying
	@Transactional
	@Query(value = "update StoneColor set isDeleted=true where id in (?1)")
	public void deleteStoneColors(Set<String> stoneColorIds);

}
