package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.Stone;

@Repository
public interface StoneRepository extends JpaRepository<Stone, String>, JpaSpecificationExecutor<Stone> {

	@Query(value = "select case when count(s) > 0 then true else false end from Stone s left join s.partyStoneRates psr on psr.stone = s "
			+ " left join s.saleStoneRates ssr on ssr.stone = s where (psr.isDeleted = false or ssr.isDeleted = false) and s.id in (?1)")
	public boolean stoneHasOrphans(Set<String> stoneIds);

	@Modifying
	@Transactional
	@Query(value = "update Stone set isDeleted=true where id in (?1)")
	public void deleteStones(Set<String> stoneIds);

}
