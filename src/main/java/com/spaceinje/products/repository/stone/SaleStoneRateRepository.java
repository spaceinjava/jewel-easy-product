package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.SaleStoneRate;

@Repository
public interface SaleStoneRateRepository extends JpaRepository<SaleStoneRate, String>, JpaSpecificationExecutor<SaleStoneRate> {

	@Modifying
	@Transactional
	@Query(value = "update SaleStoneRate set isDeleted=true where id in (?1)")
	public void deleteSaleRates(Set<String> saleRateIds);

}
