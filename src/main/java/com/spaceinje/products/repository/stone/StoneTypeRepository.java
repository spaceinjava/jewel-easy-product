package com.spaceinje.products.repository.stone;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.products.model.stone.master.StoneType;

@Repository
public interface StoneTypeRepository extends JpaRepository<StoneType, String>, JpaSpecificationExecutor<StoneType> {

}
