package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.StoneGroup;

@Repository
public interface StoneGroupRepository extends JpaRepository<StoneGroup, String>, JpaSpecificationExecutor<StoneGroup> {

	@Query(value = "select case when count(sg) > 0 then true else false end from StoneGroup sg "
			+ " left join sg.stoneSizes ss on ss.stoneGroup = sg left join sg.stones s on s.stoneGroup = sg "
			+ " where (ss.isDeleted = false or s.isDeleted = false) and sg.id in (?1)")
	public boolean stoneGroupHasOrphans(Set<String> stoneGroupIds);

	@Modifying
	@Transactional
	@Query(value = "update StoneGroup set isDeleted=true where id in (?1)")
	public void deleteStoneGroups(Set<String> stoneGroupIds);

}
