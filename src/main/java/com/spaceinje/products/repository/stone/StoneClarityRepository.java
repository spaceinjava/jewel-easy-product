package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.StoneClarity;

@Repository
public interface StoneClarityRepository
		extends JpaRepository<StoneClarity, String>, JpaSpecificationExecutor<StoneClarity> {

	@Query(value = "select case when count(sc) > 0 then true else false end from StoneClarity sc "
			+ " left join sc.partyStoneRates psr on psr.stoneClarity = sc left join sc.saleStoneRates ssr on ssr.stoneClarity = sc "
			+ " where (psr.isDeleted = false or ssr.isDeleted = false) and sc.id in (?1)")
	public boolean stoneClarityHasOrphans(Set<String> stoneClarityIds);

	@Modifying
	@Transactional
	@Query(value = "update StoneClarity set isDeleted=true where id in (?1)")
	public void deleteStoneClarities(Set<String> stoneClarityIds);

}
