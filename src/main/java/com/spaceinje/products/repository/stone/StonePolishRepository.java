package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.StonePolish;

@Repository
public interface StonePolishRepository
		extends JpaRepository<StonePolish, String>, JpaSpecificationExecutor<StonePolish> {

	@Query(value = "select case when count(sp) > 0 then true else false end from StonePolish sp "
			+ " left join sp.partyStoneRates psr on psr.stonePolish = sp left join sp.saleStoneRates ssr on ssr.stonePolish = sp "
			+ " where (psr.isDeleted = false or ssr.isDeleted = false) and sp.id in (?1)")
	public boolean stonePolishHasOrphans(Set<String> stonePolishIds);

	@Modifying
	@Transactional
	@Query(value = "update StonePolish set isDeleted=true where id in (?1)")
	public void deleteStonePolishes(Set<String> stonePolishIds);

}
