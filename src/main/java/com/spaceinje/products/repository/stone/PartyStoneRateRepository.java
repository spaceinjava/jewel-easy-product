package com.spaceinje.products.repository.stone;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.stone.master.PartyStoneRate;

@Repository
public interface PartyStoneRateRepository extends JpaRepository<PartyStoneRate, String>, JpaSpecificationExecutor<PartyStoneRate> {

	@Modifying
	@Transactional
	@Query(value = "update PartyStoneRate set isDeleted=true where id in (?1)")
	public void deletePartyRates(Set<String> partyRateIds);

}
