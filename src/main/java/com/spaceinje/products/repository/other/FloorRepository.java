package com.spaceinje.products.repository.other;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.other.master.Floor;

@Repository
public interface FloorRepository extends JpaRepository<Floor, String>, JpaSpecificationExecutor<Floor> {

	@Query(value = "select case when count(f) > 0 then true else false end from Floor f left join f.counters c on c.floor = f "
			+ " where (c.isDeleted = false) and f.id in (?1)")
	public boolean floorHasOrphans(Set<String> floorIds);

	@Modifying
	@Transactional
	@Query(value = "update Floor set isDeleted=true where id in (?1)")
	public void deleteFloors(Set<String> floorIds);

}
