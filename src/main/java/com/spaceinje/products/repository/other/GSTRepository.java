package com.spaceinje.products.repository.other;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;


import com.spaceinje.products.model.other.master.GST;

@Repository
public interface GSTRepository extends JpaRepository<GST, String>, JpaSpecificationExecutor<GST> {


	@Modifying
	@Transactional
	@Query(value = "update GST set isDeleted=true where id in (?1)")
	public void deleteGSTs(Set<String> gstIDs);


}
