package com.spaceinje.products.repository.other;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.other.master.Purity;

@Repository
public interface PurityRepository extends JpaRepository<Purity, String>, JpaSpecificationExecutor<Purity> {

	@Query(value = "select case when count(p) > 0 then true else false end from Purity p left join p.partyWastages pw on pw.purity = p "
			+ " left join p.saleWastages sw on sw.purity = p left join p.rates r on r.purity = p "
			+ " left join p.productDetails pd on pd.purity = p "
			+ " where (pw.isDeleted = false or sw.isDeleted = false or r.isDeleted = false or pd.isDeleted = false) and p.id in (?1)")
	public boolean purityHasOrphans(Set<String> purityIds);

	@Modifying
	@Transactional
	@Query(value = "update Purity set isDeleted=true where id in (?1)")
	public void deletePurities(Set<String> purityIds);

}
