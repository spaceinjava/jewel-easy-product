package com.spaceinje.products.repository.other;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.other.master.HSNCode;

@Repository
public interface HSNCodeRepository extends JpaRepository<HSNCode, String>, JpaSpecificationExecutor<HSNCode> {

	
	@Modifying
	@Transactional
	@Query(value = "update HSNCode set isDeleted=true where id in (?1)")
	public void deleteHSNCodes(Set<String> hsnIds);

}
