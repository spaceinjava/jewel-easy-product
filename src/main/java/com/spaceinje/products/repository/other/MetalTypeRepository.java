package com.spaceinje.products.repository.other;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.products.model.master.MetalType;


@Repository
public interface MetalTypeRepository extends JpaRepository<MetalType, String>, JpaSpecificationExecutor<MetalType> {

}
