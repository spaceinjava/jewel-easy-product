package com.spaceinje.products.repository.other;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.other.master.Counter;

public interface CounterRepository extends JpaRepository<Counter, String>, JpaSpecificationExecutor<Counter>{
	
	@Modifying
	@Transactional
	@Query(value = "update Counter set isDeleted=true where id in (?1)")
	public void deleteCounters(Set<String> counterIds);

}
