package com.spaceinje.products.repository.other;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spaceinje.products.model.other.master.Rate;

@Repository
public interface RateRepository extends JpaRepository<Rate, String>, JpaSpecificationExecutor<Rate> {

	@Modifying
	@Transactional
	@Query(value = "update Rate set isDeleted=true where mainGroup.id in (?1)")
	public void deleteRatesByMainGroup(String mainGroup);

}
