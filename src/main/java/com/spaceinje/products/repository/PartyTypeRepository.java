package com.spaceinje.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.spaceinje.products.model.master.PartyType;

@Repository
public interface PartyTypeRepository extends JpaRepository<PartyType, String>, JpaSpecificationExecutor<PartyType> {

}
