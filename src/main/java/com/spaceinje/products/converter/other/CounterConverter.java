package com.spaceinje.products.converter.other;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.other.master.CounterBean;
import com.spaceinje.products.bean.other.master.FloorBean;
import com.spaceinje.products.model.other.master.Counter;
import com.spaceinje.products.model.other.master.Floor;

@Service
public class CounterConverter {

	ModelMapper mapper = new ModelMapper();

	public Counter beanToEntity(CounterBean bean) {
		Counter entity = mapper.map(bean, Counter.class);
		FloorBean floorBean = bean.getFloor();
		Floor floor = mapper.map(floorBean, Floor.class);
		entity.setFloor(floor);
		return entity;
	}

	public CounterBean entityToBean(Counter entity) {
		CounterBean bean = mapper.map(entity, CounterBean.class);
		Floor floor = entity.getFloor();
		FloorBean floorBean = mapper.map(floor, FloorBean.class);
		bean.setFloor(floorBean);
		return bean;
	}
}
