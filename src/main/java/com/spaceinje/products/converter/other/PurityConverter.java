package com.spaceinje.products.converter.other;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.model.master.MainGroup;
import com.spaceinje.products.model.other.master.Purity;

@Service
public class PurityConverter {

	ModelMapper mapper = new ModelMapper();

	public Purity beanToEntity(PurityBean bean) {
		Purity entity = mapper.map(bean, Purity.class);
		MainGroupBean mainGroupBean = bean.getMainGroup();
		MainGroup mainGroup = mapper.map(mainGroupBean, MainGroup.class);
		entity.setMainGroup(mainGroup);
		return entity;
	}

	public PurityBean entityToBean(Purity entity) {
		PurityBean bean = mapper.map(entity, PurityBean.class);
		MainGroup mainGroup = entity.getMainGroup();
		MainGroupBean mainGroupBean = mapper.map(mainGroup, MainGroupBean.class);
		bean.setMainGroup(mainGroupBean);
		return bean;
	}
}
