package com.spaceinje.products.converter.other;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.other.master.RateBean;
import com.spaceinje.products.model.master.MainGroup;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.other.master.Rate;

@Service
public class RateConverter {
	ModelMapper mapper = new ModelMapper();

	public Rate beanToEntity(RateBean bean) {
		Rate entity = mapper.map(bean, Rate.class);
		MainGroupBean mainGroupBean = (bean).getMainGroup();
		PurityBean purityBean = (bean).getPurity();
		MainGroup mainGroup = mapper.map(mainGroupBean, MainGroup.class);
		Purity purity = mapper.map(purityBean, Purity.class);
		entity.setMainGroup(mainGroup);
		entity.setPurity(purity);
		return entity;
	}

	public RateBean entityToBean(Rate entity) {
		RateBean bean = mapper.map(entity, RateBean.class);
		MainGroup mainGroup = entity.getMainGroup();
		Purity purity = entity.getPurity();
		MainGroupBean mainGroupBean = mapper.map(mainGroup, MainGroupBean.class);
		PurityBean purityBean = mapper.map(purity, PurityBean.class);
		bean.setMainGroup(mainGroupBean);
		bean.setPurity(purityBean);
		return bean;
	}

	public List<Rate> beansToEntities(List<RateBean> beans) {
		return beans.stream().map(bean -> {
			Rate entity = mapper.map(bean, Rate.class);
			MainGroupBean mainGroupBean = (bean).getMainGroup();
			PurityBean purityBean = (bean).getPurity();
			MainGroup mainGroup = mapper.map(mainGroupBean, MainGroup.class);
			Purity purity = mapper.map(purityBean, Purity.class);
			entity.setMainGroup(mainGroup);
			entity.setPurity(purity);
			return entity;
		}).collect(Collectors.toList());
	}

	public List<RateBean> entitiesToBeans(List<Rate> entities) {
		return entities.stream().map(entity -> {
			RateBean bean = mapper.map(entity, RateBean.class);
			MainGroup mainGroup = entity.getMainGroup();
			Purity purity = entity.getPurity();
			MainGroupBean mainGroupBean = mapper.map(mainGroup, MainGroupBean.class);
			PurityBean purityBean = mapper.map(purity, PurityBean.class);
			bean.setMainGroup(mainGroupBean);
			bean.setPurity(purityBean);
			return bean;
		}).collect(Collectors.toList());
	}

}
