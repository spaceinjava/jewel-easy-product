package com.spaceinje.products.converter.stone;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.bean.stone.master.StoneTypeBean;
import com.spaceinje.products.model.stone.master.StoneGroup;
import com.spaceinje.products.model.stone.master.StoneType;

@Service
public class StoneGroupConverter {

	ModelMapper mapper = new ModelMapper();

	public StoneGroup beanToEntity(StoneGroupBean bean) {
		StoneGroup entity = mapper.map(bean, StoneGroup.class);
		StoneTypeBean stoneTypeBean = bean.getStoneType();
		StoneType stoneType = mapper.map(stoneTypeBean, StoneType.class);
		entity.setStoneType(stoneType);
		return entity;
	}

	public StoneGroupBean entityToBean(StoneGroup entity) {
		StoneGroupBean bean = mapper.map(entity, StoneGroupBean.class);
		StoneType stoneType = entity.getStoneType();
		StoneTypeBean stoneTypeBean = mapper.map(stoneType, StoneTypeBean.class);
		bean.setStoneType(stoneTypeBean);
		return bean;
	}
}
