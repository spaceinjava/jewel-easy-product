package com.spaceinje.products.converter.stone;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.stone.master.StoneBean;
import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.model.stone.master.Stone;
import com.spaceinje.products.model.stone.master.StoneGroup;

@Service
public class StoneConverter {

	ModelMapper mapper = new ModelMapper();

	public Stone beanToEntity(StoneBean bean) {
		Stone entity = mapper.map(bean, Stone.class);
		StoneGroupBean stoneGroupBean = bean.getStoneGroup();
		StoneGroup stoneGroup = mapper.map(stoneGroupBean, StoneGroup.class);
		entity.setStoneGroup(stoneGroup);
		return entity;
	}

	public StoneBean entityToBean(Stone entity) {
		StoneBean bean = mapper.map(entity, StoneBean.class);
		StoneGroup stoneGroup = entity.getStoneGroup();
		StoneGroupBean stoneGroupBean = mapper.map(stoneGroup, StoneGroupBean.class);
		bean.setStoneGroup(stoneGroupBean);
		return bean;
	}
}
