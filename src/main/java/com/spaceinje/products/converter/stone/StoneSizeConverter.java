package com.spaceinje.products.converter.stone;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.stone.master.StoneGroupBean;
import com.spaceinje.products.bean.stone.master.StoneSizeBean;
import com.spaceinje.products.model.stone.master.StoneGroup;
import com.spaceinje.products.model.stone.master.StoneSize;

@Service
public class StoneSizeConverter {

	ModelMapper mapper = new ModelMapper();

	public StoneSize beanToEntity(StoneSizeBean bean) {
		StoneSize entity = mapper.map(bean, StoneSize.class);
		StoneGroupBean stoneGroupBean = bean.getStoneGroup();
		StoneGroup stoneGroup = mapper.map(stoneGroupBean, StoneGroup.class);
		entity.setStoneGroup(stoneGroup);
		return entity;
	}

	public StoneSizeBean entityToBean(StoneSize entity) {
		StoneSizeBean bean = mapper.map(entity, StoneSizeBean.class);
		StoneGroup stoneGroup = entity.getStoneGroup();
		StoneGroupBean stoneGroupBean = mapper.map(stoneGroup, StoneGroupBean.class);
		bean.setStoneGroup(stoneGroupBean);
		return bean;
	}

}
