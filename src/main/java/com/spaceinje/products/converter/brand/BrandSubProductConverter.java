package com.spaceinje.products.converter.brand;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.bean.brand.master.BrandSubProductBean;
import com.spaceinje.products.model.brand.master.BrandProduct;
import com.spaceinje.products.model.brand.master.BrandSubProduct;

@Service
public class BrandSubProductConverter {

	ModelMapper mapper = new ModelMapper();

	public BrandSubProduct beanToEntity(BrandSubProductBean bean) {
		BrandSubProduct brandSubProduct = mapper.map(bean, BrandSubProduct.class);
		BrandProductBean brandProductBean = bean.getBrandProduct();
		BrandProduct brandProduct = mapper.map(brandProductBean, BrandProduct.class);
		brandSubProduct.setBrandProduct(brandProduct);
		return brandSubProduct;
	}

	public BrandSubProductBean entityToBean(BrandSubProduct entity) {
		BrandSubProductBean brandSubProductBean = mapper.map(entity, BrandSubProductBean.class);
		BrandProduct brandProduct = entity.getBrandProduct();
		BrandProductBean brandProductBean = mapper.map(brandProduct, BrandProductBean.class);
		brandSubProductBean.setBrandProduct(brandProductBean);
		return brandSubProductBean;
	}

}
