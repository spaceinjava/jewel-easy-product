package com.spaceinje.products.converter.brand;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.model.brand.master.Brand;
import com.spaceinje.products.model.master.MainGroup;

@Service
public class BrandConverter {

	ModelMapper mapper = new ModelMapper();

	public Brand beanToEntity(BrandBean bean) {
		Brand brand = mapper.map(bean, Brand.class);
		MainGroupBean mainGroupBean = bean.getMainGroup();
		MainGroup mainGroup = mapper.map(mainGroupBean, MainGroup.class);
		brand.setMainGroup(mainGroup);
		return brand;
	}

	public BrandBean entityToBean(Brand entity) {
		BrandBean brandBean = mapper.map(entity, BrandBean.class);
		MainGroup mainGroup = entity.getMainGroup();
		MainGroupBean mainGroupBean = mapper.map(mainGroup, MainGroupBean.class);
		brandBean.setMainGroup(mainGroupBean);
		return brandBean;
	}
}
