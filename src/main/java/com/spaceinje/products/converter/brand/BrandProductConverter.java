package com.spaceinje.products.converter.brand;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.brand.master.BrandBean;
import com.spaceinje.products.bean.brand.master.BrandProductBean;
import com.spaceinje.products.model.brand.master.Brand;
import com.spaceinje.products.model.brand.master.BrandProduct;

@Service
public class BrandProductConverter {

	ModelMapper mapper = new ModelMapper();

	public BrandProduct beanToEntity(BrandProductBean bean) {
		BrandProduct brandProduct = mapper.map(bean, BrandProduct.class);
		BrandBean brandBean = bean.getBrand();
		Brand brand = mapper.map(brandBean, Brand.class);
		brandProduct.setBrand(brand);
		return brandProduct;
	}

	public BrandProductBean entityToBean(BrandProduct entity) {
		BrandProductBean brandProductBean = mapper.map(entity, BrandProductBean.class);
		Brand brand = entity.getBrand();
		BrandBean brandBean = mapper.map(brand, BrandBean.class);
		brandProductBean.setBrand(brandBean);
		return brandProductBean;
	}
	
}
