package com.spaceinje.products.converter.product;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.other.master.PurityBean;
import com.spaceinje.products.bean.product.master.PartyWastageBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.product.master.PartyWastage;
import com.spaceinje.products.model.product.master.ProductSize;
import com.spaceinje.products.model.product.master.SubProduct;

@Service
public class PartyWastageConverter {

	ModelMapper mapper = new ModelMapper();

	public PartyWastage beanToEntity(PartyWastageBean bean) {
		PartyWastage entity = mapper.map(bean, PartyWastage.class);
		SubProductBean subProductBean = bean.getSubProduct();
		SubProduct subProduct = mapper.map(subProductBean, SubProduct.class);
		entity.setSubProduct(subProduct);
		ProductSizeBean productSizeBean = bean.getProductSize();
		ProductSize productSize = mapper.map(productSizeBean, ProductSize.class);
		entity.setProductSize(productSize);
		PurityBean purityBean = bean.getPurity();
		Purity purity = mapper.map(purityBean, Purity.class);
		entity.setPurity(purity);
		return entity;
	}

	public PartyWastageBean entityToBean(PartyWastage entity) {
		PartyWastageBean bean = mapper.map(entity, PartyWastageBean.class);
		SubProduct subProduct = entity.getSubProduct();
		SubProductBean subProductBean = mapper.map(subProduct, SubProductBean.class);
		bean.setSubProduct(subProductBean);
		ProductSize productSize = entity.getProductSize();
		ProductSizeBean productSizeBean = mapper.map(productSize, ProductSizeBean.class);
		bean.setProductSize(productSizeBean);
		Purity purity = entity.getPurity();
		PurityBean purityBean = mapper.map(purity, PurityBean.class);
		bean.setPurity(purityBean);
		return bean;
	}
}
