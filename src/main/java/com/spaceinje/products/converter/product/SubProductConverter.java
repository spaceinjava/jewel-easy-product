package com.spaceinje.products.converter.product;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.SubProductBean;
import com.spaceinje.products.model.product.master.Product;
import com.spaceinje.products.model.product.master.SubProduct;

@Service
public class SubProductConverter {

	ModelMapper mapper = new ModelMapper();

	public SubProduct beanToEntity(SubProductBean bean) {
		SubProduct entity = mapper.map(bean, SubProduct.class);
		ProductBean productBean = bean.getProduct();
		Product product = mapper.map(productBean, Product.class);
		entity.setProduct(product);
		return entity;
	}

	public SubProductBean entityToBean(SubProduct entity) {
		SubProductBean bean = mapper.map(entity, SubProductBean.class);
		Product product = entity.getProduct();
		ProductBean productBean = mapper.map(product, ProductBean.class);
		bean.setProduct(productBean);
		return bean;
	}
}
