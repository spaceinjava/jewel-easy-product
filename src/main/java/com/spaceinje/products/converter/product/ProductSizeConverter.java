package com.spaceinje.products.converter.product;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.bean.product.master.ProductSizeBean;
import com.spaceinje.products.model.product.master.Product;
import com.spaceinje.products.model.product.master.ProductSize;

@Service
public class ProductSizeConverter {

	ModelMapper mapper = new ModelMapper();

	public ProductSize beanToEntity(ProductSizeBean bean) {
		ProductSize entity = mapper.map(bean, ProductSize.class);
		ProductBean productBean = bean.getProduct();
		Product product = mapper.map(productBean, Product.class);
		entity.setProduct(product);
		return entity;
	}

	public ProductSizeBean entityToBean(ProductSize entity) {
		ProductSizeBean bean = mapper.map(entity, ProductSizeBean.class);
		Product product = entity.getProduct();
		ProductBean productBean = mapper.map(product, ProductBean.class);
		bean.setProduct(productBean);
		return bean;
	}
}
