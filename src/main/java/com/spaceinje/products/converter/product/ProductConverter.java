package com.spaceinje.products.converter.product;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.spaceinje.products.bean.master.MainGroupBean;
import com.spaceinje.products.bean.product.master.ProductBean;
import com.spaceinje.products.model.master.MainGroup;
import com.spaceinje.products.model.product.master.Product;

@Service
public class ProductConverter {

	ModelMapper mapper = new ModelMapper();

	public Product beanToEntity(ProductBean bean) {
		Product entity = mapper.map(bean, Product.class);
		MainGroupBean mainGroupBean = bean.getMainGroup();
		MainGroup mainGroup = mapper.map(mainGroupBean, MainGroup.class);
		entity.setMainGroup(mainGroup);
		return entity;
	}

	public ProductBean entityToBean(Product entity) {
		ProductBean bean = mapper.map(entity, ProductBean.class);
		MainGroup mainGroup = entity.getMainGroup();
		MainGroupBean mainGroupBean = mapper.map(mainGroup, MainGroupBean.class);
		bean.setMainGroup(mainGroupBean);
		return bean;
	}
}
