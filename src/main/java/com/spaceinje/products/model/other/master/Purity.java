package com.spaceinje.products.model.other.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.inventory.ProductDetails;
import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MainGroup;
import com.spaceinje.products.model.product.master.PartyWastage;
import com.spaceinje.products.model.product.master.SaleWastage;

@Entity
public class Purity extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 5642558781691940513L;

	private double purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "mainGroupId", nullable = false)
	private MainGroup mainGroup;

	@OneToMany(mappedBy = "purity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PartyWastage> partyWastages;

	@OneToMany(mappedBy = "purity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SaleWastage> saleWastages;

	@OneToMany(mappedBy = "purity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Rate> rates;

	@OneToMany(mappedBy = "purity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ProductDetails> productDetails;

	public double getPurity() {
		return purity;
	}

	public void setPurity(double purity) {
		this.purity = purity;
	}

	public MainGroup getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroup mainGroup) {
		this.mainGroup = mainGroup;
	}

	public Set<PartyWastage> getPartyWastages() {
		return partyWastages;
	}

	public void setPartyWastages(Set<PartyWastage> partyWastages) {
		this.partyWastages = partyWastages;
	}

	public Set<SaleWastage> getSaleWastages() {
		return saleWastages;
	}

	public void setSaleWastages(Set<SaleWastage> saleWastages) {
		this.saleWastages = saleWastages;
	}

	public Set<Rate> getRates() {
		return rates;
	}

	public void setRates(Set<Rate> rates) {
		this.rates = rates;
	}

	public Set<ProductDetails> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(Set<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}

	@Override
	public String toString() {
		return "Purity [purity=" + purity + ", mainGroup=" + mainGroup + ", partyWastages=" + partyWastages
				+ ", saleWastages=" + saleWastages + ", rates=" + rates + ", productDetails=" + productDetails
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode()
				+ "]";
	}

}
