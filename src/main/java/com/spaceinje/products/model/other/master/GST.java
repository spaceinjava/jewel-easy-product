package com.spaceinje.products.model.other.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MetalType;

@Entity
public class GST extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -8101257394463112142L;

	private double percent;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "metalTypeId", nullable = false)
	private MetalType metalType;

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public MetalType getMetalType() {
		return metalType;
	}

	public void setMetalType(MetalType metalType) {
		this.metalType = metalType;
	}

	@Override
	public String toString() {
		return "GST [percent=" + percent + ", metalType=" + metalType + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
