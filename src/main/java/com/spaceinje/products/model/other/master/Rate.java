package com.spaceinje.products.model.other.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MainGroup;

@Entity
public class Rate extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 3790618306083741609L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "mainGroupId", nullable = false)
	private MainGroup mainGroup;

	private double perGramRate;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	private double purityRate;

	private boolean display;

	public MainGroup getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroup mainGroup) {
		this.mainGroup = mainGroup;
	}

	public double getPerGramRate() {
		return perGramRate;
	}

	public void setPerGramRate(double perGramRate) {
		this.perGramRate = perGramRate;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public double getPurityRate() {
		return purityRate;
	}

	public void setPurityRate(double purityRate) {
		this.purityRate = purityRate;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	@Override
	public String toString() {
		return "Rate [mainGroup=" + mainGroup + ", perGramRate=" + perGramRate + ", purity=" + purity + ", purityRate="
				+ purityRate + ", display=" + display + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
