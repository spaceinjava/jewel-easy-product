package com.spaceinje.products.model.other.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MainGroup;

@Entity
public class HSNCode extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 4146858830648998305L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "mainGroupId", nullable = false)
	private MainGroup mainGroup;

	private String hsnCode;

	private String description;

	public MainGroup getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroup mainGroup) {
		this.mainGroup = mainGroup;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "HSNCode [mainGroup=" + mainGroup + ", hsnCode=" + hsnCode + ", description=" + description
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode()
				+ "]";
	}

}
