package com.spaceinje.products.model.other.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class Counter extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 8206627172658005401L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "floorId", nullable = false)
	private Floor floor;

	private String shortCode;

	private String counterName;

	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCounterName() {
		return counterName;
	}

	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}

	@Override
	public String toString() {
		return "Counter [floor=" + floor + ", shortCode=" + shortCode + ", counterName=" + counterName + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
