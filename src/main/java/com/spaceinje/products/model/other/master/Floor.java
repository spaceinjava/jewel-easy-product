package com.spaceinje.products.model.other.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class Floor extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -3858275328276661896L;

	private String floorName;

	private String shortCode;

	@OneToMany(mappedBy = "floor", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Counter> counters;

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Set<Counter> getCounters() {
		return counters;
	}

	public void setCounters(Set<Counter> counters) {
		this.counters = counters;
	}

	@Override
	public String toString() {
		return "Floor [floorName=" + floorName + ", shortCode=" + shortCode + ", counters=" + counters + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
