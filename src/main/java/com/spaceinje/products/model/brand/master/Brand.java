package com.spaceinje.products.model.brand.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MainGroup;

@Entity
public class Brand extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 345227014582428180L;

	private String shortCode;

	private String brandName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "mainGroupId", nullable = false)
	private MainGroup mainGroup;

	@OneToMany(mappedBy = "brand", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BrandProduct> brandProducts;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public MainGroup getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroup mainGroup) {
		this.mainGroup = mainGroup;
	}

	@Override
	public String toString() {
		return "Brand [shortCode=" + shortCode + ", brandName=" + brandName + ", mainGroup=" + mainGroup
				+ ", brandProducts=" + brandProducts + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
