package com.spaceinje.products.model.brand.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class BrandProduct extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -922493395968909333L;

	private String shortCode;

	private String productName;

	private double gst;

	private boolean tray;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "brandId", nullable = false)
	private Brand brand;

	@OneToMany(mappedBy = "brandProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BrandSubProduct> brandSubProducts;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getGst() {
		return gst;
	}

	public void setGst(double gst) {
		this.gst = gst;
	}

	public boolean isTray() {
		return tray;
	}

	public void setTray(boolean tray) {
		this.tray = tray;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "BrandProduct [shortCode=" + shortCode + ", productName=" + productName + ", gst=" + gst + ", tray="
				+ tray + ", brand=" + brand + ", brandSubProducts=" + brandSubProducts + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
