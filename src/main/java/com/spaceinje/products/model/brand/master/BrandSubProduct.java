package com.spaceinje.products.model.brand.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.lot.BrandedLot;
import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class BrandSubProduct extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -8461263831087531200L;

	private String shortCode;

	private String subProductName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "brandProductId", nullable = false)
	private BrandProduct brandProduct;

	@OneToMany(mappedBy = "brandSubProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BrandedLot> brandedLot;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSubProductName() {
		return subProductName;
	}

	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}

	public BrandProduct getBrandProduct() {
		return brandProduct;
	}

	public void setBrandProduct(BrandProduct brandProduct) {
		this.brandProduct = brandProduct;
	}

	public Set<BrandedLot> getBrandedLot() {
		return brandedLot;
	}

	public void setBrandedLot(Set<BrandedLot> brandedLot) {
		this.brandedLot = brandedLot;
	}

	@Override
	public String toString() {
		return "BrandSubProduct [shortCode=" + shortCode + ", subProductName=" + subProductName + ", brandProduct="
				+ brandProduct + ", brandedLot=" + brandedLot + "]";
	}

}
