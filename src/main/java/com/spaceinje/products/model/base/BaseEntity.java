package com.spaceinje.products.model.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

//@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable{

	private static final long serialVersionUID = -7522317522867593007L;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "base_id", strategy = "com.spaceinje.products.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String id;
	
	private String tenantCode;
	
	@Column(columnDefinition = "boolean default false", nullable = false)
	private Boolean isDeleted;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	

}
