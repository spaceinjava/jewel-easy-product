package com.spaceinje.products.model.product.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.other.master.Purity;

@Entity
public class PartyWastage extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 7517949757854633207L;

	private String partyId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private SubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productSizeId", nullable = false)
	private ProductSize productSize;

	private double touchValue;

	private double touchPercent;

	private double mcPerGram;

	private double mcPerDir;

	private double wastagePerGram;

	private double wastagePerDir;

//	private String mcOn;
//
//	private String wastageOn;

	public SubProduct getSubProduct() {
		return subProduct;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public double getTouchValue() {
		return touchValue;
	}

	public void setTouchValue(double touchValue) {
		this.touchValue = touchValue;
	}

	public double getTouchPercent() {
		return touchPercent;
	}

	public void setTouchPercent(double touchPercent) {
		this.touchPercent = touchPercent;
	}

	public double getMcPerGram() {
		return mcPerGram;
	}

	public void setMcPerGram(double mcPerGram) {
		this.mcPerGram = mcPerGram;
	}

	public double getMcPerDir() {
		return mcPerDir;
	}

	public void setMcPerDir(double mcPerDir) {
		this.mcPerDir = mcPerDir;
	}

	public double getWastagePerGram() {
		return wastagePerGram;
	}

	public void setWastagePerGram(double wastagePerGram) {
		this.wastagePerGram = wastagePerGram;
	}

	public double getWastagePerDir() {
		return wastagePerDir;
	}

	public void setWastagePerDir(double wastagePerDir) {
		this.wastagePerDir = wastagePerDir;
	}

//	public String getMcOn() {
//		return mcOn;
//	}
//
//	public void setMcOn(String mcOn) {
//		this.mcOn = mcOn;
//	}
//
//	public String getWastageOn() {
//		return wastageOn;
//	}
//
//	public void setWastageOn(String wastageOn) {
//		this.wastageOn = wastageOn;
//	}

	@Override
	public String toString() {
		return "PartyWastage [partyId=" + partyId + ", subProduct=" + subProduct + ", purity=" + purity
				+ ", productSize=" + productSize + ", touchValue=" + touchValue + ", touchPercent=" + touchPercent
				+ ", mcPerGram=" + mcPerGram + ", mcPerDir=" + mcPerDir + ", wastagePerGram=" + wastagePerGram
				+ ", wastagePerDir=" + wastagePerDir + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
