package com.spaceinje.products.model.product.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.other.master.Purity;

@Entity
public class SaleWastage extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 3067550170782985086L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "subProductId", nullable = false)
	private SubProduct subProduct;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "purityId", nullable = false)
	private Purity purity;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productSizeId", nullable = false)
	private ProductSize productSize;

	private double fromWeight;

	private double toWeight;

	private double maxWstPerGram;

	private double minWstPerGram;

	private double maxDirWst;

	private double minDirWst;

	private double maxMcPerGram;

	private double minMcPerGram;

	private double minDirMc;

	private double maxDirMc;

	private String mcOn;

	private String wastageOn;

	private boolean incWstMc;

	public SubProduct getSubProduct() {
		return subProduct;
	}

	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	public Purity getPurity() {
		return purity;
	}

	public void setPurity(Purity purity) {
		this.purity = purity;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public double getFromWeight() {
		return fromWeight;
	}

	public void setFromWeight(double fromWeight) {
		this.fromWeight = fromWeight;
	}

	public double getToWeight() {
		return toWeight;
	}

	public void setToWeight(double toWeight) {
		this.toWeight = toWeight;
	}

	public double getMaxWstPerGram() {
		return maxWstPerGram;
	}

	public void setMaxWstPerGram(double maxWstPerGram) {
		this.maxWstPerGram = maxWstPerGram;
	}

	public double getMinWstPerGram() {
		return minWstPerGram;
	}

	public void setMinWstPerGram(double minWstPerGram) {
		this.minWstPerGram = minWstPerGram;
	}

	public double getMaxDirWst() {
		return maxDirWst;
	}

	public void setMaxDirWst(double maxDirWst) {
		this.maxDirWst = maxDirWst;
	}

	public double getMinDirWst() {
		return minDirWst;
	}

	public void setMinDirWst(double minDirWst) {
		this.minDirWst = minDirWst;
	}

	public double getMaxMcPerGram() {
		return maxMcPerGram;
	}

	public void setMaxMcPerGram(double maxMcPerGram) {
		this.maxMcPerGram = maxMcPerGram;
	}

	public double getMinMcPerGram() {
		return minMcPerGram;
	}

	public void setMinMcPerGram(double minMcPerGram) {
		this.minMcPerGram = minMcPerGram;
	}

	public double getMinDirMc() {
		return minDirMc;
	}

	public void setMinDirMc(double minDirMc) {
		this.minDirMc = minDirMc;
	}

	public double getMaxDirMc() {
		return maxDirMc;
	}

	public void setMaxDirMc(double maxDirMc) {
		this.maxDirMc = maxDirMc;
	}

	public String getMcOn() {
		return mcOn;
	}

	public void setMcOn(String mcOn) {
		this.mcOn = mcOn;
	}

	public String getWastageOn() {
		return wastageOn;
	}

	public void setWastageOn(String wastageOn) {
		this.wastageOn = wastageOn;
	}

	public boolean isIncWstMc() {
		return incWstMc;
	}

	public void setIncWstMc(boolean incWstMc) {
		this.incWstMc = incWstMc;
	}

	@Override
	public String toString() {
		return "SaleWastage [subProduct=" + subProduct + ", purity=" + purity + ", productSize=" + productSize
				+ ", fromWeight=" + fromWeight + ", toWeight=" + toWeight + ", maxWstPerGram=" + maxWstPerGram
				+ ", minWstPerGram=" + minWstPerGram + ", maxDirWst=" + maxDirWst + ", minDirWst=" + minDirWst
				+ ", maxMcPerGram=" + maxMcPerGram + ", minMcPerGram=" + minMcPerGram + ", minDirMc=" + minDirMc
				+ ", maxDirMc=" + maxDirMc + ", mcOn=" + mcOn + ", wastageOn=" + wastageOn + ", incWstMc=" + incWstMc
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode()
				+ "]";
	}

}
