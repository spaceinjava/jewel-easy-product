package com.spaceinje.products.model.product.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.inventory.ProductDetails;
import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class ProductSize extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -6695877723345576701L;

	private double productSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	@OneToMany(mappedBy = "productSize", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PartyWastage> partyWastages;

	@OneToMany(mappedBy = "productSize", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SaleWastage> saleWastages;

	@OneToMany(mappedBy = "productSize", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ProductDetails> productDetails;

	public double getProductSize() {
		return productSize;
	}

	public void setProductSize(double productSize) {
		this.productSize = productSize;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Set<PartyWastage> getPartyWastages() {
		return partyWastages;
	}

	public void setPartyWastages(Set<PartyWastage> partyWastages) {
		this.partyWastages = partyWastages;
	}

	public Set<SaleWastage> getSaleWastages() {
		return saleWastages;
	}

	public void setSaleWastages(Set<SaleWastage> saleWastages) {
		this.saleWastages = saleWastages;
	}

	public Set<ProductDetails> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(Set<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}

	@Override
	public String toString() {
		return "ProductSize [productSize=" + productSize + ", product=" + product + ", partyWastages=" + partyWastages
				+ ", saleWastages=" + saleWastages + ", productDetails=" + productDetails + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
