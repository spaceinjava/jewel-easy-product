package com.spaceinje.products.model.product.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.inventory.BrandedDetails;
import com.spaceinje.journal.model.inventory.ProductDetails;
import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class SubProduct extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 2677129365702844L;

	private String shortCode;

	private String subProductName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	@OneToMany(mappedBy = "subProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PartyWastage> partyWastages;

	@OneToMany(mappedBy = "subProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SaleWastage> saleWastages;

	@OneToMany(mappedBy = "subProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BrandedDetails> brandedDetails;

	@OneToMany(mappedBy = "subProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ProductDetails> productDetails;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSubProductName() {
		return subProductName;
	}

	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Set<PartyWastage> getPartyWastages() {
		return partyWastages;
	}

	public void setPartyWastages(Set<PartyWastage> partyWastages) {
		this.partyWastages = partyWastages;
	}

	public Set<SaleWastage> getSaleWastages() {
		return saleWastages;
	}

	public void setSaleWastages(Set<SaleWastage> saleWastages) {
		this.saleWastages = saleWastages;
	}

	public Set<BrandedDetails> getBrandedDetails() {
		return brandedDetails;
	}

	public void setBrandedDetails(Set<BrandedDetails> brandedDetails) {
		this.brandedDetails = brandedDetails;
	}

	public Set<ProductDetails> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(Set<ProductDetails> productDetails) {
		this.productDetails = productDetails;
	}

	@Override
	public String toString() {
		return "SubProduct [shortCode=" + shortCode + ", subProductName=" + subProductName + ", product=" + product
				+ ", partyWastages=" + partyWastages + ", saleWastages=" + saleWastages + ", brandedDetails="
				+ brandedDetails + ", productDetails=" + productDetails + ", getId()=" + getId() + ", getIsDeleted()="
				+ getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
