package com.spaceinje.products.model.product.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.master.MainGroup;

@Entity
public class Product extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -1065609499934698737L;

	private String shortCode;

	private String productName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "mainGroupId", nullable = false)
	private MainGroup mainGroup;

	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SubProduct> subProducts;

	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ProductSize> productSizes;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public MainGroup getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(MainGroup mainGroup) {
		this.mainGroup = mainGroup;
	}

	public Set<SubProduct> getSubProducts() {
		return subProducts;
	}

	public void setSubProducts(Set<SubProduct> subProducts) {
		this.subProducts = subProducts;
	}

	public Set<ProductSize> getProductSizes() {
		return productSizes;
	}

	public void setProductSizes(Set<ProductSize> productSizes) {
		this.productSizes = productSizes;
	}

	@Override
	public String toString() {
		return "Product [shortCode=" + shortCode + ", productName=" + productName + ", mainGroup=" + mainGroup
				+ ", subProducts=" + subProducts + ", productSizes=" + productSizes + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
