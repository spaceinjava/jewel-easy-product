package com.spaceinje.products.model.stone.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class PartyStoneRate extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 2217892060961807987L;

	private String partyId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	private double minRate;

	private double maxRate;

	private String uom;

	private boolean noWeight;

	private boolean fixedRate;

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public double getMinRate() {
		return minRate;
	}

	public void setMinRate(double minRate) {
		this.minRate = minRate;
	}

	public double getMaxRate() {
		return maxRate;
	}

	public void setMaxRate(double maxRate) {
		this.maxRate = maxRate;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public boolean isNoWeight() {
		return noWeight;
	}

	public void setNoWeight(boolean noWeight) {
		this.noWeight = noWeight;
	}

	public boolean isFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(boolean fixedRate) {
		this.fixedRate = fixedRate;
	}

	@Override
	public String toString() {
		return "PartyStoneRate [partyId=" + partyId + ", stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor="
				+ stoneColor + ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", minRate="
				+ minRate + ", maxRate=" + maxRate + ", uom=" + uom + ", noWeight=" + noWeight + ", fixedRate="
				+ fixedRate + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()="
				+ getTenantCode() + "]";
	}

}
