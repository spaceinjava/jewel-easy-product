package com.spaceinje.products.model.stone.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.inventory.StoneDetails;
import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class Stone extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -954575228443112820L;

	private String shortCode;

	private String stoneName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneGroupId", nullable = false)
	private StoneGroup stoneGroup;

	@OneToMany(mappedBy = "stone", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PartyStoneRate> partyStoneRates;

	@OneToMany(mappedBy = "stone", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SaleStoneRate> saleStoneRates;

	@OneToMany(mappedBy = "stone", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneDetails> stoneDetails;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getStoneName() {
		return stoneName;
	}

	public void setStoneName(String stoneName) {
		this.stoneName = stoneName;
	}

	public StoneGroup getStoneGroup() {
		return stoneGroup;
	}

	public void setStoneGroup(StoneGroup stoneGroup) {
		this.stoneGroup = stoneGroup;
	}

	public Set<PartyStoneRate> getPartyStoneRates() {
		return partyStoneRates;
	}

	public void setPartyStoneRates(Set<PartyStoneRate> partyStoneRates) {
		this.partyStoneRates = partyStoneRates;
	}

	public Set<SaleStoneRate> getSaleStoneRates() {
		return saleStoneRates;
	}

	public void setSaleStoneRates(Set<SaleStoneRate> saleStoneRates) {
		this.saleStoneRates = saleStoneRates;
	}

	public Set<StoneDetails> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<StoneDetails> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	@Override
	public String toString() {
		return "Stone [shortCode=" + shortCode + ", stoneName=" + stoneName + ", stoneGroup=" + stoneGroup
				+ ", partyStoneRates=" + partyStoneRates + ", saleStoneRates=" + saleStoneRates + ", stoneDetails="
				+ stoneDetails + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()="
				+ getTenantCode() + "]";
	}

}
