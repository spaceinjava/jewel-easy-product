package com.spaceinje.products.model.stone.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class StoneGroup extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -6048179334444311260L;

	private String shortCode;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneTypeId", nullable = false)
	private StoneType stoneType;

	private String groupName;

	private boolean diamond;

	@OneToMany(mappedBy = "stoneGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneSize> stoneSizes;

	@OneToMany(mappedBy = "stoneGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Stone> stones;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public StoneType getStoneType() {
		return stoneType;
	}

	public void setStoneType(StoneType stoneType) {
		this.stoneType = stoneType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isDiamond() {
		return diamond;
	}

	public void setDiamond(boolean diamond) {
		this.diamond = diamond;
	}

	public Set<StoneSize> getStoneSizes() {
		return stoneSizes;
	}

	public void setStoneSizes(Set<StoneSize> stoneSizes) {
		this.stoneSizes = stoneSizes;
	}

	public Set<Stone> getStones() {
		return stones;
	}

	public void setStones(Set<Stone> stones) {
		this.stones = stones;
	}

	@Override
	public String toString() {
		return "StoneGroup [shortCode=" + shortCode + ", stoneType=" + stoneType + ", groupName=" + groupName
				+ ", diamond=" + diamond + ", stoneSizes=" + stoneSizes + ", stones=" + stones + ", getId()=" + getId()
				+ ", getIsDeleted()=" + getIsDeleted() + "]";
	}

}
