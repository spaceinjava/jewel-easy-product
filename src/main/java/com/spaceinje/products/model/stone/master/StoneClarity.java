package com.spaceinje.products.model.stone.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.spaceinje.journal.model.inventory.StoneDetails;
import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class StoneClarity extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -4067253988400782196L;

	private String shortCode;

	private String clarityName;

	@OneToMany(mappedBy = "stoneClarity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PartyStoneRate> partyStoneRates;

	@OneToMany(mappedBy = "stoneClarity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SaleStoneRate> saleStoneRates;

	@OneToMany(mappedBy = "stoneClarity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneDetails> stoneDetails;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getClarityName() {
		return clarityName;
	}

	public void setClarityName(String clarityName) {
		this.clarityName = clarityName;
	}

	public Set<PartyStoneRate> getPartyStoneRates() {
		return partyStoneRates;
	}

	public void setPartyStoneRates(Set<PartyStoneRate> partyStoneRates) {
		this.partyStoneRates = partyStoneRates;
	}

	public Set<SaleStoneRate> getSaleStoneRates() {
		return saleStoneRates;
	}

	public void setSaleStoneRates(Set<SaleStoneRate> saleStoneRates) {
		this.saleStoneRates = saleStoneRates;
	}

	public Set<StoneDetails> getStoneDetails() {
		return stoneDetails;
	}

	public void setStoneDetails(Set<StoneDetails> stoneDetails) {
		this.stoneDetails = stoneDetails;
	}

	@Override
	public String toString() {
		return "StoneClarity [shortCode=" + shortCode + ", clarityName=" + clarityName + ", partyStoneRates="
				+ partyStoneRates + ", saleStoneRates=" + saleStoneRates + ", stoneDetails=" + stoneDetails
				+ ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode()
				+ "]";
	}

}
