package com.spaceinje.products.model.stone.master;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.spaceinje.products.model.base.AuditEntity;

@Entity
public class SaleStoneRate extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 7756055151716195448L;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stoneId", nullable = false)
	private Stone stone;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneSizeId", nullable = true)
	private StoneSize stoneSize;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneColorId", nullable = true)
	private StoneColor stoneColor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stoneClarityId", nullable = true)
	private StoneClarity stoneClarity;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "stonePolishId", nullable = true)
	private StonePolish stonePolish;

	private double minRate;

	private double maxRate;

	private String uom;

	private boolean noWeight;

	private boolean saleRate;

	private boolean barcodeRate;

	public Stone getStone() {
		return stone;
	}

	public void setStone(Stone stone) {
		this.stone = stone;
	}

	public StoneSize getStoneSize() {
		return stoneSize;
	}

	public void setStoneSize(StoneSize stoneSize) {
		this.stoneSize = stoneSize;
	}

	public StoneColor getStoneColor() {
		return stoneColor;
	}

	public void setStoneColor(StoneColor stoneColor) {
		this.stoneColor = stoneColor;
	}

	public StoneClarity getStoneClarity() {
		return stoneClarity;
	}

	public void setStoneClarity(StoneClarity stoneClarity) {
		this.stoneClarity = stoneClarity;
	}

	public StonePolish getStonePolish() {
		return stonePolish;
	}

	public void setStonePolish(StonePolish stonePolish) {
		this.stonePolish = stonePolish;
	}

	public double getMinRate() {
		return minRate;
	}

	public void setMinRate(double minRate) {
		this.minRate = minRate;
	}

	public double getMaxRate() {
		return maxRate;
	}

	public void setMaxRate(double maxRate) {
		this.maxRate = maxRate;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public boolean isNoWeight() {
		return noWeight;
	}

	public void setNoWeight(boolean noWeight) {
		this.noWeight = noWeight;
	}

	public boolean isSaleRate() {
		return saleRate;
	}

	public void setSaleRate(boolean saleRate) {
		this.saleRate = saleRate;
	}

	public boolean isBarcodeRate() {
		return barcodeRate;
	}

	public void setBarcodeRate(boolean barcodeRate) {
		this.barcodeRate = barcodeRate;
	}

	@Override
	public String toString() {
		return "SaleStoneRate [stone=" + stone + ", stoneSize=" + stoneSize + ", stoneColor=" + stoneColor
				+ ", stoneClarity=" + stoneClarity + ", stonePolish=" + stonePolish + ", minRate=" + minRate
				+ ", maxRate=" + maxRate + ", uom=" + uom + ", noWeight=" + noWeight + ", saleRate=" + saleRate
				+ ", barcodeRate=" + barcodeRate + ", getId()=" + getId() + ", getIsDeleted()=" + getIsDeleted()
				+ ", getTenantCode()=" + getTenantCode() + "]";
	}

}
