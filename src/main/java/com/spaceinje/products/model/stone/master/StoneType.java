package com.spaceinje.products.model.stone.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class StoneType implements Serializable {

	private static final long serialVersionUID = 5892636510092425878L;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "base_id", strategy = "com.spaceinje.products.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String id;
	
	private String shortCode;

	private String typeName;

	@OneToMany(mappedBy = "stoneType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StoneGroup> stoneGroups;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Set<StoneGroup> getStoneGroups() {
		return stoneGroups;
	}

	public void setStoneGroups(Set<StoneGroup> stoneGroups) {
		this.stoneGroups = stoneGroups;
	}

	@Override
	public String toString() {
		return "StoneType [id=" + id + ", shortCode=" + shortCode + ", typeName=" + typeName + ", stoneGroups="
				+ stoneGroups + "]";
	}

	

}
