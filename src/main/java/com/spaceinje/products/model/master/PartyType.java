package com.spaceinje.products.model.master;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class PartyType implements Serializable {

	private static final long serialVersionUID = 2666099962991692151L;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "base_id", strategy = "com.spaceinje.products.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String id;

	private String partyType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	@Override
	public String toString() {
		return "PartyType [id=" + id + ", partyType=" + partyType + "]";
	}

}
