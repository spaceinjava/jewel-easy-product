package com.spaceinje.products.model.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

import com.spaceinje.products.model.other.master.GST;

@Entity
public class MetalType implements Serializable {

	private static final long serialVersionUID = -5826098726386854554L;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "base_id", strategy = "com.spaceinje.products.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String id;
	
	private String shortCode;

	private String metalType;

	@OneToMany(mappedBy = "metalType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<GST> gsts;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getMetalType() {
		return metalType;
	}

	public void setMetalType(String metalType) {
		this.metalType = metalType;
	}

	public Set<GST> getGsts() {
		return gsts;
	}

	public void setGsts(Set<GST> gsts) {
		this.gsts = gsts;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "MetalType [id=" + id + ", shortCode=" + shortCode + ", metalType=" + metalType + ", gsts=" + gsts + "]";
	}

	
}
