package com.spaceinje.products.model.master;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.spaceinje.products.model.base.AuditEntity;
import com.spaceinje.products.model.brand.master.Brand;
import com.spaceinje.products.model.other.master.HSNCode;
import com.spaceinje.products.model.other.master.Purity;
import com.spaceinje.products.model.other.master.Rate;
import com.spaceinje.products.model.product.master.Product;

@Entity
public class MainGroup extends AuditEntity implements Serializable {

	private static final long serialVersionUID = -8269328510784527167L;

	private String groupName;

	private String shortCode;

	@OneToMany(mappedBy = "mainGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Brand> brands;

	@OneToMany(mappedBy = "mainGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Product> products;

	@OneToMany(mappedBy = "mainGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<HSNCode> hsnCodes;

	@OneToMany(mappedBy = "mainGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Purity> purities;

	@OneToMany(mappedBy = "mainGroup", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<Rate> rates;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Set<Brand> getBrands() {
		return brands;
	}

	public void setBrands(Set<Brand> brands) {
		this.brands = brands;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public Set<HSNCode> getHsnCodes() {
		return hsnCodes;
	}

	public void setHsnCodes(Set<HSNCode> hsnCodes) {
		this.hsnCodes = hsnCodes;
	}

	public Set<Purity> getPurities() {
		return purities;
	}

	public void setPurities(Set<Purity> purities) {
		this.purities = purities;
	}

	public Set<Rate> getRates() {
		return rates;
	}

	public void setRates(Set<Rate> rates) {
		this.rates = rates;
	}

	@Override
	public String toString() {
		return "MainGroup [groupName=" + groupName + ", shortCode=" + shortCode + ", brands=" + brands + ", products="
				+ products + ", hsnCodes=" + hsnCodes + ", purities=" + purities + ", rates=" + rates + ", getId()="
				+ getId() + ", getIsDeleted()=" + getIsDeleted() + ", getTenantCode()=" + getTenantCode() + "]";
	}

}
