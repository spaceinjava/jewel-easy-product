package com.spaceinje;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spaceinje.products.util.ResponseCachet;

@FeignClient(name = "tenant", configuration = FeignClientConfiguration.class)
public interface LotServiceClient {

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/v1/tenant/employee/getAllEmployes")
	ResponseCachet getEmployeeDetails(@RequestParam("tenant_code") String tenant_code);
}
