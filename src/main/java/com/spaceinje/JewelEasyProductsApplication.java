package com.spaceinje;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@RefreshScope
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = "com.spaceinje")
@EnableWebSecurity
@OpenAPIDefinition(info = @Info(title = "Jewel Easy Product Service", description = "Inventory management services", version = "1.0"), servers = {
		@Server(url = "https://dev.spaceinje.com/products", description = "Dev Environment"),
		@Server(url = "http://localhost:4000", description = "Local") })
@EnableJpaRepositories(basePackages = { "com.spaceinje.products.repository", "com.spaceinje.journal.repository" })
@EnableJpaAuditing
@EnableCaching
public class JewelEasyProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelEasyProductsApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
