FROM java:8
WORKDIR /
ADD target/jewel-easy-*.jar jewel-easy-product.jar
EXPOSE 80
CMD ["java", "-jar", "jewel-easy-product.jar"]